# Backend Service for Learning Platform Frontend app

## Overview

This project provides RESTful APIs for frontend app and open socket connection for real time features.

## Usage

1. Config environment variables

	Create .env file in the root folder based on .env.template

2. Initiate database and migration

- Run database creation script in the sql folder.
- Install typeorm globally:
```bash
npm install -g typeorm
```
- Run migration:
```bash
npm run migration:run
```

3. Run app
```bash
npm run start:dev
# or
npm start
```

## Swagger
[http://localhost:5000/api-doc](http://localhost:5000/api-doc)

## Frontend repository

[https://gitlab.com/advanced-web/realtime-learning-platform](https://gitlab.com/advanced-web/realtime-learning-platform)
