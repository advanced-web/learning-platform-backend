import { OAuthProvider } from "../core/interfaces/oauth.interface";
import { SLIDE_TYPE } from "./entities.const";

export const PAGINATION = {
	DEFAULT_PAGE: 1,
	DEFAULT_PAGE_SIZE: 10,
	MAX_PAGE_SIZE: 100,
};

export const DATE_FORMAT = {};

export const UNKNOWN_ERROR_RESPONSE_CODE = 1;
export const API_MESSAGES = {
	INTERNAL_ERROR: "Đã có lỗi trong quá trình xử lí. Vui lòng liên hệ Admin để nhận hỗ trợ",
};

export const REGEX = {
	NAME: /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W]+$/,
	PASSWORD: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
};

export const OAUTH_PROVIDERS: Record<string, OAuthProvider> = {
	ZALO: "zalo",
	FACEBOOK: "facebook",
	GOOGLE: "google",
	APPLE: "apple",
};

export const DEFAULT_MULTIPLE_CHOICE_OPTIONS = 3;
export const DEFAULT_SLIDE_TYPE = SLIDE_TYPE.MULTIPLE_CHOICE;
