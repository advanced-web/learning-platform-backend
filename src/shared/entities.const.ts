export enum GROUP_TYPE {
	SELF_CREATED = "self_created",
	JOINED = "joined",
}

export enum GROUP_MEMBER_ROLE {
	OWNER = "owner",
	MEMBER = "member",
	CO_OWNER = "co-owner",
}

export const GROUP_MEMBER_ROLE_NAME = {
	["owner"]: "Trưởng nhóm",
	["co-owner"]: "Phó trưởng nhóm",
	["member"]: "Thành viên",
};

export const GROUP_MEMBER_ACTIVITY_KEYS = {
	CREATE_GROUP: "create_group",
	JOINED_GROUP: "joined_group",
	WAITING_TO_JOIN_GROUP: "waiting_to_join_group",
	LEFT_GROUP: "left_group",
	KICKED_OUT_OF_GROUP: "kicked_out_of_group",
};

export const GROUP_MEMBER_ACTIVITY_VALUES = {
	CREATE_GROUP: "Tạo nhóm",
	JOINED_GROUP: "Đã tham gia nhóm",
	WAITING_TO_JOIN_GROUP: "Đang chờ xác nhận tham gia nhóm",
	LEFT_GROUP: "Đã rời nhóm",
	KICKED_OUT_OF_GROUP: "Đã được mời rời nhóm",
};

export enum SLIDE_TYPE {
	MULTIPLE_CHOICE = "multiple_choice",
	HEADING = "heading",
	PARAGRAPH = "paragraph",
}

export const PRESENTATION_PACE_STATES = {
	IDLE: "idle",
	PRESENTING: "presenting",
};

export const PRESENTATION_PACE_ACTIONS = {
	PRESENT: "present",
	QUIT: "quit",
	CHANGE_SLIDE: "change_slide",
};

export const SLIDE_TEXT_PROPERTY_LENGTHS = {
	MULTIPLE_CHOICE_SLIDE_QUESTION: 150,
	MULTIPLE_CHOICE_SLIDE_QUESTION_DESCRIPTION: 250,
	MULTIPLE_CHOICE_SLIDE_ANSWER: 50,
	PARAGRAPH_SLIDE_QUESTION: 150,
	PARAGRAPH_SLIDE_QUESTION_DESCRIPTION: 800,
	HEADING_SLIDE_QUESTION: 150,
	HEADING_SLIDE_QUESTION_DESCRIPTION: 500,
};

export const PRESENTATION_PACE_SCOPES = {
	PUBLIC: "public",
	GROUP: "group",
};

// ! Must refactor -> store in db
export const RESOURCE_PERMISSION_TYPES = {
	GROUP: "group",
	PRESENTATION: "presentation",
	SLIDE: "slide",
};

export const PRESENTATION_COLLAB_STATUS = {
	JOINED: "joined",
	PENDING: "pending_invitation",
};

export const PRESENTATION_TYPE = {
	OWNER: "owner",
	COLLABORATOR: "collaborator",
};

export const QUESTION_REATION_TYPE = {
	LIKE: "like",
} as const;
