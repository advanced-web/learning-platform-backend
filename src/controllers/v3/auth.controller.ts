import { NextFunction, Request, Response } from "express";
import passport from "passport";
import PassportLocalStrategy, { VerifyFunction } from "passport-local";
import { validateRequestMiddleware } from "../../common";
import { Logger } from "../../common/utils/logger/logger";
import { DataResponseBuilder, HttpErrorBuilder, IBaseResponseObject, loginBodySchema } from "../../core";
import { ISendAccountActivationData } from "../../infrastructure";
import { SendEmailEvent } from "../../infrastructure/events/send-email.event";
import { LocalAuthService, ResUserProfileService, ResUserService } from "../../services";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";
import { BaseController } from "../base.controller";

export class AuthControllerV3 extends BaseController {
	private readonly _localAuthService: LocalAuthService;
	private readonly _resUserService: ResUserService;
	private readonly _resUserProfileService: ResUserProfileService;

	constructor() {
		super("/api/v3/auth");

		this._localAuthService = new LocalAuthService();
		this._resUserService = new ResUserService();
		this._resUserProfileService = new ResUserProfileService();

		this.registerPassportAuthStrategy();
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}
	protected registerRouterHandler(): void {
		this._router.post(
			"/login",
			validateRequestMiddleware("body", loginBodySchema),
			this.loginWithLocalStrategyAsync
		);
	}

	protected registerPassportAuthStrategy() {
		const passportLocalStrategy = new PassportLocalStrategy.Strategy(
			{ usernameField: "username", passwordField: "password", session: false },
			this.handleLocalLoginStrategyProcessAsync.bind(this)
		);

		passport.use(passportLocalStrategy);
	}

	async loginWithLocalStrategyAsync(req: Request, res: Response, next: NextFunction) {
		passport.authenticate("local", async (error, data: IBaseResponseObject) => {
			if (error) {
				return next(error);
			}

			res.json(data);
		})(req, res, next);
	}

	/**
	 *
	 * @description This function handles the credential verification process
	 * @returns By pass the response data into the next handler
	 */
	protected handleLocalLoginStrategyProcessAsync: VerifyFunction = async (
		username: string,
		password: string,
		done
	) => {
		try {
			const [user] = await this._resUserService.getUserByUsernameAsync(username);
			if (!user) {
				return done(
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.BAD_REQUEST)
						.withResponseCode(RESPONSE_CODE.INVALID_LOGIN)
						.build(),
					null
				);
			}

			if (!this._localAuthService.compareUserPassword(password, user.password ?? "")) {
				return done(
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.BAD_REQUEST)
						.withResponseCode(RESPONSE_CODE.INVALID_LOGIN)
						.build(),
					null
				);
			}

			// Account activation
			if (!user.isActive) {
				// generate activation code
				const activationToken = await this._resUserService.generateUserActivationTokenAsync(user);
				SendEmailEvent.instance.notify("send_account_activation", <ISendAccountActivationData>{
					to: user.username,
					token: activationToken,
					userDisplayName: "",
				});

				return done(
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.FORBIDDEN)
						.withResponseCode(RESPONSE_CODE.INACTIVE_USER)
						.build(),
					null
				);
			}

			// create token
			const userProfile = await this._resUserProfileService.getProfileByUserIdAsync(user.id);
			if (!userProfile) {
				Logger.error({ error: "profile_does_not_exist", userId: user.id });
				return done(
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.BAD_REQUEST)
						.withResponseCode(RESPONSE_CODE.LOGIN_ERROR)
						.build(),
					null
				);
			}

			const tokenInformation = this._localAuthService.generateAccessToken(user, userProfile);

			done(null, DataResponseBuilder.init().withData(tokenInformation).build());
		} catch (error) {
			done(error, null);
		}
	};
}
