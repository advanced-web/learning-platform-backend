import { NextFunction, Request, Response } from "express";
import * as SwaggerUi from "swagger-ui-express";
import { AppConfig } from "../infrastructure";
import { BaseController } from "./base.controller";
import swaggerJson from "./swagger.json";

export class ApiDocController extends BaseController {
	constructor() {
		super("/api-doc");
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}

	protected registerRouterHandler(): void {
		this._router.use("", this.modifySwaggerOnTheFly, SwaggerUi.serveFiles(swaggerJson, {}));
		this._router.get("/", SwaggerUi.setup());
	}

	modifySwaggerOnTheFly(req: any, res: Response, next: NextFunction) {
		swaggerJson.servers[0].url = AppConfig.SERVER_URL;
		req.swaggerDoc = swaggerJson;
		next();
	}
}
