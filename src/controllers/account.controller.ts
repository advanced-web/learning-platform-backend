import { Request, Response } from "express";
import moment from "moment";
import { CipherDecoder } from "../common";
import { AppConfig, TokenConfig } from "../infrastructure";
import { ResUserService } from "../services";
import { BaseController } from "./base.controller";

export class AccountController extends BaseController {
	private _resUserService: ResUserService;
	constructor() {
		super("/account");

		this._resUserService = new ResUserService();
	}

	protected registerMiddlewares(): void {}

	protected registerRouterHandler(): void {
		this._router.get("/activate", this.activateUserAcountView.bind(this));
	}

	async activateUserAcountView(req: Request, res: Response) {
		try {
			const { query } = req;
			const { token } = query;

			if (!token || !token.toString().trim()) {
				res.render("pages/account-activation", { isSuccess: false });
				return;
			}

			const user = await this._resUserService.getUserByTokenAsync(token.toString());

			if (!user) {
				// wrong token or the user has already activated
				res.render("pages/account-activation", { isSuccess: false });
				return;
			}

			// found user that the token belongs to
			// if user has already activated, the token is invalid => clear the token
			if (user.isActive) {
				res.render("pages/account-activation", { isSuccess: false });
				return;
			}

			// verify token
			const { activationTokenKey } = user;
			const resultString = CipherDecoder.init().decode(
				token.toString(),
				activationTokenKey ?? "",
				TokenConfig.ACTIVATE_TOKEN_SIGN
			);

			const result = JSON.parse(resultString) as { userId: number; exp: number };

			// check the expiry
			if (moment(result.exp * 1000).isBefore(moment())) {
				res.render("pages/account-activation", { isSuccess: false });
				return;
			}

			// update user status + remove token
			await this._resUserService.updateRecordByIdAsync(user.id, {
				isActive: true,
				activationToken: null,
				activationTokenKey: null,
				activationDate: moment() as any,
			});

			return res.render("pages/account-activation", {
				isSuccess: true,
				next: AppConfig.CLIENT_ACTIVATION_REDIRECT,
				redirectTimeout: 10000,
			});
		} catch (error) {
			console.log(error);
			return res.render("pages/errors/500");
		}
	}
}
