import { NextFunction, Request, Response } from "express";
import _ from "lodash";
import moment from "moment";
import {
	authMiddlewareV2,
	CipherDecoder,
	multerMiddleware,
	validateRequestMiddleware,
	verifyGroupMemberHasJoinedAsync,
	verifyUserActivationMiddlewareAsync,
} from "../../common";
import { ResourcePermission } from "../../common/decorators";
import { Logger } from "../../common/utils/logger/logger";
import {
	acceptInvitedUserBodySchema,
	ActivityRecord,
	createGroupBodySchema,
	DataResponseBuilder,
	getGroupDetailQuerySchema,
	getListGroupQuerySchema,
	groupIdPathParamSchema,
	HttpErrorBuilder,
	InvitationTokenPayload,
	LearningGroup,
	removeMemberParamsSchema,
	sendInvitationLinkByEmailBodySchema,
	setGroupMemberRoleBodySchema,
	setGroupMemberRoleParamsSchema,
} from "../../core";
import { ImgBBService, InvitationLinkConfig, ISendGroupInvitationData, SendEmailEvent } from "../../infrastructure";
import { LearningGroupMemberService, LearningGroupService, UserPermissionService } from "../../services";
import {
	GROUP_MEMBER_ACTIVITY_KEYS,
	GROUP_MEMBER_ACTIVITY_VALUES,
	GROUP_MEMBER_ROLE,
	GROUP_MEMBER_ROLE_NAME,
	HTTP_CODE,
	RESOURCE_PERMISSION_TYPES,
	RESPONSE_CODE,
} from "../../shared";
import { DEFAULT_GROUP_PERMISSION, GROUP_PERMISSIONS } from "../../shared/permission.const";
import { BaseController } from "../base.controller";

export class LearningGroupController extends BaseController {
	private readonly _learningGroupService: LearningGroupService;
	private readonly _learningGroupMemberService: LearningGroupMemberService;
	private readonly _imgbbService: ImgBBService;
	private readonly _userPermissionService: UserPermissionService;

	constructor() {
		super("/api/v1/groups");
		this._learningGroupService = new LearningGroupService();
		this._learningGroupMemberService = new LearningGroupMemberService();
		this._imgbbService = new ImgBBService();
		this._userPermissionService = new UserPermissionService();
	}

	protected registerMiddlewares(): void {}

	protected registerRouterHandler(): void {
		this._router.post(
			"/",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			multerMiddleware.single("groupAvatar"),
			validateRequestMiddleware("body", createGroupBodySchema),
			this.createLearningGroupAsync.bind(this)
		);

		this._router.get(
			"/",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("query", getListGroupQuerySchema),
			this.getLearningGroupListAsync.bind(this)
		);

		this._router.get(
			"/:id",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("params", groupIdPathParamSchema),
			validateRequestMiddleware("query", getGroupDetailQuerySchema),
			verifyGroupMemberHasJoinedAsync,
			this.getGroupDetailAsync.bind(this)
		);

		this._router.post(
			"/accept-invitation",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("query", acceptInvitedUserBodySchema),
			this.acceptInvitedUserAsync.bind(this)
		);

		this._router.post(
			"/:id/invite",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("params", groupIdPathParamSchema),
			validateRequestMiddleware("body", sendInvitationLinkByEmailBodySchema),
			this.sendInvitationLinkByEmailAsync.bind(this)
		);

		this._router.put(
			"/:id/members/:groupMemberId",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("params", setGroupMemberRoleParamsSchema),
			validateRequestMiddleware("body", setGroupMemberRoleBodySchema),
			this.setGroupMemberRoleAsync.bind(this)
		);

		this._router.delete(
			"/:id",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			this.deleteGroupAsync.bind(this)
		);

		this._router.delete(
			"/:id/members/:memberId",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("params", removeMemberParamsSchema),
			this.removeMemberOutOfGroupAsync.bind(this)
		);
	}

	async createLearningGroupAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { body, userinfo } = req;

			let groupAvatar!: string;
			if (req.file) {
				const { path, filename } = req.file;

				Logger.debug(`Uploading avatar to imgbb with ${JSON.stringify({ filename, path })}`);
				const uploadResp = await this._imgbbService.uploadAsync(path, filename);

				if (uploadResp.status === 200 && uploadResp.success) {
					Logger.debug("Uploaded successfully avatar to imgbb");
					groupAvatar = uploadResp.data.url ?? uploadResp.data.image.url;
				} else {
					Logger.debug("Failed to upload avatar to imgbb");
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.BAD_REQUEST)
						.withResponseCode(RESPONSE_CODE.UPLOAD_ERROR)
						.throw();
					return;
				}
			}

			// create group
			const group = await this._learningGroupService.saveAsync({
				name: body.name,
				description: body.description,
				groupAvatar,
				ownerProfileId: userinfo.uid,
				ownerDisplayName: userinfo.display_name,
			});

			// return the generated invitation link
			await this._learningGroupService.generateAndSaveInvitationLinkAsync(group.id);

			// add current user into member table
			await this._learningGroupMemberService.saveAsync({
				groupId: group.id,
				userProfileId: userinfo.uid,
				userDisplayName: userinfo.display_name,
				role: GROUP_MEMBER_ROLE.OWNER,
				roleName: GROUP_MEMBER_ROLE_NAME["owner"],
				activity: [
					{
						timestamp: moment().toISOString(),
						key: GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP,
						value: "Tạo nhóm",
					},
				],
				currentStatus: {
					timestamp: moment().toISOString(),
					key: GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP,
					value: "Tạo nhóm",
				},
			});

			// update current user's permission
			await this._userPermissionService.saveAsync({
				subjectId: userinfo.uid,
				resourceType: RESOURCE_PERMISSION_TYPES.GROUP,
				resourceId: group.id,
				permissions: DEFAULT_GROUP_PERMISSION.OWNER.join(","),
			});

			const result = DataResponseBuilder.init().withStatusCode(HTTP_CODE.CREATED).build();

			res.status(HTTP_CODE.CREATED).json(result);
		} catch (error) {
			next(error);
		}
	}

	async getLearningGroupListAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { query, userinfo } = req;
			const { limit, page, groupType, searchKey } = query as any;

			let groupList: LearningGroup[] = [];
			let total: number = 0;

			// get all group that a user has joined
			// the groupType is as same as group member role -> see entities.const.ts
			const [groupJoinedList] = await this._learningGroupMemberService.getGroupListByMemberIdRoleAsync(
				userinfo.uid,
				groupType
			);

			// We can get group information by group id list
			const groupIdList = groupJoinedList.map((group) => group.groupId);

			const filter: Record<string, any> = { searchKey: searchKey?.toString(), groupIdList };

			[groupList, total] = await this._learningGroupService.getGroupListAsync(
				filter,
				parseInt(limit?.toString() as string),
				parseInt(page?.toString() as string)
			);

			if (total === 0) {
				return res.json(
					DataResponseBuilder.init()
						.withData({
							items: [],
							pagination: {
								totalRecords: total,
								page,
								limit,
							},
						})
						.build()
				);
			}

			if (page > Math.ceil(total / limit)) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.throw();
				return;
			}

			res.json(
				DataResponseBuilder.init()
					.withData({
						items: groupList,
						pagination: {
							totalRecords: total,
							page,
							limit,
						},
					})
					.build()
			);
		} catch (error) {
			next(error);
		}
	}

	async getGroupDetailAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const groupId = parseInt(req.params.id);
			const fetchedGroupFields = [
				"id",
				"name",
				"description",
				"group_avatar",
				"invitation_link",
				"presentation_series_id",
				"vote_key",
			];
			const group = await this._learningGroupService.getRawGroupByIdAsync(groupId, fetchedGroupFields);

			if (!group) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
					.throw();
				return;
			}

			const { limit, page } = req.query as any;
			const [memberList, totalRecords] =
				await this._learningGroupMemberService.getRawMemberListJoinUserByGroupIdAsync(
					groupId,
					parseInt(limit?.toString() as string),
					parseInt(page?.toString() as string)
				);

			if (page > Math.ceil(totalRecords / limit)) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.MEMBER_NOT_FOUND)
					.throw();
				return;
			}

			// Get current user permission
			const userPermissionList = await this._userPermissionService.getRecordByAsync({
				subjectId: req.userinfo.uid,
				resourceId: groupId,
				resourceType: RESOURCE_PERMISSION_TYPES.GROUP,
			});

			const result = DataResponseBuilder.init()
				.withData({
					groupDetail: group,
					items: memberList,
					pagination: { totalRecords, page, limit },
					permission: {
						subject: { id: req.userinfo.uid, displayName: req.userinfo.display_name },
						actions: userPermissionList ? userPermissionList.permissions.split(",") : [],
					},
				})
				.build();

			return res.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	async acceptInvitedUserAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const code = _.get(req.query, "code", "") as string;

			const [group] = await this._learningGroupService.getManyRecordsAsync({
				where: { invitationToken: code },
			});
			if (!group) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
					.throw();
				return;
			}

			// decode invitation token (code)
			const resultString = CipherDecoder.init().decode(
				code,
				group.invitationTokenKey ?? "",
				InvitationLinkConfig.INVITATION_LINK_SIGN
			);
			const decodedInvitationCode: InvitationTokenPayload = JSON.parse(resultString);

			const user = req.userinfo;
			const [member] = await this._learningGroupMemberService.getManyRecordsAsync({
				where: {
					groupId: group.id,
					userProfileId: user.uid,
				},
			});

			// this user is joined or invited (waiting) or left
			if (member) {
				// update
				if (
					member.currentStatus.key !== GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP &&
					member.currentStatus.key !== GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP
				) {
					// update the current status and add new activity, must denote this user to member role
					const joinedGroupState: ActivityRecord = {
						timestamp: moment().toISOString(),
						key: GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP,
						value: GROUP_MEMBER_ACTIVITY_VALUES.JOINED_GROUP,
					};

					await this._learningGroupMemberService.updateRecordByIdAsync(member.id, {
						role: decodedInvitationCode.role,
						currentStatus: joinedGroupState,
						activity: [...member.activity, joinedGroupState],
					});

					// update current user's permission: default memeber permission only
					await this._userPermissionService.updateByAsync(
						{ subjectId: user.uid, resourceType: RESOURCE_PERMISSION_TYPES.GROUP, resourceId: group.id },
						{
							permissions: decodedInvitationCode.permissions,
						}
					);
				}
			} else {
				// insert
				const joinedGroupState: ActivityRecord = {
					timestamp: moment().toISOString(),
					key: GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP,
					value: GROUP_MEMBER_ACTIVITY_VALUES.JOINED_GROUP,
				};
				// add this user to the group as a member
				await this._learningGroupMemberService.saveAsync({
					groupId: group.id,
					userProfileId: user.uid,
					userDisplayName: user.display_name,
					role: decodedInvitationCode.role,
					roleName: GROUP_MEMBER_ROLE_NAME[decodedInvitationCode.role],
					activity: [joinedGroupState],
					currentStatus: joinedGroupState,
				});

				// create current user's permission: default memeber permission only
				await this._userPermissionService.saveAsync({
					subjectId: user.uid,
					resourceType: RESOURCE_PERMISSION_TYPES.GROUP,
					resourceId: group.id,
					permissions: decodedInvitationCode.permissions,
				});
			}

			const result = DataResponseBuilder.init().withData({ groupId: group.id }).build();
			return res.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	// ! Temporary solution: send via multiple email with the same link, does not support priavte email for each user
	@ResourcePermission([GROUP_PERMISSIONS.INVITE_MEMBER], (req: Request, res: Response) => {
		return { resourceId: parseInt(req.params.id), resourceType: RESOURCE_PERMISSION_TYPES.GROUP };
	})
	async sendInvitationLinkByEmailAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const groupId = parseInt(req.params.id);
			const emails = _.get(req.body, "emails", "") as string[];

			const group = await this._learningGroupService.getRecordByIdAsync(groupId);
			if (!group) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
					.throw();
				return;
			}

			// !old solution: only owner can send
			// * new solution: check by permission through decorator
			// if (group.ownerProfileId !== req.user.uid) {
			// 	HttpErrorBuilder.init()
			// 		.withStatusCode(HTTP_CODE.FORBIDDEN)
			// 		.withResponseCode(RESPONSE_CODE.INVITATION_PERMISSION)
			// 		.throw();
			// 	return;
			// }

			// prevent send link to the sender's email
			// current solution: prevent sender only
			const shoudSendEmails = emails.filter((email) => email !== req.userinfo.email);

			if (shoudSendEmails.length === 0) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.EMPTY_INVITATION_EMAIL_LIST)
					.throw();
				return;
			}

			res.status(HTTP_CODE.OK).json(DataResponseBuilder.init().build());
			// send email with link
			SendEmailEvent.instance.notify("send_group_invitation", <ISendGroupInvitationData>{
				to: shoudSendEmails.join(","),
				groupName: group.name,
				invitationLink: group.invitationLink,
			});
		} catch (error) {
			next(error);
		}
	}

	@ResourcePermission(
		[GROUP_PERMISSIONS.PROMOTE_MEMBER_ROLE, GROUP_PERMISSIONS.DENOTE_MEMBER_ROLE],
		(req: Request, res: Response) => {
			return { resourceId: parseInt(req.params.id), resourceType: RESOURCE_PERMISSION_TYPES.GROUP };
		}
	)
	async setGroupMemberRoleAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const groupId = parseInt(req.params.id);
			const groupMemberId = parseInt(req.params.groupMemberId);
			const role = _.get(req.body, "role", "") as string;

			// update: add role with permissions
			const permissionPayload = _.get(req.body, "permissions", []) as string[];

			const group = await this._learningGroupService.getRecordByIdAsync(groupId);
			if (!group) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
					.throw();
				return;
			}
			// !old solution verify if the current user can change role (owner only)
			// else if (group.ownerProfileId !== req.user.uid) {
			// 	HttpErrorBuilder.init()
			// 		.withStatusCode(HTTP_CODE.FORBIDDEN)
			// 		.withResponseCode(RESPONSE_CODE.CHANGE_ROLE_PERMISSION)
			// 		.throw();
			// 	return;
			// }

			const groupMember = await this._learningGroupMemberService.getRecordByIdAsync(groupMemberId);
			if (
				!groupMember ||
				groupMember.groupId !== groupId ||
				groupMember.currentStatus.key === GROUP_MEMBER_ACTIVITY_KEYS.LEFT_GROUP
			) {
				// member not found
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.MEMBER_NOT_FOUND)
					.throw();
				return;
			} else if (groupMember.userProfileId === req.userinfo.uid) {
				// change my role -> is not allowed
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.CHANGE_ROLE_PERMISSION)
					.throw();
				return;
			}

			// update member role
			await this._learningGroupMemberService.updateRecordByIdAsync(groupMemberId, {
				role,
				roleName: GROUP_MEMBER_ROLE_NAME[role],
			});

			// update member permission
			// const permissions =

			let permissions = [...permissionPayload];

			// assign default group permissions for each role if the client does not submit permissions
			if (permissionPayload.length === 0) {
				permissions =
					role === GROUP_MEMBER_ROLE.CO_OWNER
						? DEFAULT_GROUP_PERMISSION.CO_OWNER
						: DEFAULT_GROUP_PERMISSION.MEMBER;
			}

			await this._userPermissionService.updateByAsync(
				{
					subjectId: groupMember.userProfileId,
					resourceId: groupId,
					resourceType: RESOURCE_PERMISSION_TYPES.GROUP,
				},
				{ permissions: permissions.join(",") }
			);

			return res.status(HTTP_CODE.OK).json(DataResponseBuilder.init().build());
		} catch (error) {
			next(error);
		}
	}

	@ResourcePermission([GROUP_PERMISSIONS.DELETE], (req: Request, res: Response) => {
		return { resourceId: parseInt(req.params.id), resourceType: RESOURCE_PERMISSION_TYPES.GROUP };
	})
	async deleteGroupAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { id } = req.params;
			const groupId = parseInt(id);

			// find if the current group exists
			const group = await this._learningGroupService.getRecordByIdAsync(groupId);
			if (!group) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
					.throw();
				return;
			}

			// must ensure that there is no presentation on this group
			if (group.presentationSeriesId) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_IN_PRESENT)
					.throw();
				return;
			}

			// delete all permission on this group
			await this._userPermissionService.deleteByAsync({
				resourceId: groupId,
				resourceType: RESOURCE_PERMISSION_TYPES.GROUP,
			});

			// delete all member in this group
			await this._learningGroupMemberService.deleteByAsync({ groupId });

			// finally delete the current group
			await this._learningGroupService.deleteRecordByIdAsync(groupId);

			res.status(HTTP_CODE.OK).json(DataResponseBuilder.init().withStatusCode(HTTP_CODE.OK).build());
		} catch (error) {
			next(error);
		}
	}

	@ResourcePermission([GROUP_PERMISSIONS.KICK_MEMBER], (req: Request, res: Response) => {
		return { resourceId: parseInt(req.params.id), resourceType: RESOURCE_PERMISSION_TYPES.GROUP };
	})
	async removeMemberOutOfGroupAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { id, memberId } = req.params;

			const groupId = parseInt(id);
			const groupMemberId = parseInt(memberId);

			// find if the current group exists
			const group = await this._learningGroupService.getRecordByIdAsync(groupId);
			if (!group) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
					.throw();
				return;
			}

			const groupMember = await this._learningGroupMemberService.getRecordByIdAsync(groupMemberId);

			if (
				!groupMember ||
				groupMember.groupId !== groupId ||
				[GROUP_MEMBER_ACTIVITY_KEYS.LEFT_GROUP, GROUP_MEMBER_ACTIVITY_KEYS.KICKED_OUT_OF_GROUP].includes(
					groupMember.currentStatus.key
				)
			) {
				// member not found
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.MEMBER_NOT_FOUND)
					.throw();
				return;
			}

			// kick my self & kick owner -> is not allowed
			if (groupMember.userProfileId === group.ownerProfileId || groupMember.userProfileId === req.userinfo.uid) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.CAN_NOT_REMOVE_MEMBER)
					.throw();
				return;
			}

			// finally remove the member
			// remove permission first
			await this._userPermissionService.deleteByAsync({
				subjectId: groupMember.userProfileId,
				resourceId: groupId,
				resourceType: RESOURCE_PERMISSION_TYPES.GROUP,
			});

			// update status of member -> kicked out
			const joinedGroupState: ActivityRecord = {
				timestamp: moment().toISOString(),
				key: GROUP_MEMBER_ACTIVITY_KEYS.KICKED_OUT_OF_GROUP,
				value: GROUP_MEMBER_ACTIVITY_VALUES.KICKED_OUT_OF_GROUP,
			};

			await this._learningGroupMemberService.updateRecordByIdAsync(groupMember.id, {
				currentStatus: joinedGroupState,
				activity: [...groupMember.activity, joinedGroupState],
				role: GROUP_MEMBER_ROLE.MEMBER,
				roleName: GROUP_MEMBER_ROLE_NAME.member,
			});

			res.json(DataResponseBuilder.init().build());
		} catch (error) {
			next(error);
		}
	}
}
