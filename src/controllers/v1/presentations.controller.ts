import { NextFunction, Request, Response } from "express";
import { nanoid } from "nanoid";
import { FindOptionsWhere, In } from "typeorm";
import {
	authMiddlewareV2, mapPresentationRole, preGetPresentationAsync,
	validateRequestMiddleware,
	ValidationHandler,
	ValueGenerator,
	verifyUserActivationMiddlewareAsync,
	ResourcePermission
} from "../../common";
import {
	createPresentationSchema,
	DataResponseBuilder,
	editPresentationSchema,
	getPresentationListSchema,
	HttpErrorBuilder,
	PerformSlideActionBody,
	performSlideActionBodySchema,
	presentationIdPathParamSchema,
	PresentationPace,
	Presentations,
	presentationSeriesIdPathParamSchema
} from "../../core";
import { SlideControllerSocketEventHandler, SocketRoomIdentifiers } from "../../infrastructure";
import {
	LearningGroupMemberService,
	LearningGroupService,
	PresentationCollaboratorService,
	PresentationsService,
	SlidesService,
	UserPermissionService
} from "../../services";
import {
	DEFAULT_PRESENTATION_PERMISSION,
	DEFAULT_SLIDE_TYPE,
	GROUP_MEMBER_ROLE,
	HTTP_CODE, PRESENTATION_PACE_ACTIONS,
	PRESENTATION_PACE_SCOPES,
	PRESENTATION_PACE_STATES,
	PRESENTATION_PERMISSIONS,
	PRESENTATION_TYPE,
	RESOURCE_PERMISSION_TYPES,
	RESPONSE_CODE
} from "../../shared";
import { BaseController } from "../base.controller";

export class PresentationsController extends BaseController {
	private _presentationsService: PresentationsService;
	private _slidesService: SlidesService;
	private _learningGroupService: LearningGroupService;
	private _learningGroupMemberService: LearningGroupMemberService;
	private readonly _userPermission: UserPermissionService;
	private readonly _collaboratorService: PresentationCollaboratorService;

	constructor() {
		super("/api/v1/presentations");

		this._presentationsService = new PresentationsService();
		this._slidesService = new SlidesService();
		this._learningGroupService = new LearningGroupService();
		this._learningGroupMemberService = new LearningGroupMemberService();
		this._userPermission = new UserPermissionService();
		this._collaboratorService = new PresentationCollaboratorService();
	}

	protected registerMiddlewares() {
		this._router.use(authMiddlewareV2, verifyUserActivationMiddlewareAsync);
	}

	protected registerRouterHandler() {
		this._router.get(
			"/",
			validateRequestMiddleware("query", getPresentationListSchema),
			this.getPresentationListAsync.bind(this)
		);

		this._router.post(
			"/",
			validateRequestMiddleware("body", createPresentationSchema),
			this.createPresentationAsync.bind(this)
		);

		this._router.put(
			"/:id",
			validateRequestMiddleware("params", presentationIdPathParamSchema),
			validateRequestMiddleware("body", editPresentationSchema),
			this.editPresentationAsync.bind(this)
		);

		this._router.delete(
			"/:id",
			validateRequestMiddleware("params", presentationIdPathParamSchema),
			this.deletePresentationAsync.bind(this)
		);

		this._router.get(
			"/:series_id",
			validateRequestMiddleware("params", presentationSeriesIdPathParamSchema),
			preGetPresentationAsync("params.series_id"),
			mapPresentationRole,
			this.getPresentationDetailAsync.bind(this)
		);

		this._router.post(
			"/:series_id",
			validateRequestMiddleware("params", presentationSeriesIdPathParamSchema),
			validateRequestMiddleware("body", performSlideActionBodySchema),
			preGetPresentationAsync("params.series_id"),
			this.performSlideActionAsync.bind(this)
		);
	}

	/**
	 * @description All users can call to obtain the list of presentation which they are the owner or the collaborator
	 */
	async getPresentationListAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid } = request.userinfo;
			const { limit, page, type } = request.query as any;

			let [presentations, count] = [[] as Presentations[], 0];

			if (type === PRESENTATION_TYPE.COLLABORATOR) {
				const collab = await this._collaboratorService.getManyRecordsAsync({ where: { userId: uid } });

				if (collab.length > 0) {
					[presentations, count] =
						await this._presentationsService.getPresentationListAndCountBySeriesIdAsync(
							collab.map((c) => `'${c.pesentationSeriesId}'`),
							parseInt(limit?.toString() as string),
							parseInt(page?.toString() as string)
						);
				}
			} else if (type === PRESENTATION_TYPE.OWNER) {
				[presentations, count] = await this._presentationsService.getPresentationListAndCountByOwnerIdAsync(
					uid,
					parseInt(limit?.toString() as string),
					parseInt(page?.toString() as string)
				);
			}

			if (count !== 0 && page > Math.ceil(count / limit)) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.throw();
				return;
			}

			const result = DataResponseBuilder.init()
				.withData({ items: presentations, pagination: { totalRecords: count, page, limit } })
				.build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @description
	 * - All users can create the presentation
	 * - Updated on RLP-162 27/12/2022: add edit permission for the owner
	 */
	async createPresentationAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid, display_name } = request.userinfo;
			const { name } = request.body as { name: string };

			const defaultPace = ValueGenerator.instance.generatePresentationPace();

			// Creates presentation
			const createdPresentation = await this._presentationsService.saveAsync({
				name,
				voteKey: nanoid(),
				ownerId: uid,
				ownerDisplayName: display_name,
				pace: defaultPace,
				closedForVoting: false,
				slideCount: 0,
			});

			// Updates presentation's slide count and pace
			await this._slidesService.createSlideAndUpdatePresentation(DEFAULT_SLIDE_TYPE, createdPresentation);

			// Create permission on this presentation
			await this._userPermission.saveAsync({
				subjectId: uid,
				resourceId: createdPresentation.id,
				resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION,
				permissions: DEFAULT_PRESENTATION_PERMISSION.OWNER.join(","),
			});

			// Fetchs updated presentation
			const updatedNewPresentation = await this._presentationsService.getRecordByIdAsync(createdPresentation.id);
			const result = DataResponseBuilder.init()
				.withStatusCode(HTTP_CODE.CREATED)
				.withData(updatedNewPresentation)
				.build();

			return response.status(HTTP_CODE.CREATED).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access presentation's owner and collaboratos
	 * @description Change the presentation's general information: name
	 */
	@ResourcePermission([PRESENTATION_PERMISSIONS.EDIT], (req: Request, res: Response) => {
		return { resourceId: parseInt(req.params.id), resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION };
	})
	async editPresentationAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const presentationId = parseInt(request.params.id);
			const { name } = request.body as { name: string };

			const where: FindOptionsWhere<Presentations> = { id: presentationId };
			const updateResult = await this._presentationsService.updateByAsync(where, { name });
			if (!updateResult.affected) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.throw();
				return;
			}

			const result = DataResponseBuilder.init().build();
			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access presentation's owner
	 * @description Delete the presentation
	 */
	async deletePresentationAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid } = request.userinfo;
			const presentationId = parseInt(request.params.id);

			const isPresenting = await this._presentationsService.checkPaceStateByIdentifier(
				PRESENTATION_PACE_STATES.PRESENTING,
				{ key: "id", value: presentationId }
			);

			if (isPresenting) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTING_PRESENTATION)
					.throw();
				return;
			}

			const deleteResult = await this._presentationsService.deleteByIdAndOwnerIdAsync(presentationId, uid);
			if (!deleteResult.affected) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.throw();
				return;
			}

			const result = DataResponseBuilder.init().build();
			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access owner and collab of a presentation
	 * @access owner and co-owner of a group
	 */
	// The previous midd ensure the presentation is existed
	// * REVISE permission RLP-175 - 2022-12-29
	async getPresentationDetailAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { series_id } = request.params;
			const presentation = request.presentation!!;

			// Before get more detail, we must check the permission of current user
			// Must ensure which type of present state
			// If the current user is owner or collaborator => by pass
			const mappedRole = request.presentationRoleMap!!;

			if (!mappedRole.groupRole && !mappedRole.presentationRole) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION)
					.build();
			}

			const fetchedSlideFields = ["id", "admin_key", "type"];
			const slides = await this._slidesService.getRawSlidesBySeriesId(series_id, fetchedSlideFields);

			const result = DataResponseBuilder.init()
				.withData({ ...presentation, slides, permission: mappedRole })
				.build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access check permission base on action
	 * - only the owner of the presentation can "present"
	 * - the owner or co-owner of a group can "quit" | "change_slide"
	 */
	// The previous midd ensure the presentation is existed
	// * REVISE permission RLP-175 - 2022-12-29
	async performSlideActionAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid } = request.userinfo;
			const admin_key: string | undefined = request.body.admin_key;
			const action: string = request.body.action;

			// Ensure that `admin_key` is valid if `action` is `present` or `change_slide`
			// admin_key belongs to slide
			if (
				(action === PRESENTATION_PACE_ACTIONS.PRESENT || action === PRESENTATION_PACE_ACTIONS.CHANGE_SLIDE) &&
				!admin_key
			) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.VALIDATION_ERROR)
					.withErrors([
						{
							message: "Trang trình chiếu không hợp lệ",
							path: ["amdin_key"],
							type: "any.required",
							context: {
								label: "admin_key",
								value: { admin_key },
								key: "admin_key",
							},
						},
					])
					.throw();
				return;
			}

			const presentation = request.presentation!!;
			let { scope, groupId } = presentation.pace;

			const invalidPermissionError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.FORBIDDEN)
				.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION)
				.build();

			// Must verify user's permission on action first
			if (scope === PRESENTATION_PACE_SCOPES.GROUP) {
				// Check group member permission on presentation: owner or co-owner
				const groupMember = await this._learningGroupMemberService.getRecordByAsync({
					groupId: presentation.pace.groupId!!,
					userProfileId: uid,
					role: In([GROUP_MEMBER_ROLE.OWNER, GROUP_MEMBER_ROLE.CO_OWNER]),
				});

				if (!groupMember || (action === PRESENTATION_PACE_ACTIONS.PRESENT && uid !== presentation.ownerId)) {
					throw invalidPermissionError;
				}
			} else {
				if (presentation.ownerId !== uid) {
					throw invalidPermissionError;
				}
			}

			if (action === PRESENTATION_PACE_ACTIONS.QUIT) {
				scope = "public";
				groupId = null;
			} else if (action === PRESENTATION_PACE_ACTIONS.PRESENT) {
				const validationErrors = this._validatePresentationPacePermission(request.body);
				if (validationErrors.length > 0) {
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.BAD_REQUEST)
						.withResponseCode(RESPONSE_CODE.VALIDATION_ERROR)
						.withErrors(validationErrors)
						.throw();
					return;
				}

				scope = request.body.scope;
				groupId = request.body.groupId; // nullable

				if (presentation.pace.state === PRESENTATION_PACE_STATES.PRESENTING) {
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.FORBIDDEN)
						.withResponseCode(RESPONSE_CODE.PRESENTING_SLIDE_PERMISSION)
						.throw();
					return;
				}

				if (scope === PRESENTATION_PACE_SCOPES.GROUP) {
					const safeGroupId = Number(groupId) || 0;
					const group = await this._learningGroupService.getRecordByIdAsync(safeGroupId);

					if (!group) {
						HttpErrorBuilder.init()
							.withStatusCode(HTTP_CODE.BAD_REQUEST)
							.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
							.throw();
						return;
					}

					// group.presentationSeriesId !== presentation.pace.active
					// because presentation.pace.state is "idle"
					if (group.presentationSeriesId) { // !== null
						if (presentation.seriesId === group.presentationSeriesId) {
							HttpErrorBuilder.init()
								.withStatusCode(HTTP_CODE.BAD_REQUEST)
								.withResponseCode(RESPONSE_CODE.MY_PRESENTATION_IN_GROUP)
								.throw();
							return;
						} else {
							HttpErrorBuilder.init()
								.withStatusCode(HTTP_CODE.BAD_REQUEST)
								.withResponseCode(RESPONSE_CODE.OTHER_PRESENTATION_IN_GROUP)
								.throw();
							return;
						}
					}

					// Validate group owner/co-owner
					const isValidRole = await this._learningGroupMemberService.existsByAsync({
						groupId: safeGroupId,
						userProfileId: uid,
						role: In([GROUP_MEMBER_ROLE.OWNER, GROUP_MEMBER_ROLE.CO_OWNER]),
					});

					if (!isValidRole) {
						HttpErrorBuilder.init()
							.withStatusCode(HTTP_CODE.FORBIDDEN)
							.withResponseCode(RESPONSE_CODE.PRESENT_SLIDE_PERMISSION)
							.throw();
						return;
					}
				}
			} else {
				// Change slide action
				// Not allow to change slide when it is "idle"
				if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.FORBIDDEN)
						.withResponseCode(RESPONSE_CODE.CHANGE_SLIDE_PERMISSION)
						.throw();
					return;
				}
			}

			if (action !== PRESENTATION_PACE_ACTIONS.QUIT) {
				const isExistedSlide = await this._presentationsService.hasSlideBySeriesIdAndAdminKey(
					presentation.seriesId,
					admin_key!
				);
				if (!isExistedSlide) {
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.BAD_REQUEST)
						.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
						.throw();
					return;
				}
			}

			let groupPresentationSeriesId: string | null = null;
			let groupPresentationVoteKey: string | null = null;
			let {
				active: paceActive,
				state: paceState,
				counter: presentCount,
				groupId: groupIdToUpdatePresentationSeriesId,
			} = presentation.pace;
			let socketRoomGroupId: number | null = null;

			// Infers the pace state and the present count from the action
			switch (action) {
				case PRESENTATION_PACE_ACTIONS.PRESENT: {
					presentCount += 1;
					paceState = PRESENTATION_PACE_STATES.PRESENTING;
					paceActive = admin_key!; // Ensure it's valid value because this function's first condition
					groupPresentationSeriesId = presentation.seriesId;
					groupPresentationVoteKey = presentation.voteKey;
					groupIdToUpdatePresentationSeriesId = groupId; // groupId from request body, valid group id
					socketRoomGroupId = groupId;
					break;
				}
				case PRESENTATION_PACE_ACTIONS.QUIT: {
					paceState = PRESENTATION_PACE_STATES.IDLE;
					groupPresentationSeriesId = null;
					groupPresentationVoteKey = null;
					socketRoomGroupId = presentation.pace.groupId;
					break;
				}
				case PRESENTATION_PACE_ACTIONS.CHANGE_SLIDE: {
					paceActive = admin_key!; // Ensure it's valid value because this function's first condition
					groupPresentationSeriesId = presentation.seriesId;
					groupPresentationVoteKey = presentation.voteKey;
					groupIdToUpdatePresentationSeriesId = null;
					break;
				}
			}

			// Updates presentation's pace
			const newPace: PresentationPace = {
				...presentation.pace,
				active: paceActive,
				state: paceState,
				counter: presentCount,
				scope,
				groupId,
			};
			await this._presentationsService.updateRecordByIdAsync(presentation.id, { pace: newPace });

			// Update group's presentation series id and vote_key
			if (groupIdToUpdatePresentationSeriesId) {
				await this._learningGroupService.updateRecordByIdAsync(groupIdToUpdatePresentationSeriesId, {
					presentationSeriesId: groupPresentationSeriesId,
					voteKey: groupPresentationVoteKey,
				});
			}

			// Send response
			response.sendStatus(HTTP_CODE.NO_CONTENT);

			// Emit socket event
			const { seriesId, voteKey } = presentation;
			const socketRoomIds: SocketRoomIdentifiers = { voteKey, groupId: socketRoomGroupId };
			SlideControllerSocketEventHandler.instance.handleSlideAction(socketRoomIds, action, seriesId, newPace);
		} catch (error) {
			next(error);
		}
	}

	private _validatePresentationPacePermission(data: PerformSlideActionBody) {
		const validationErrors = ValidationHandler.instance.validatePresentationPacePermissionStructure(data);
		const { scope, groupId } = data;

		if (!ValidationHandler.instance.validatePresentationPacePermission(scope, groupId)) {
			validationErrors.push({
				message: "Có lỗi xảy ra trong quá trình xử lý yêu cầu",
				path: ["scope", "groupId"],
				type: "",
				context: {
					label: "",
					value: { scope, groupId },
					key: "",
				},
			});
		}

		return validationErrors;
	}
}
