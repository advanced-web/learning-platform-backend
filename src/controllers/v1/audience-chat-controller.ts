import { NextFunction, Request, Response } from "express";
import {
	authMiddlewareV2,
	preGetPresentationAsync,
	validateRequestMiddleware,
	verifyPresentationVotingLinkPermission,
	verifyUserActivationMiddlewareAsync,
} from "../../common";
import { createChatMessageSchema, getChatHistorySchema, DataResponseBuilder, HttpErrorBuilder } from "../../core";
import { ChatSocketEventHandler } from "../../infrastructure";
import { LearningGroupMemberService, PresentationChatService } from "../../services";
import {
	GROUP_MEMBER_ACTIVITY_KEYS,
	HTTP_CODE,
	PRESENTATION_PACE_SCOPES,
	PRESENTATION_PACE_STATES,
	RESPONSE_CODE,
} from "../../shared";
import { BaseController } from "../base.controller";

export class AudienceChatController extends BaseController {
	private readonly _chatService: PresentationChatService;
	private readonly _groupMemberService: LearningGroupMemberService;

	constructor() {
		super("/api/v1/audience");

		this._chatService = new PresentationChatService();
		this._groupMemberService = new LearningGroupMemberService();
	}

	protected registerRouterHandler(): void {
		this._router.use(authMiddlewareV2, verifyUserActivationMiddlewareAsync);
	}

	protected registerMiddlewares(): void {
		this._router.get(
			"/presentations/:vote_key/chats",
			validateRequestMiddleware("query", getChatHistorySchema),
			verifyPresentationVotingLinkPermission,
			this.getPresentationChatListAsync.bind(this)
		);

		this._router.post(
			"/presentations/:vote_key/chats",
			validateRequestMiddleware("body", createChatMessageSchema),
			verifyPresentationVotingLinkPermission,
			this.createNewChatMessageAsync.bind(this)
		);
	}

	async getPresentationChatListAsync(req: Request, res: Response, next: NextFunction) {
		try {
			// get chat history
			const presentation = req.presentation!;
			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			const { query } = req;

			const presentNo = parseInt(query.presentNo?.toString() as string);
			const page = parseInt(query.page?.toString() as string);
			const limit = parseInt(query.limit?.toString() as string);

			const [chatHistory, totalRecords] = await this._chatService.getChatHistoryAsync(
				{
					presentationSeriesId: presentation.seriesId,
					presentNo,
					orderBy: "DESC",
				},
				limit,
				page
			);

			if (totalRecords > 0 && page > Math.ceil(totalRecords / limit)) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.throw();
				return;
			}

			const data = {
				items: chatHistory,
				pagination: { totalRecords, page, limit },
				user: {
					uid: req.userinfo.uid,
					displayName: req.userinfo.display_name,
				},
			};

			res.json(DataResponseBuilder.init().withData(data).build());
		} catch (error) {
			next(error);
		}
	}

	async createNewChatMessageAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const {
				userinfo,
				body: { message },
			} = req;
			const presentation = req.presentation!;

			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			// store the message with the newest presentatio no
			const chat = await this._chatService.saveAsync({
				presentationSeriesId: presentation.seriesId,
				presentNo: presentation.pace.counter,
				userId: userinfo.uid,
				userAvatarLink: userinfo.avatar,
				userDisplayName: userinfo.display_name,
				message,
			});

			res.status(HTTP_CODE.CREATED).json(
				DataResponseBuilder.init().withResponseCode(HTTP_CODE.CREATED).withData(chat).build()
			);

			// emit socket
			ChatSocketEventHandler.instance.sendMessage(presentation.voteKey, chat);
		} catch (error) {
			next(error);
		}
	}
}
