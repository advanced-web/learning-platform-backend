import { NextFunction, Request, Response } from "express";
import _ from "lodash";
import moment from "moment";
import {
	authMiddlewareV2,
	preGetPresentationAsync,
	validateRequestMiddleware,
	verifyUserActivationMiddlewareAsync,
} from "../../common";
import { ResourcePermission } from "../../common/decorators";
import { DataResponseBuilder, HttpErrorBuilder } from "../../core";
import {
	acceptInvitationSchema,
	getCollaboratorListSchema,
	invitePresentationCollaboratorsSchema,
} from "../../core/schemas/collaborators.schema";
import { ISendCollaboratorInvitationData, SendEmailEvent } from "../../infrastructure";
import { PresentationCollaboratorService, PresentationsService, UserPermissionService } from "../../services";
import { HTTP_CODE, PRESENTATION_COLLAB_STATUS, RESOURCE_PERMISSION_TYPES, RESPONSE_CODE } from "../../shared";
import { DEFAULT_PRESENTATION_PERMISSION, PRESENTATION_PERMISSIONS } from "../../shared/permission.const";
import { BaseController } from "../base.controller";

export class CollaboratorController extends BaseController {
	private readonly _presentationService: PresentationsService;
	private readonly _presentationCollabService: PresentationCollaboratorService;
	private readonly _userPermission: UserPermissionService;

	constructor() {
		super("/api/v1/collaborators");

		this._presentationService = new PresentationsService();
		this._presentationCollabService = new PresentationCollaboratorService();
		this._userPermission = new UserPermissionService();
	}

	protected registerMiddlewares(): void {
		this._router.use(authMiddlewareV2, verifyUserActivationMiddlewareAsync);
	}

	protected registerRouterHandler(): void {
		this._router.get(
			"/",
			validateRequestMiddleware("query", getCollaboratorListSchema),
			preGetPresentationAsync("query.presentationId"),
			this.getCollaboratorListAsync.bind(this)
		);
		this._router.post(
			"/",
			validateRequestMiddleware("body", invitePresentationCollaboratorsSchema),
			this.invitePresentationCollaboratorAsync.bind(this)
		);

		this._router.post(
			"/accept-invitation",
			validateRequestMiddleware("body", acceptInvitationSchema),
			this.acceptInvitedCollabAsync.bind(this)
		);

		this._router.delete(
			"/:id",
			preGetPresentationAsync("query.presentationId"),
			this.deleteCollaboratorAsync.bind(this)
		);
	}

	// !Current solution: allows the presentation's owner only
	@ResourcePermission([PRESENTATION_PERMISSIONS.MANAGE_COLLABORATOR], (req: Request, res: Response) => {
		return { resourceId: req.presentation?.id || -1, resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION };
	})
	async getCollaboratorListAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { query } = req;
			const { limit, page } = query as any;

			// find the current presentation
			const presentation = req.presentation!!;

			const [collaboratorList, totalRecords] =
				await this._presentationCollabService.getCollaboratorListWithUserAsync(
					{
						presentationId: presentation.seriesId,
					},
					parseInt(limit),
					parseInt(page)
				);

			let dataResponse = DataResponseBuilder.init();
			if (totalRecords === 0) {
				return res.json(
					dataResponse
						.withData({ items: [], pagination: { totalRecords: totalRecords, page, limit } })
						.build()
				);
			}

			if (page > Math.ceil(totalRecords / limit)) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.build();
			}

			res.json(
				dataResponse.withData({ items: collaboratorList, pagination: { totalRecords, page, limit } }).build()
			);
		} catch (error) {
			next(error);
		}
	}

	async invitePresentationCollaboratorAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { userinfo } = req;
			const seriesId = _.get(req.body, "presentationId", "") as string;
			const emails = _.get(req.body, "emails", []) as string[];

			// find the current presentation
			const presentation = await this._presentationService.getRecordByAsync({ seriesId });
			if (!presentation) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.build();
			}

			// prevent invite the present owner
			if (emails.includes(userinfo.email)) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.CAN_NOT_INVITE_PRESENT_OWNER)
					.withErrors(userinfo.email)
					.build();
			}
			const preparedData = await this._presentationCollabService.prepareSendCollaboratorInvitationDataAsync(
				presentation.seriesId,
				emails
			);
			res.json(DataResponseBuilder.init().build());

			// Trigger email event
			preparedData.forEach((data) => {
				SendEmailEvent.instance.notify("send_collaborator_invitation", <ISendCollaboratorInvitationData>{
					to: data.email,
					invitationLink: data.invitationLink,
					presentationName: presentation.name,
				});
			});
		} catch (error) {
			next(error);
		}
	}

	async acceptInvitedCollabAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { userinfo } = req;
			const seriesId = _.get(req.body, "presentationId", "") as string;
			const invitationToken = _.get(req.body, "invitationToken", "") as string;

			// find the current presentation
			const presentation = await this._presentationService.getRecordByAsync({ seriesId });
			if (!presentation) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.build();
			}

			// ignore the owner
			if (userinfo.uid === presentation.ownerId) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_INVITE_COLLAB_TOKEN)
					.build();
			}

			const collaborator = await this._presentationCollabService.getRecordByAsync({ invitationToken });
			if (!collaborator) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_INVITE_COLLAB_TOKEN)
					.build();
			}

			// check the expiry time and malicious user (use another user access token)
			const checkResult = this._presentationCollabService.decodeInvitationToken(
				invitationToken,
				collaborator.invitationTokenKey
			);

			if (!checkResult || checkResult.email !== userinfo.email) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_INVITE_COLLAB_TOKEN)
					.build();
			}

			await Promise.allSettled([
				// update the collaborator infomation
				this._presentationCollabService.updateRecordByIdAsync(collaborator.id, {
					userId: userinfo.uid,
					status: PRESENTATION_COLLAB_STATUS.JOINED,
					joinedAt: moment().toDate(),
					invitationToken: "",
					invitationTokenKey: "",
				}),

				// assign permission for  collaborator
				this._userPermission.saveAsync({
					subjectId: userinfo.uid,
					resourceId: presentation.id,
					resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION,
					permissions: DEFAULT_PRESENTATION_PERMISSION.COLLABORATOR.join(","),
				}),
			]);

			res.json(DataResponseBuilder.init().build());
		} catch (error) {
			next(error);
		}
	}

	@ResourcePermission([PRESENTATION_PERMISSIONS.MANAGE_COLLABORATOR], (req: Request, res: Response) => {
		return { resourceId: req.presentation?.id || -1, resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION };
	})
	async deleteCollaboratorAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { id } = req.params;
			const presentation = req.presentation!!;
			const collaboratorId = parseInt(id);

			// Find the collaborator
			const collaborator = await this._presentationCollabService.getRecordByIdAsync(collaboratorId);
			if (!collaborator) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.COLLABORATOR_NOT_FOUNND)
					.build();
			}

			// Delete permission => can not edit the presentation, edit slides
			// Delete colab record
			await Promise.allSettled([
				this._userPermission.deleteByAsync({
					subjectId: collaborator.userId,
					resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION,
					resourceId: presentation.id,
				}),
				this._presentationCollabService.deleteRecordByIdAsync(collaboratorId),
			]);

			res.json(DataResponseBuilder.init().build());
		} catch (error) {
			next(error);
		}
	}
}
