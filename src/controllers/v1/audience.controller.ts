import { NextFunction, Request, Response } from "express";
import _ from "lodash";
import {
	authMiddlewareV2,
	validateRequestMiddleware,
	verifyPresentationVotingLinkPermission,
	verifyUserActivationMiddlewareAsync,
} from "../../common";
import { Logger } from "../../common/utils/logger/logger";
import {
	audienceVotingResultAsync,
	DataResponseBuilder,
	getQuestionListQuerySchema,
	HttpErrorBuilder,
	postNewQuestionBodySchema,
	QuestionReaction,
	reactQuestionBodySchema,
	voteKeyAndAdminKeyParamsSchema,
	voteKeyPathParamSchema,
} from "../../core";
import {
	PresentationQuestionService,
	QuestionReactionService,
	SlidesService,
	VotingResultsService,
} from "../../services";
import { HTTP_CODE, PRESENTATION_PACE_STATES, RESPONSE_CODE } from "../../shared";
import { BaseController } from "../base.controller";
import {
	QuestionAnswerSocketEventHandler,
	SocketPresentNamespaceCreateQuestionMessage,
	SocketPresentNamespaceToggleVoteQuestionMessage,
	SocketToggleVoteQuestionAction,
	VotingResultSocketEventHandler
} from "../../infrastructure";
import { In } from "typeorm";

export class AudienceController extends BaseController {
	private readonly _slidesService: SlidesService;
	private readonly _votingResultsService: VotingResultsService;
	private readonly _questionService: PresentationQuestionService;
	private readonly _questionReactionService: QuestionReactionService;

	constructor() {
		super("/api/v1/audience");

		this._slidesService = new SlidesService();
		this._votingResultsService = new VotingResultsService();
		this._questionService = new PresentationQuestionService();
		this._questionReactionService = new QuestionReactionService();
	}

	protected registerMiddlewares() {
		this._router.use(authMiddlewareV2, verifyUserActivationMiddlewareAsync);
	}

	protected registerRouterHandler() {
		this._router.get(
			"/presentations/:vote_key",
			validateRequestMiddleware("params", voteKeyPathParamSchema),
			verifyPresentationVotingLinkPermission,
			this.getPresentationAsync.bind(this)
		);

		this._router.get(
			"/presentations/:vote_key/slides/:admin_key",
			validateRequestMiddleware("params", voteKeyAndAdminKeyParamsSchema),
			verifyPresentationVotingLinkPermission,
			this.getSlideAsync.bind(this)
		);

		this._router.put(
			"/presentations/:vote_key/slides/:admin_key",
			validateRequestMiddleware("body", audienceVotingResultAsync),
			verifyPresentationVotingLinkPermission,
			this.updateAudienceVotingResultAsync.bind(this)
		);

		this._router.get(
			"/presentations/:vote_key/question-answers",
			validateRequestMiddleware("query", getQuestionListQuerySchema),
			verifyPresentationVotingLinkPermission,
			this.getQuestionListAsync.bind(this)
		);

		this._router.post(
			"/presentations/:vote_key/question-answers",
			validateRequestMiddleware("body", postNewQuestionBodySchema),
			verifyPresentationVotingLinkPermission,
			this.postNewQuestionAsync.bind(this)
		);

		this._router.put(
			"/presentations/:vote_key/question-answers/:question_id/reactions",
			validateRequestMiddleware("body", reactQuestionBodySchema),
			verifyPresentationVotingLinkPermission,
			this.reactQuestionAsync.bind(this)
		);
	}

	async getPresentationAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { vote_key } = request.params;

			Logger.debug(`Presentation vote_key ${vote_key}`);

			// Because the `verifyPresentationVotingLinkPermission` has passed
			const presentation = request.presentation!;

			const fetchedSlideFields = ["id", "admin_key", "type"];
			const slides = await this._slidesService.getRawSlidesBySeriesId(presentation.seriesId, fetchedSlideFields);

			const returnedData = {
				name: presentation.name,
				vote_key: presentation.voteKey,
				pace: _.omit(presentation.pace, ["scope", "groupId"]),
				closed_for_voting: presentation.closedForVoting,
				slide_count: presentation.slideCount,
				slides,
			};
			const result = DataResponseBuilder.init().withData(returnedData).build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	async getSlideAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { admin_key, vote_key } = request.params;
			Logger.debug(`admin_key vote_key ${admin_key} ${vote_key}`);

			// Because the `verifyPresentationVotingLinkPermission` has passed
			const presentation = request.presentation!;

			// Fetchs one slide by admin_key and series_id
			const slide = await this._slidesService.getRecordByAsync({
				adminKey: admin_key,
				presentationSeriesId: presentation.seriesId,
			});

			// Slide is not found/not belongs to the presentation
			if (!slide) {
				Logger.error("Slide is not found/not belongs to the presentation");

				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.throw();
				return;
			}

			const choiceWithoutCorrectAnswers = slide.choices.map((choice) => _.omit(choice, "correctAnswer"));
			const audienceSlideFields = [
				"id",
				"presentationId",
				"presentationSeriesId",
				"speakerNotes",
				"createdAt",
				"updatedAt",
			];
			const slideForAudience = _.omit(slide, audienceSlideFields);

			const result = DataResponseBuilder.init()
				.withStatusCode(HTTP_CODE.OK)
				.withData({ ...slideForAudience, choices: choiceWithoutCorrectAnswers })
				.build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	async updateAudienceVotingResultAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid, display_name } = request.userinfo;
			const { admin_key, vote_key } = request.params;
			const { answer } = request.body;
			Logger.debug(`admin_key vote_key answer ${admin_key} ${vote_key} ${answer}`);

			// Because the `verifyPresentationVotingLinkPermission` has passed
			const presentation = request.presentation!;
			const closedForVoting = presentation.closedForVoting;
			const { state: presentationState, active: presentingAdminKey } = presentation.pace;

			if (
				closedForVoting ||
				presentationState === PRESENTATION_PACE_STATES.IDLE ||
				(presentationState === PRESENTATION_PACE_STATES.PRESENTING && presentingAdminKey !== admin_key)
			) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.CLOSED_VOTING)
					.throw();
				return;
			}

			// Fetchs one slide by admin_key and series_id
			const slide = await this._slidesService.getRecordByAsync({
				adminKey: admin_key,
				presentationSeriesId: presentation.seriesId,
			});

			// Slide is not found/not belongs to the presentation
			if (!slide) {
				Logger.error("Slide is not found/not belongs to the presentation");

				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.throw();
				return;
			}

			const isVoted = await this._votingResultsService.existsByAsync({ slideAdminKey: admin_key, userId: uid });
			if (isVoted) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.EXCEED_VOTING_SUBMISSION)
					.throw();
				return;
			}

			const answerIndex = slide.choices.findIndex((choice) => choice.id === answer);
			if (answerIndex === -1) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.ANSWER_NOT_FOUND)
					.throw();
				return;
			}

			await this._votingResultsService.saveAsync({
				slideId: slide.id,
				slideAdminKey: slide.adminKey,
				userId: uid,
				userDisplayName: display_name,
				answerId: answer,
			});

			// Send response
			response.sendStatus(HTTP_CODE.NO_CONTENT);

			// Emit socket event
			const voteKey = presentation.voteKey;
			VotingResultSocketEventHandler.instance.sendResult(voteKey, slide.id, slide.adminKey, slide.choices);
		} catch (error) {
			next(error);
		}
	}


	// async updateAudienceVotingResultAsync(request: Request, response: Response, next: NextFunction) {
	// 	try {
	// 		const { admin_key, vote_key } = request.params;
	// 		const { answer } = request.body;
	// 		Logger.debug(`admin_key vote_key answer ${admin_key} ${vote_key} ${answer}`);

	// 		// Because the `verifyPresentationVotingLinkPermission` has passed
	// 		const presentation = request.presentation!;

	// 		// Fetchs one slide by admin_key and series_id
	// 		const slide = await this._slidesService.getRecordByAsync({
	// 			adminKey: admin_key,
	// 			presentationSeriesId: presentation.seriesId,
	// 		});

	// 		// Slide is not found/not belongs to the presentation
	// 		if (!slide) {
	// 			Logger.error("Slide is not found/not belongs to the presentation");

	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
	// 				.throw();
	// 			return;
	// 		}

	// 		const votingResult = await this._votingResultsService.getRecordByAsync({ slideAdminKey: admin_key });
	// 		if (!votingResult) {
	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.VOTING_RESULT_NOT_FOUND)
	// 				.throw();
	// 			return;
	// 		}

	// 		const resultIndex = votingResult.results.findIndex(item => item.id === answer);
	// 		if (resultIndex === -1) {
	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.ANSWER_NOT_FOUND)
	// 				.throw();
	// 			return;
	// 		}

	// 		let newResults = [...votingResult.results];
	// 		newResults[resultIndex].score[0] = newResults[resultIndex].score[0] + 1;
	// 		const newRespondents = votingResult.respondents + 1;

	// 		// Updates voting result
	// 		await this._votingResultsService.updateRecordByIdAsync(
	// 			votingResult.id,
	// 			{ results: newResults, respondents: newRespondents },
	// 		);

	// 		return response.sendStatus(HTTP_CODE.NO_CONTENT);
	// 	} catch (error) {
	// 		next(error);
	// 	}
	// }

	async postNewQuestionAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { userinfo, body } = req;
			const presentation = req.presentation!;
			const question = body.question as string;

			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			// insert new question
			const insertedQuestion = await this._questionService.saveAsync({
				presentationSeriesId: presentation.seriesId,
				question,
				totalLike: 0,
				userId: userinfo.uid,
				userDisplayName: userinfo.display_name,
				userAvatarLink: userinfo.avatar,
			});

			res.status(HTTP_CODE.CREATED).json(
				DataResponseBuilder.init().withData(insertedQuestion).withStatusCode(HTTP_CODE.CREATED).build()
			);

			// Emit socket event
			const message: SocketPresentNamespaceCreateQuestionMessage = {
				action: "create",
				id: insertedQuestion.id,
				presentationSeriesId: insertedQuestion.presentationSeriesId,
				totalLike: insertedQuestion.totalLike,
				isArchive: insertedQuestion.isArchive,
				userId: insertedQuestion.userId,
				userDisplayName: insertedQuestion.userDisplayName,
				question: insertedQuestion.question,
				createdAt: insertedQuestion.createdAt.toISOString(),
				archivedAt: insertedQuestion.archivedAt ? insertedQuestion.archivedAt.toISOString() : null,
			};
			QuestionAnswerSocketEventHandler.instance.createQuestion(presentation.voteKey, message);
		} catch (error) {
			next(error);
		}
	}

	async getQuestionListAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { query } = req;
			const { page, limit, sortByTime, sortByTotal, isArchive } = query as any;
			const presentation = req.presentation!;

			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			const [questionList, totalRecords] = await this._questionService.getQuestionListAsync(
				{
					presentationSeriesId: presentation.seriesId,
					isArchive,
					orderByTime: sortByTime,
					orderByTotal: sortByTotal,
				},
				limit,
				page
			);

			if (totalRecords > 0 && page > Math.ceil(totalRecords / limit)) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.build();
			}

			// !must be refactored: use getManyRecordsByIdsAsync()
			const questionIds = questionList.map(q => q.id);
			const reactions = await this._questionReactionService.getManyRecordsAsync({
				where: {
					questionId: In(questionIds),
					userId: req.userinfo.uid,
				},
			});

			const questionWithReactionList = questionList.map(q => {
				const isVoted = reactions.findIndex(r => r.questionId === q.id) !== -1;
				return { ...q, isVoted };
			});

			res.json(
				DataResponseBuilder.init()
					.withData({
						items: questionWithReactionList,
						pagination: { totalRecords, page, limit },
						user: {
							uid: req.userinfo.uid,
							displayName: req.userinfo.display_name,
						},
					})
					.build()
			);
		} catch (error) {
			next(error);
		}
	}

	async reactQuestionAsync(req: Request, res: Response, next: NextFunction) {
		try {
			// ensure the presentation is presenting
			const presentation = req.presentation!;
			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			const { params, body } = req;

			const questionId = _.get(params, "question_id", 0) as number;
			const reaction = _.get(body, "reaction", "") as string;
			const actionType = _.get(body, "actionType", "") as string;

			const question = await this._questionService.getRecordByIdAsync(questionId);
			if (!question || question.presentationSeriesId !== presentation.seriesId) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.QUESTION_NOT_FOUND)
					.build();
			}

			// must find the reaction
			const isReactionExisted = await this._questionReactionService.existsByAsync({
				questionId,
				userId: req.userinfo.uid,
				reactionType: reaction,
			});

			// do the login based on actionType
			const error = HttpErrorBuilder.init().withStatusCode(HTTP_CODE.BAD_REQUEST);
			const reactionRecord = {
				questionId,
				userId: req.userinfo.uid,
				reactionType: reaction,
			} as QuestionReaction;

			if (actionType === "up") {
				if (isReactionExisted) {
					throw error.withResponseCode(RESPONSE_CODE.QUESTION_REACTED).build();
				}

				await this._questionReactionService.saveAsync(reactionRecord);

				// !must replace the following statement by trigger
				await this._questionService.updateRecordByIdAsync(questionId, { totalLike: question.totalLike + 1 });

				// Emit socket event
				const voteKey = presentation.voteKey;
				const message: Omit<SocketPresentNamespaceToggleVoteQuestionMessage, "action"> = {
					id: question.id,
					totalLike: question.totalLike + 1,
					isArchive: question.isArchive,
					archivedAt: question.archivedAt ? question.archivedAt.toISOString() : null
				};
				QuestionAnswerSocketEventHandler.instance.toggleVoteQuestion("up_vote", voteKey, message);
			} else if (actionType === "down") {
				if (!isReactionExisted) {
					throw error.withResponseCode(RESPONSE_CODE.QUESTION_NOT_REACTED).build();
				}

				await this._questionReactionService.deleteByAsync({ ...reactionRecord });

				// !must replace the following statement by trigger
				await this._questionService.updateRecordByIdAsync(questionId, { totalLike: question.totalLike - 1 });

				// Emit socket event
				const voteKey = presentation.voteKey;
				const message: Omit<SocketPresentNamespaceToggleVoteQuestionMessage, "action"> = {
					id: question.id,
					totalLike: question.totalLike - 1,
					isArchive: question.isArchive,
					archivedAt: question.archivedAt ? question.archivedAt.toISOString() : null
				};
				QuestionAnswerSocketEventHandler.instance.toggleVoteQuestion("down_vote", voteKey, message);
			}

			res.json(DataResponseBuilder.init().build());
		} catch (error) {
			next(error);
		}
	}
}
