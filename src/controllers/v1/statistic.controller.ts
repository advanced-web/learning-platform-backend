import { NextFunction, Request, Response } from "express";
import { In } from 'typeorm';
import { authMiddlewareV2, validateRequestMiddleware, verifyUserActivationMiddlewareAsync } from "../../common";
import {
	DataResponseBuilder,
	HttpErrorBuilder,
	Presentations,
	SlideSummaryResponseData,
	getPresentationSummaryQuerySchema,
	presentationSeriesIdAndAdminKeyStatisticSchema,
	presentationSeriesIdStatisticSchema,
} from "../../core";
import {
	LearningGroupMemberService,
	PresentationsService,
	SlidesService,
	VotingResultsService,
} from '../../services';
import {
	GROUP_MEMBER_ACTIVITY_KEYS,
	GROUP_MEMBER_ROLE,
	HTTP_CODE,
	PRESENTATION_PACE_SCOPES,
	PRESENTATION_PACE_STATES,
	RESPONSE_CODE,
} from '../../shared';
import { BaseController } from "../base.controller";

export class StatisticController extends BaseController {
	private _presentationService: PresentationsService;
	private _slideService: SlidesService;
	private _votingResultService: VotingResultsService;
	private _groupMemberService: LearningGroupMemberService;

	constructor() {
		super("/api/v1/statistics");

		this._presentationService = new PresentationsService();
		this._slideService = new SlidesService();
		this._votingResultService = new VotingResultsService();
		this._groupMemberService = new LearningGroupMemberService();
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}

	protected registerRouterHandler(): void {
		this._router.get(
			"/presentations/:series_id",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("params", presentationSeriesIdStatisticSchema),
			this.getPresentationSummaryAsync.bind(this),
		);

		this._router.get(
			"/presentations/:series_id/slides/:admin_key",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			validateRequestMiddleware("params", presentationSeriesIdAndAdminKeyStatisticSchema),
			validateRequestMiddleware("query", getPresentationSummaryQuerySchema),
			this.getSlideSummaryAsync.bind(this),
		);
	}

	async getPresentationSummaryAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid: userId } = request.userinfo;
			const { series_id: seriesId } = request.params;

			const presentation = await this._presentationService.getRecordByAsync({ seriesId });
			if (!presentation) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.throw();
				return;
			}

			// check view voting result permission
			await this._checkViewVotingResultPermission(userId, presentation);

			// get presentation and list of slides
			const slides = await this._slideService.getManyRecordsAsync({
				select: ["id", "adminKey", "type"],
				where: { presentationSeriesId: seriesId },
				order: { position: "ASC" },
			});

			const slideIds = slides.map(slide => slide.id);
			const votingCount = await this._votingResultService.countByAsync({ slideId: In(slideIds) });
			const participantCount = votingCount; // re-caculate it if a user can vote in multiple times

			const result = DataResponseBuilder.init()
				.withData({
					...presentation,
					votingCount,
					participantCount,
					slides,
				})
				.build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	async getSlideSummaryAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { uid: userId } = request.userinfo;
			const { limit, page } = request.query as any;
			const { series_id: seriesId, admin_key: adminKey } = request.params;

			const presentation = await this._presentationService.getRecordByAsync({ seriesId });
			if (!presentation) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.throw();
				return;
			}

			// check view voting result permission
			await this._checkViewVotingResultPermission(userId, presentation);

			const slide = await this._slideService.getRecordByAsync({ adminKey });
			if (!slide) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.throw();
				return;
			}

			const [votingResults, votingResultCount] = await this._votingResultService.getResultsBySlideIdAndCountByAnswerId(
				slide.id,
				parseInt(limit?.toString() as string),
				parseInt(page?.toString() as string),
			);

			if (votingResultCount !== 0 && page > Math.ceil(votingResultCount / limit)) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.throw();
				return;
			}

			// map list of voting results with slide
			const mappedVotingResults: SlideSummaryResponseData[] = votingResults.map(votingResult => {
				const choice = slide.choices.find(choice => choice.id === votingResult.answerId);

				return {
					userId: votingResult.userId,
					userDisplayName: votingResult.userDisplayName,
					answerId: votingResult.answerId,
					answer: choice?.label ?? "",
					votedAt: votingResult.votedAt,
				};
			});

			const result = DataResponseBuilder.init()
				.withData({
					items: mappedVotingResults,
					pagination: {
						totalRecords: votingResultCount,
						page,
						limit,
					},
				})
				.build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	private async _checkViewVotingResultPermission(userId: number, presentation: Presentations) {
		// the presentation is presenting in public
		// and you isn't an its owner
		if (presentation.pace.scope === PRESENTATION_PACE_SCOPES.PUBLIC && presentation.ownerId !== userId) {
			HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.CAN_NOT_VIEW_VOTING_RESULT)
				.throw();
			return;
		}

		// the presentation is presenting in a group
		// and you don't have a permission to view the voting result
		// allowed role: owner, co-owner
		if (
			presentation.pace.state === PRESENTATION_PACE_STATES.PRESENTING
			&& presentation.pace.scope === PRESENTATION_PACE_SCOPES.GROUP
		) {
			const groupId = presentation.pace.groupId ?? 0;
			const isValidRole = await this._groupMemberService.existsByStatusListAndRoleListAsync(
				groupId,
				userId,
				[GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP, GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP],
				[GROUP_MEMBER_ROLE.OWNER, GROUP_MEMBER_ROLE.CO_OWNER],
			);

			if (!isValidRole) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.CAN_NOT_VIEW_VOTING_RESULT)
					.throw();
				return;
			}
		}
	}
}
