import { NextFunction, Request, Response } from "express";
import { ValidationErrorItem } from "joi";
import _ from "lodash";
import { In } from "typeorm";
import {
	authMiddlewareV2,
	mapPresentationRole,
	preGetPresentationAsync,
	TypeConverter,
	validateRequestMiddleware,
	ValidationHandler,
	verifyUserActivationMiddlewareAsync,
} from "../../common";
import { ResourcePermission } from "../../common/decorators";
import { Logger } from "../../common/utils/logger/logger";
import {
	createSlideSchema,
	DataResponseBuilder,
	HttpErrorBuilder,
	IBaseSlideChoice,
	presentationSeriesIdPathParamSchema,
	seriesIdAndAdminKeyParamsSchema,
	SimpleVotingResultsResponse,
	UpdateSlideBody,
	updateSlideSchema,
} from "../../core";
import { SlidesService, VotingResultsService } from "../../services";
import {
	HTTP_CODE,
	PRESENTATION_PACE_STATES,
	PRESENTATION_PERMISSIONS,
	RESOURCE_PERMISSION_TYPES,
	RESPONSE_CODE,
	SLIDE_TYPE,
} from "../../shared";
import { BaseController } from "../base.controller";

export class SlidesController extends BaseController {
	private _slidesService: SlidesService;
	private _votingResultsService: VotingResultsService;

	constructor() {
		super("/api/v1");

		this._slidesService = new SlidesService();
		this._votingResultsService = new VotingResultsService();
	}

	protected registerMiddlewares() {
		this._router.use(authMiddlewareV2, verifyUserActivationMiddlewareAsync);
	}

	protected registerRouterHandler() {
		this._router.post(
			"/presentations/:series_id/slides/",
			validateRequestMiddleware("params", presentationSeriesIdPathParamSchema),
			validateRequestMiddleware("body", createSlideSchema),
			preGetPresentationAsync("params.series_id"),
			this.createSlideAsync.bind(this)
		);

		this._router.get(
			"/presentations/:series_id/slides/:admin_key",
			validateRequestMiddleware("params", seriesIdAndAdminKeyParamsSchema),
			preGetPresentationAsync("params.series_id"),
			mapPresentationRole,
			this.getSlideDetailByKeyAsync.bind(this)
		);

		this._router.get(
			"/presentations/:series_id/slides/:admin_key/result",
			validateRequestMiddleware("params", seriesIdAndAdminKeyParamsSchema),
			preGetPresentationAsync("params.series_id"),
			mapPresentationRole,
			this.getVotingResultAsync.bind(this)
		);

		this._router.put(
			"/presentations/:series_id/slides/:admin_key",
			validateRequestMiddleware("params", seriesIdAndAdminKeyParamsSchema),
			validateRequestMiddleware("body", updateSlideSchema),
			preGetPresentationAsync("params.series_id"),
			this.updateSlideAsync.bind(this)
		);

		this._router.delete(
			"/presentations/:series_id/slides/:admin_key",
			validateRequestMiddleware("params", seriesIdAndAdminKeyParamsSchema),
			preGetPresentationAsync("params.series_id"),
			this.deleteSlideAsync.bind(this)
		);

		// this._router.put(
		// 	"/presentations/:series_id/slides/:admin_key/:action",
		// 	authMiddlewareV2,
		// 	verifyUserActivationMiddlewareAsync,
		// 	validateRequestMiddleware("params", performSlideActionParamsSchema),
		// 	this.performSlideActionAsync.bind(this),
		// );
	}

	/**
	 * @access only presentation owner and collaborator
	 */
	// * REVISE permission RLP-175 - 2022-12-29
	@ResourcePermission([PRESENTATION_PERMISSIONS.EDIT], (req: Request, res: Response) => {
		return { resourceId: req.presentation!!.id, resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION };
	})
	async createSlideAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { type } = request.body as { type: SLIDE_TYPE };
			const presentation = request.presentation!!;

			// The presentation is not found or presenting
			if (presentation.pace.state === PRESENTATION_PACE_STATES.PRESENTING) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTING_PRESENTATION)
					.build();
			}

			const createdSlide = await this._slidesService.createSlideAndUpdatePresentation(type, presentation);
			const result = DataResponseBuilder.init().withStatusCode(HTTP_CODE.CREATED).withData(createdSlide).build();

			return response.status(HTTP_CODE.CREATED).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access owner and collab of a presentation
	 * @access owner and co-owner of a group
	 */
	// * REVISE permission RLP-175 - 2022-12-29
	async getSlideDetailByKeyAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { series_id, admin_key } = request.params;
			const mappedRole = request.presentationRoleMap!!;

			if (!mappedRole.groupRole && !mappedRole.presentationRole) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION)
					.build();
			}

			// Fetchs one slide by admin_key and series_id
			const slide = await this._slidesService.getRecordByAsync({
				adminKey: admin_key,
				presentationSeriesId: series_id,
			});

			// Slide is not found/not belongs to the presentation
			if (!slide) {
				Logger.error("Slide is not found/not belongs to the presentation");

				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.throw();
				return;
			}

			const result = DataResponseBuilder.init().withStatusCode(HTTP_CODE.OK).withData(slide).build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access owner and collab of a presentation
	 * @access owner and co-owner of a group
	 */
	// * REVISE permission RLP-175 - 2022-12-29
	async getVotingResultAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { admin_key } = request.params;
			const presentation = request.presentation!!;

			const mappedRole = request.presentationRoleMap!!;

			if (!mappedRole.groupRole && !mappedRole.presentationRole) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION);
			}

			// Fetchs one slide by admin_key and series_id
			const slide = await this._slidesService.getRecordByAsync({
				adminKey: admin_key,
				presentationSeriesId: presentation.seriesId,
			});

			// Slide is not found/not belongs to the presentation
			if (!slide) {
				Logger.error("Slide is not found/not belongs to the presentation");
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.build();
			}

			const votingResults = await this._votingResultsService.getResultsByAdminKeyAndCountByAnswerId(admin_key);
			const returnedData: SimpleVotingResultsResponse = {
				slideId: slide.id,
				slideAdminKey: slide.adminKey,
				respondents: 0,
				results: [],
			};

			// maps list of choices to list of voting results
			slide.choices.map((choice) => {
				const votingResult = votingResults.find((element) => element.answerId === choice.id);
				const answerCount = votingResult ? Number(votingResult.answerCount) : 0;

				returnedData.respondents += answerCount;
				returnedData.results.push({ id: choice.id, label: choice.label, score: [answerCount] });
			});

			const result = DataResponseBuilder.init().withStatusCode(HTTP_CODE.OK).withData(returnedData).build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access owner and collab of a presentation
	 */
	// * REVISE permission RLP-175 - 2022-12-29
	@ResourcePermission([PRESENTATION_PERMISSIONS.MANAGE_SLIDES], (req: Request, res: Response) => {
		return { resourceId: req.presentation?.id || -1, resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION };
	})
	async updateSlideAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { admin_key } = request.params;
			const requestBody: UpdateSlideBody = request.body;

			// Ensure the requested slide is existed
			const currentSlide = await this._slidesService.getRecordByAsync({ adminKey: admin_key });
			if (!currentSlide) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.throw();
				return;
			}

			// Ensure that the slide type is always valid
			const slideTypeEnum = TypeConverter.instance.stringToSlideTypeEnum(requestBody.type)!;
			const validationErrors = this._validateSpecificSlideProperties(
				requestBody.question,
				requestBody.questionDescription,
				requestBody.choices,
				slideTypeEnum
			);

			if (validationErrors.length > 0) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.VALIDATION_ERROR)
					.withErrors(validationErrors)
					.throw();
				return;
			}

			const presentation = request.presentation!!;

			// The presentation is not found
			// The current slide is presenting
			if (
				presentation.pace.state === PRESENTATION_PACE_STATES.PRESENTING &&
				presentation.pace.active === admin_key
			) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTING_PRESENTATION)
					.build();
			}

			const slideType = requestBody.type;
			const hasVotingResults = await this._votingResultsService.existsByAsync({ slideAdminKey: admin_key });
			if (slideType !== currentSlide.type && hasVotingResults) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.EDIT_VOTED_SLIDE_PERMISSION)
					.throw();
				return;
			}

			const newChoiceIds = requestBody.choices.map((choice) => choice.id);
			const currentChoiceIds = currentSlide.choices.map((choice) => choice.id);
			const deletedChoiceIds = _.difference(currentChoiceIds, newChoiceIds);
			if (deletedChoiceIds.length > 0) {
				if (hasVotingResults) {
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.FORBIDDEN)
						.withResponseCode(RESPONSE_CODE.EDIT_VOTED_SLIDE_PERMISSION)
						.throw();
					return;
				}

				await this._votingResultsService.deleteByAsync({
					slideId: currentSlide.id,
					answerId: In(deletedChoiceIds),
				});
			}

			// Updates slide
			const omitableFields = ["id", "adminKey", "presentationId", "presentationSeriesId", "position"];
			const bodyToUpdate = _.omit(requestBody, omitableFields);
			await this._slidesService.updateRecordByIdAsync(currentSlide.id, bodyToUpdate);

			const updatedSlide = await this._slidesService.getRecordByIdAsync(currentSlide.id);
			const result = DataResponseBuilder.init().withStatusCode(HTTP_CODE.OK).withData(updatedSlide).build();

			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	// async updateSlideAsync(request: Request, response: Response, next: NextFunction) {
	// 	try {
	// 		const { uid } = request.user;
	// 		const { series_id, admin_key } = request.params;
	// 		const requestBody: UpdateSlideBody = request.body;

	// 		Logger.debug(`series_id admin_key ${series_id} ${admin_key}`);

	// 		// Ensure that the slide type is always valid
	// 		const slideTypeEnum = TypeConverter.instance.stringToSlideTypeEnum(requestBody.type)!;
	// 		const validationErrors = this._validateSpecificSlideProperties(
	// 			requestBody.question,
	// 			requestBody.questionDescription,
	// 			requestBody.choices,
	// 			slideTypeEnum,
	// 		);

	// 		if (validationErrors.length > 0) {
	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.VALIDATION_ERROR)
	// 				.withErrors(validationErrors)
	// 				.throw();
	// 			return;
	// 		}

	// 		const presentation = await this._presentationsService.getRecordByAsync({
	// 			seriesId: series_id,
	// 			ownerId: uid,
	// 		});

	// 		// Presentation not found
	// 		if (!presentation) {
	// 			Logger.error("Presentation not found");

	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
	// 				.throw();
	// 			return;
	// 		}

	// 		const votingResult = await this._votingResultsService.getRecordByAsync({ slideAdminKey: admin_key });
	// 		if (!votingResult) {
	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
	// 				.throw();
	// 		} else {
	// 			const choices = requestBody.choices;
	// 			const newSlideVotingResults: SlideVotingResult[] = choices.map(choice => {
	// 				return {
	// 					id: choice.id,
	// 					label: choice.label,
	// 					score: votingResult.results.find(item => item.id === choice.id)?.score ?? [0],
	// 				};
	// 			});

	// 			// Updates slide
	// 			const omitableFields = ["id", "presentationId", "presentationSeriesId", "position"];
	// 			const bodyToUpdate = _.omit(requestBody, omitableFields);
	// 			await this._slidesService.updateRecordByIdAsync(requestBody.id, bodyToUpdate);

	// 			// Updates slide's voting results
	// 			await this._votingResultsService.updateRecordByIdAsync(
	// 				votingResult.id,
	// 				{ results: newSlideVotingResults },
	// 			);

	// 			const updatedSlide = await this._slidesService.getRecordByIdAsync(requestBody.id);
	// 			const result = DataResponseBuilder.init()
	// 				.withStatusCode(HTTP_CODE.OK)
	// 				.withData(updatedSlide)
	// 				.build();

	// 			return response.status(HTTP_CODE.OK).json(result);
	// 		}
	// 	} catch (error) {
	// 		next(error);
	// 	}
	// }

	/**
	 * @access owner and collab of a presentation
	 */
	// * REVISE permission RLP-175 - 2022-12-29
	@ResourcePermission([PRESENTATION_PERMISSIONS.MANAGE_SLIDES], (req: Request, res: Response) => {
		return { resourceId: req.presentation?.id || -1, resourceType: RESOURCE_PERMISSION_TYPES.PRESENTATION };
	})
	async deleteSlideAsync(request: Request, response: Response, next: NextFunction) {
		try {
			const { admin_key } = request.params;
			const presentation = request.presentation!!;

			// The presentation is not found or presenting
			if (presentation.pace.state === PRESENTATION_PACE_STATES.PRESENTING) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTING_PRESENTATION)
					.build();
			}

			const deleteResult = await this._slidesService.deleteByAdminKeyAndPresentationInstance(
				admin_key,
				presentation
			);

			if (!deleteResult?.affected) {
				Logger.error("Slide not found");

				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
					.throw();
				return;
			}

			const result = DataResponseBuilder.init().withStatusCode(HTTP_CODE.OK).build();
			return response.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	// async performSlideActionAsync(request: Request, response: Response, next: NextFunction) {
	// 	try {
	// 		const { uid } = request.user;
	// 		const { series_id, admin_key, action } = request.params;

	// 		Logger.debug(`series_id adminKey action ${series_id} ${admin_key} ${action}`);

	// 		const presentation = await this._presentationsService.getRecordByAsync({
	// 			seriesId: series_id,
	// 			ownerId: uid,
	// 		});

	// 		// Presentation not found
	// 		if (!presentation) {
	// 			Logger.error("Presentation not found");

	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
	// 				.throw();
	// 			return;
	// 		}

	// 		let { scope, groupId } = presentation.pace;

	// 		if (action === PRESENTATION_PACE_ACTIONS.QUIT) {
	// 			scope = "public";
	// 			groupId = null;
	// 		} else if (action === PRESENTATION_PACE_ACTIONS.PRESENT) {
	// 			const validationErrors = this._validatePresentationPacePermission(request.body);
	// 			if (validationErrors.length > 0) {
	// 				HttpErrorBuilder.init()
	// 					.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 					.withResponseCode(RESPONSE_CODE.VALIDATION_ERROR)
	// 					.withErrors(validationErrors)
	// 					.throw();
	// 				return;
	// 			}

	// 			scope = request.body.scope;
	// 			groupId = request.body.groupId; // number

	// 			// Validate group owner/co-owner
	// 			const isValidRole = await this._learningGroupMemberService.existsByAsync({
	// 				groupId: groupId!,
	// 				userProfileId: uid,
	// 				role: In([GROUP_MEMBER_ROLE.OWNER, GROUP_MEMBER_ROLE.CO_OWNER]),
	// 			});

	// 			if (!isValidRole) {
	// 				HttpErrorBuilder.init()
	// 					.withStatusCode(HTTP_CODE.FORBIDDEN)
	// 					.withResponseCode(RESPONSE_CODE.PRESENT_SLIDE_PERMISSION)
	// 					.throw();
	// 				return;
	// 			}
	// 		}

	// 		const isExistedSlide = await this._presentationsService.hasSlideBySeriesIdAndAdminKey(series_id, admin_key);
	// 		if (!isExistedSlide) {
	// 			Logger.error("Slide is not found/not belongs to the presentation");

	// 			HttpErrorBuilder.init()
	// 				.withStatusCode(HTTP_CODE.BAD_REQUEST)
	// 				.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
	// 				.throw();
	// 			return;
	// 		}

	// 		// Infers the pace state and the present count from the action
	// 		let { state: paceState, counter: presentCount } = presentation.pace;
	// 		switch (action) {
	// 			case PRESENTATION_PACE_ACTIONS.PRESENT: {
	// 				presentCount += 1;
	// 				paceState = PRESENTATION_PACE_STATES.PRESENTING;
	// 				break;
	// 			}
	// 			case PRESENTATION_PACE_ACTIONS.QUIT: {
	// 				paceState = PRESENTATION_PACE_STATES.IDLE;
	// 				break;
	// 			}
	// 		}

	// 		// Creates new pace
	// 		const newPace: PresentationPace = {
	// 			...presentation.pace,
	// 			active: admin_key,
	// 			state: paceState,
	// 			counter: presentCount,
	// 			scope,
	// 			groupId,
	// 		}

	// 		// Updates presentation's pace
	// 		await this._presentationsService.updateRecordByIdAsync(presentation.id, { pace: newPace });

	// 		return response.sendStatus(HTTP_CODE.NO_CONTENT);
	// 	} catch (error) {
	// 		next(error);
	// 	}
	// }

	private _validateSpecificSlideProperties(
		question: string,
		questionDescription: string,
		choices: IBaseSlideChoice[],
		slideType: SLIDE_TYPE
	) {
		const validationErrors: ValidationErrorItem[] = [];

		if (!ValidationHandler.instance.validateQuestionLength(slideType, question)) {
			validationErrors.push({
				message: "Tên câu hỏi/tiêu đề không được vượt quá số lượng ký tự cho phép",
				path: ["question"],
				type: "string.max",
				context: {
					label: "question",
					value: question,
					key: "question",
				},
			});
		}

		if (!ValidationHandler.instance.validateQuestionDescriptionLength(slideType, questionDescription)) {
			validationErrors.push({
				message: "Mô tả không được vượt quá số lượng ký tự cho phép",
				path: ["questionDescription"],
				type: "string.max",
				context: {
					label: "questionDescription",
					value: question,
					key: "questionDescription",
				},
			});
		}

		const choicesValidationErrorItems = ValidationHandler.instance.validateSlideChoiceListStructure(
			slideType,
			choices
		);
		validationErrors.push(...choicesValidationErrorItems);

		return validationErrors;
	}

	// private _validatePresentationPacePermission(data: PerformSlideActionBody) {
	// 	const validationErrors = ValidationHandler.instance.validatePresentationPacePermissionStructure(data);
	// 	const { scope, groupId } = data;

	// 	if (!ValidationHandler.instance.validatePresentationPacePermission(scope, groupId)) {
	// 		validationErrors.push({
	// 			message: "Có lỗi xảy ra trong quá trình xử lý yêu cầu",
	// 			path: ["scope", "groupId"],
	// 			type: "",
	// 			context: {
	// 				label: "",
	// 				value: { scope, groupId },
	// 				key: "",
	// 			},
	// 		});
	// 	}

	// 	return validationErrors;
	// }
}
