import { NextFunction, Request, Response } from "express";
import { DataResponse, HttpErrorBuilder } from "../../core";
import { AuthService } from "../../services";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";
import { BaseController } from "../base.controller";

/**
 * @deprecated
 */
export class AuthControllerV1 extends BaseController {
	private readonly _authService: AuthService;

	constructor() {
		super("/api/v1/auth");

		this._authService = new AuthService();
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}
	protected registerRouterHandler(): void {
		this._router.get("/login", this.getLoginUrl.bind(this));
		this._router.post("/token", this.getLoginTokenAsync.bind(this));
		this._router.get("/userinfo", this.getUserLoginInformation.bind(this));
	}

	/**
	 * @deprecated
	 */
	getLoginUrl(req: Request, res: Response, next: NextFunction) {
		try {
			const url = this._authService.getLoginURI();
			const data = new DataResponse(200, "OK", url);
			res.json(data.toJSON());
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @deprecated
	 */
	async getLoginTokenAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { code } = req.body;
			const dataToken = await this._authService.obtainTokenDataAsync(code);
			res.json(new DataResponse(200, "", dataToken));
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @deprecated
	 */
	async getUserLoginInformation(req: Request, res: Response, next: NextFunction) {
		try {
			const headers = req.headers;
			const authorization = headers.authorization;
			if (!authorization) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.UNAUTHORIZED)
					.withResponseCode(RESPONSE_CODE.MISSING_TOKEN)
					.throw();
				return;
			}

			const token = authorization.split(" ")[1];
			if (!token) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.UNAUTHORIZED)
					.withResponseCode(RESPONSE_CODE.MISSING_TOKEN)
					.throw();
				return;
			}

			const userinfo = await this._authService.getUserInformationAsync(token);
			res.json(new DataResponse(200, "OK", userinfo).toJSON());
		} catch (error) {
			next(error);
		}
	}
}
