import { NextFunction, Request, Response } from "express";
import {
	authMiddlewareV2,
	multerMiddleware,
	validateRequestMiddleware,
	verifyUserActivationMiddlewareAsync
} from "../../common";
import { Logger } from "../../common/utils/logger/logger";
import { DataResponseBuilder, HttpErrorBuilder, UpdateProfileBody, updateProfileBodySchema } from "../../core";
import { ImgBBService } from "../../infrastructure";
import { ResUserProfileService } from "../../services";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";
import { BaseController } from "../base.controller";

export class ResUserProfileController extends BaseController {
	private readonly _resUserProfileService: ResUserProfileService;
	private readonly _imgbbService: ImgBBService;

	constructor() {
		super("/api/v1/profile");

		this._resUserProfileService = new ResUserProfileService();
		this._imgbbService = new ImgBBService();
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}

	protected registerRouterHandler(): void {
		this._router.get(
			"/",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			this.getUserProfileAsync.bind(this)
		);

		this._router.put(
			"/",
			validateRequestMiddleware("body", updateProfileBodySchema),
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			this.updatePartiallyProfileAsync.bind(this)
		);

		this._router.put(
			"/avatar",
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			multerMiddleware.single("avatar"),
			this.updateAvatarAsync.bind(this)
		);
	}

	async getUserProfileAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { uid } = req.userinfo;
			const fetchedFields = ["first_name", "last_name", "display_name", "email", "avatar"];
			const profile = await this._resUserProfileService.getRawProfileByUserIdAsync(uid, fetchedFields);

			if (!profile) {
				Logger.error("Cannot find res_user_profile by uid, res_user already found");
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.INTERNAL_SERVER_ERROR)
					.throw();
				return;
			}

			const result = DataResponseBuilder.init().withData(profile).build();
			return res.status(HTTP_CODE.OK).json(result);
		} catch (error) {
			next(error);
		}
	}

	async updateAvatarAsync(req: Request, res: Response, next: NextFunction) {
		try {
			if (req.file) {
				const { path, filename } = req.file;

				Logger.debug(`Uploading avatar to imgbb with ${JSON.stringify({ filename, path })}`);
				const uploadResp = await this._imgbbService.uploadAsync(path, filename);

				if (uploadResp.status === 200 && uploadResp.success) {
					Logger.debug("Uploaded successfully avatar to imgbb");

					const { uid } = req.userinfo;
					const imageUrl = uploadResp.data.url ?? uploadResp.data.image.url;
					if (imageUrl) {
						Logger.debug(`Updating avatar to the database with uid = ${uid}`);
						await this._resUserProfileService.updatePartiallyByUserIdAsync(uid, { avatar: imageUrl });
						Logger.debug(`Updated successfully avatar to the database with uid = ${uid}`);
					}

					const result = DataResponseBuilder.init()
						.withData({ url: imageUrl })
						.build();
					return res.status(HTTP_CODE.OK).json(result);
				}

				Logger.debug("Failed to upload avatar to imgbb");
			}

			HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.UPLOAD_ERROR)
				.throw();
		} catch (error) {
			next(error);
		}
	}

	async updatePartiallyProfileAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { uid } = req.userinfo;
			const { firstName, lastName, displayName } = req.body as UpdateProfileBody;
			Logger.debug(JSON.stringify({ firstName, lastName, displayName }));

			if (firstName || lastName || displayName) {
				Logger.debug(`Updating profile to the database with uid = ${uid}`);
				await this._resUserProfileService.updatePartiallyByUserIdAsync(uid, { firstName, lastName, displayName });
				Logger.debug(`Updated successfully profile to the database with uid = ${uid}`);
			}

			return res.status(HTTP_CODE.OK).json(DataResponseBuilder.init().build());
		} catch (error) {
			next(error);
		}
	}
}
