import { NextFunction, Request, Response } from "express";
import _ from "lodash";
import moment from "moment";
import {
	authMiddlewareV2,
	mapPresentationRole,
	preGetPresentationAsync,
	validateRequestMiddleware,
	verifyUserActivationMiddlewareAsync,
} from "../../common";
import {
	DataResponseBuilder,
	getQuestionListQuerySchema,
	HttpErrorBuilder,
	PresentationQuestion,
	updateQuestionBodySchema,
} from "../../core";
import {
	QuestionAnswerSocketEventHandler,
	SocketPresentNamespaceToggleQuestionStateMessage,
	SocketToggleQuestionAnswerState
} from "../../infrastructure";
import { PresentationQuestionService } from "../../services";
import { HTTP_CODE, PRESENTATION_PACE_STATES, RESPONSE_CODE } from "../../shared";
import { BaseController } from "../base.controller";

/**
 * @description This controller is used for presenter
 */
export class QuestionAnswerController extends BaseController {
	private readonly _questionService: PresentationQuestionService;

	constructor() {
		super("/api/v1");
		this._questionService = new PresentationQuestionService();
	}

	protected registerMiddlewares(): void {
		this._router.use(authMiddlewareV2, verifyUserActivationMiddlewareAsync);
	}

	protected registerRouterHandler(): void {
		this._router.get(
			"/presentations/:series_id/question-answers",
			validateRequestMiddleware("query", getQuestionListQuerySchema),
			preGetPresentationAsync("params.series_id"),
			mapPresentationRole,
			this.getQuestionAndAnswerListAsync.bind(this)
		);

		this._router.patch(
			"/presentations/:series_id/question-answers/:question_id",
			validateRequestMiddleware("body", updateQuestionBodySchema),
			preGetPresentationAsync("params.series_id"),
			mapPresentationRole,
			this.updateQuestionArchiveStatusAsync.bind(this)
		);
	}

	/**
	 * @access Must update for presenter and co-owner only
	 */
	async getQuestionAndAnswerListAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { query } = req;
			const { page, limit, sortByTime, sortByTotal, isArchive } = query as any;
			const presentation = req.presentation!;

			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			// Before continue, we must check the permission of current user
			// Must ensure which type of present state
			// If the current user is present owner or (group owner | co-owner) (if presenting in group) => by pass
			const mappedRole = req.presentationRoleMap!;

			if (!mappedRole.groupRole && mappedRole.presentationRole !== "owner") {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION)
					.build();
			}

			const [questionList, totalRecords] = await this._questionService.getQuestionListAsync(
				{
					presentationSeriesId: presentation.seriesId,
					isArchive,
					orderByTime: sortByTime,
					orderByTotal: sortByTotal,
				},
				limit,
				page
			);

			if (totalRecords > 0 && page > Math.ceil(totalRecords / limit)) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_SEARCH_QUERY)
					.build();
			}
			res.json(
				DataResponseBuilder.init()
					.withData({
						items: questionList,
						pagination: { totalRecords, page, limit },
						user: {
							uid: req.userinfo.uid,
							displayName: req.userinfo.display_name,
						},
					})
					.build()
			);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @access only the owner, group owner or co-owner (if presenting in group)
	 */
	async updateQuestionArchiveStatusAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const presentation = req.presentation!;

			// ensure the presentation is presenting
			if (presentation.pace.state === PRESENTATION_PACE_STATES.IDLE) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INACTIVE_PRESENTATION)
					.build();
			}

			// Before continue, we must check the permission of current user
			// Must ensure which type of present state
			// If the current user is present owner or (group owner | co-owner) (if presenting in group) => by pass
			const mappedRole = req.presentationRoleMap!;

			if (!mappedRole.groupRole && mappedRole.presentationRole !== "owner") {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION)
					.build();
			}

			const questionId = _.get(req.params, "question_id", 0) as number;
			const actionType = _.get(req.body, "actionType", "") as string;

			const question = await this._questionService.getRecordByIdAsync(questionId);
			if (!question || question.presentationSeriesId !== presentation.seriesId) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.QUESTION_NOT_FOUND)
					.build();
			}

			// do action based on actionType
			// restore is default
			const subRecord = {
				isArchive: false,
				archivedAt: null,
			} as PresentationQuestion;

			if (actionType === "archive") {
				subRecord.isArchive = true;
				subRecord.archivedAt = moment().toDate();
			}

			await this._questionService.updateRecordByIdAsync(question.id, subRecord);

			res.json(DataResponseBuilder.init().build());

			// Emit socker event
			const qaAction: SocketToggleQuestionAnswerState = subRecord.isArchive
				? "mark_as_answered"
				: "unmark_answered";
			const voteKey = presentation.voteKey;
			const message: Omit<SocketPresentNamespaceToggleQuestionStateMessage, "action"> = {
				id: question.id,
				presentationSeriesId: question.presentationSeriesId,
				totalLike: question.totalLike,
				isArchive: subRecord.isArchive,
				userId: question.userId,
				userDisplayName: question.userDisplayName,
				question: question.question,
				createdAt: question.createdAt.toISOString(),
				archivedAt: subRecord.archivedAt ? subRecord.archivedAt.toISOString() : null,
			};
			QuestionAnswerSocketEventHandler.instance.toggleQuestionState(qaAction, voteKey, message);
		} catch (error) {
			next(error);
		}
	}
}
