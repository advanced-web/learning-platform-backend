import { NextFunction, Request, Response } from "express";
import _ from "lodash";
import { authMiddlewareV2, validateRequestMiddleware, verifyUserActivationMiddlewareAsync } from "../../common";
import { Logger } from "../../common/utils/logger/logger";
import {
	activateUserBodySchema,
	ChangePasswordBody,
	changePasswordBodySchema,
	DataResponseBuilder,
	forgotPasswordBodySchema,
	HttpErrorBuilder,
	loginBodySchema,
	oauthProviderParamSchema,
	registerBodySchema,
	resetPasswordBodySchema,
} from "../../core";
import {
	AppConfig,
	ISendAccountActivationData,
	ISendResetPasswordData,
	ISendResetPasswordNotifyData,
	ResetPasswordConfig,
} from "../../infrastructure";
import { SendEmailEvent } from "../../infrastructure/events/send-email.event";
import { LocalAuthService, OAuthService, ResUserProfileService, ResUserService } from "../../services";
import { ResetPasswordRequestService } from "../../services/reset-password-request.service";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";
import { BaseController } from "../base.controller";

export class AuthControllerV2 extends BaseController {
	private readonly _localAuthService: LocalAuthService;
	private readonly _resUserService: ResUserService;
	private readonly _resUserProfileService: ResUserProfileService;
	private readonly _oauthService: OAuthService;
	private readonly _resetPasswordRequestService: ResetPasswordRequestService;

	constructor() {
		super("/api/v2/auth");

		this._localAuthService = new LocalAuthService();
		this._resUserService = new ResUserService();
		this._resUserProfileService = new ResUserProfileService();
		this._oauthService = new OAuthService();
		this._resetPasswordRequestService = new ResetPasswordRequestService();
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}
	protected registerRouterHandler(): void {
		this._router.post("/sign-out", this.signOut.bind(this));

		this._router.post(
			"/login",
			validateRequestMiddleware("body", loginBodySchema),
			this.loginWithLocalStrategyAsync.bind(this)
		);

		this._router.get("/userinfo", authMiddlewareV2, this.getUserInfoAsync.bind(this));

		this._router.post(
			"/register",
			validateRequestMiddleware("body", registerBodySchema),
			this.registerUserAsync.bind(this)
		);

		this._router.get(
			"/:provider/login",
			validateRequestMiddleware("params", oauthProviderParamSchema),
			this.getLoginURL.bind(this)
		);

		this._router.get(
			"/:provider/callback",
			validateRequestMiddleware("params", oauthProviderParamSchema),
			this.processOAuthCallbackAsync.bind(this)
		);

		this._router.post(
			"/activate",
			validateRequestMiddleware("body", activateUserBodySchema),
			this.activateUserAsync.bind(this)
		);

		this._router.put(
			"/change-password",
			validateRequestMiddleware("body", changePasswordBodySchema),
			authMiddlewareV2,
			verifyUserActivationMiddlewareAsync,
			this.changePasswordAsync.bind(this)
		);

		this._router.post(
			"/forgot-password",
			validateRequestMiddleware("body", forgotPasswordBodySchema),
			this.sendForgotPasswordEmailAsync.bind(this)
		);

		this._router.post(
			"/reset-password",
			validateRequestMiddleware("body", resetPasswordBodySchema),
			this.handleResetPasswordAsync.bind(this)
		);
	}

	signOut(req: Request, res: Response, next: NextFunction) {
		try {
			this._localAuthService.signOut();

			// No content
			res.sendStatus(HTTP_CODE.NO_CONTENT);
		} catch (error) {
			next(error);
		}
	}

	/**
	 * @deprecated
	 */
	async loginWithLocalStrategyAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { body } = req;

			const [user] = await this._resUserService.getUserByUsernameAsync(body.username);
			if (!user) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_LOGIN)
					.throw();
				return;
			}

			// BUG: social sign in provider => user.password is null
			if (!this._localAuthService.compareUserPassword(body.password, user.password ?? "")) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_LOGIN)
					.throw();
				return;
			}

			// Account activation
			if (!user.isActive) {
				// generate activation code
				const activationToken = await this._resUserService.generateUserActivationTokenAsync(user);
				SendEmailEvent.instance.notify("send_account_activation", <ISendAccountActivationData>{
					to: user.username,
					token: activationToken,
					userDisplayName: "",
				});

				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INACTIVE_USER)
					.throw();
				return;
			}

			// create token
			const userProfile = await this._resUserProfileService.getProfileByUserIdAsync(user.id);
			if (!userProfile) {
				Logger.error({ error: "profile_does_not_exist", userId: user.id });
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.LOGIN_ERROR)
					.throw();
				return;
			}

			const tokenInformation = this._localAuthService.generateAccessToken(user, userProfile);

			const data = DataResponseBuilder.init().withData(tokenInformation).build();
			res.json(data);
		} catch (error) {
			next(error);
		}
	}

	async getUserInfoAsync(req: Request, res: Response, next: NextFunction) {
		const dataResponse = DataResponseBuilder.init().withData(_.omit(req.userinfo, "uid")).build();
		res.json(dataResponse);
	}

	async registerUserAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { body } = req;
			const [existedUser] = await this._resUserService.getUserByUsernameAsync(body.username);
			if (existedUser) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.EXISTED_EMAIL)
					.throw();
				return;
			}

			const user = await this._resUserService.createNewUserAsync({
				username: body.username,
				password: body.password,
			});

			Logger.debug("Created user:");
			Logger.debug(user);
			if (!user) {
				Logger.error({
					error: "create_user_error",
					errorDescription: "Can not create new user while signing up",
				});
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.REGISTER_ERROR)
					.throw();
				return;
			}

			// Create new profile
			const profile = await this._resUserProfileService.saveAsync({
				email: user.username,
				userId: user.id,
				firstName: body.firstName,
				lastName: body.lastName,
				displayName: `${body.firstName} ${body.lastName}`,
			});

			Logger.debug("Created profile:");
			Logger.debug(profile);
			if (!profile) {
				Logger.error({
					error: "create_user_profile_error",
					errorDescription: "Can not create new user profile while signing up",
				});
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.REGISTER_ERROR)
					.throw();
				return;
			}

			res.status(201).json(DataResponseBuilder.init().withResponseCode(201).build());

			// generate activation code
			const activationToken = await this._resUserService.generateUserActivationTokenAsync(user);
			SendEmailEvent.instance.notify("send_account_activation", <ISendAccountActivationData>{
				to: user.username,
				token: activationToken,
				userDisplayName: profile.displayName,
			});
		} catch (error) {
			next(error);
		}
	}

	async activateUserAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { body } = req;
			const [user] = await this._resUserService.getUserByUsernameAsync(body.email);
			if (!user) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_ACTIVATE_EMAIL)
					.throw();
				return;
			}

			if (user.isActive) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.ACTIVATED_EMAIL)
					.throw();
				return;
			}

			const profile = await this._resUserProfileService.getProfileByUserIdAsync(user.id);

			res.json(DataResponseBuilder.init().build());

			// generate activation code
			const activationToken = await this._resUserService.generateUserActivationTokenAsync(user);

			SendEmailEvent.instance.notify("send_account_activation", <ISendAccountActivationData>{
				to: user.username,
				token: activationToken,
				userDisplayName: profile?.displayName,
			});
		} catch (error) {
			next(error);
		}
	}

	getLoginURL(req: Request, res: Response, next: NextFunction) {
		try {
			const { provider } = req.params;
			const loginURL = this._oauthService.getLoginURL(provider);

			const result = DataResponseBuilder.init().withData({ url: loginURL });
			res.status(HTTP_CODE.OK).json(result.build());
		} catch (error) {
			next(error);
		}
	}

	async processOAuthCallbackAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { provider } = req.params;
			const { user, userProfile } = await this._oauthService.processOAuthCallback(provider, req.query);
			const tokenInformation = this._localAuthService.generateAccessToken(user, userProfile);

			const result = DataResponseBuilder.init().withData(tokenInformation);
			res.status(HTTP_CODE.OK).json(result.build());
		} catch (error) {
			next(error);
		}
	}

	async changePasswordAsync(req: Request, res: Response, next: NextFunction) {
		try {
			// fetchedUser is always not undefined if the `verifyUserActivationMiddlewareAsync` is passed
			const fetchedUser = req.fetchedUser!;
			const { uid } = req.userinfo;
			const { oldPassword, newPassword } = req.body as ChangePasswordBody;

			const logContext = "AuthenticationV2";

			// check !fetchedUser.password to omit nullable value  of typescript compiler
			if (!fetchedUser.password) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.CHANGE_PASSWORD_PERMISSION)
					.throw();
			} else if (oldPassword === newPassword) {
				Logger.info("oldPassword = newPassword", logContext);
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.UPDATE_PROFILE_ERROR)
					.throw();
			} else if (!this._localAuthService.compareUserPassword(oldPassword, fetchedUser.password)) {
				Logger.info("The old password does not match", logContext);
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.UPDATE_PROFILE_ERROR)
					.throw();
			} else {
				Logger.debug(`Updating password to the database with uid = ${uid}`, logContext);
				await this._resUserService.updatePasswordByIdAsync(uid, newPassword);
				Logger.debug(`Updated successfully password to the database with uid = ${uid}`, logContext);

				res.status(HTTP_CODE.OK).json(DataResponseBuilder.init().build());
			}
		} catch (error) {
			next(error);
		}
	}

	async sendForgotPasswordEmailAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { email } = req.body;

			// it must check whether this email is valid or not
			const [user] = await this._resUserService.getUserByUsernameAsync(email);

			if (!user) {
				return res.sendStatus(204);
			}

			res.sendStatus(204);

			// generate reset password token
			const resetToken = await this._resetPasswordRequestService.generateResetTokenAsync(email);

			// send email with reset password token
			const resetLink = `${ResetPasswordConfig.REST_PWD_REDIRECT}?token=${resetToken}`;
			SendEmailEvent.instance.notify("send_reset_password", <ISendResetPasswordData>{
				to: email,
				resetLink,
				siteUrl: AppConfig.CLIENT_MAIN_SITE,
			});
		} catch (error) {
			next(error);
		}
	}

	async handleResetPasswordAsync(req: Request, res: Response, next: NextFunction) {
		try {
			const { password, token } = req.body;

			if (!token) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_RESET_TOKEN)
					.throw();
				return;
			}

			// verify token information and exp
			const oldToken = await this._resetPasswordRequestService.getValidExpireRecordByToken(token.toString());
			if (!oldToken) {
				HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.INVALID_RESET_TOKEN)
					.throw();
				return;
			}

			// find the existed user and update
			const email = oldToken.email;
			const [user] = await this._resUserService.getUserByUsernameAsync(email);
			if (!user) {
				return res.json({ isSuccess: false });
			}

			await this._resUserService.updatePasswordByIdAsync(user.id, password);

			// invalidate token
			await this._resetPasswordRequestService.deleteRecordByIdAsync(oldToken.id);

			res.status(HTTP_CODE.OK).json(DataResponseBuilder.init().build());

			// trigger change password email
			SendEmailEvent.instance.notify("send_reset_password_notify", <ISendResetPasswordNotifyData>{
				to: email,
				forgotLink: `${ResetPasswordConfig.REST_PWD_CLIENT}`,
			});
		} catch (error) {
			next(error);
		}
	}
}
