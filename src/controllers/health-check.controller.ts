import { NextFunction, Request, Response } from "express";
import { DataResponseBuilder } from "../core";
import { BaseController } from "./base.controller";
import { AppConfig } from "../infrastructure";
import { HTTP_CODE } from "../shared";

export class HealthCheckController extends BaseController {
	constructor() {
		super("/health-check");
	}

	protected registerMiddlewares(): void {
		this._router.use((req: Request, res: Response, next: NextFunction) => {
			next();
		});
	}

	protected registerRouterHandler(): void {
		this._router.get("/", this.checkEnvironment);
	}

	checkEnvironment(req: Request, res: Response) {
		const result = DataResponseBuilder.init()
			.withData({
				name: AppConfig.APP_NAME,
				powerBy: AppConfig.POWER_BY,
				environment: AppConfig.APP_ENV,
			})
			.build();
		res.status(HTTP_CODE.OK).json(result);
	}
}
