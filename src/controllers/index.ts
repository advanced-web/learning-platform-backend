import { AccountController } from "./account.controller";
import { ApiDocController } from "./api-doc.controller";
import { BaseController } from "./base.controller";
import { HealthCheckController } from "./health-check.controller";
import {
	AudienceChatController,
	AudienceController, CollaboratorController,
	LearningGroupController,
	PresentationChatController,
	PresentationsController,
	QuestionAnswerController,
	ResUserProfileController,
	SlidesController,
	StatisticController
} from "./v1";
import { AuthControllerV2 } from "./v2";
import { AuthControllerV3 } from "./v3";

export const ControllerList: BaseController[] = [
	new HealthCheckController(),
	new ApiDocController(),
	new AccountController(),
	new ResUserProfileController(),
	new LearningGroupController(),
	new AudienceController(),
	new AudienceChatController(),
	new PresentationsController(),
	new SlidesController(),
	new StatisticController(),
	new CollaboratorController(),
	new PresentationChatController(),

	new QuestionAnswerController(),

	new AuthControllerV2(),
	new AuthControllerV3(),
];
