import { Server as HttpServer } from "http";
import { Namespace, Server, Socket } from "socket.io";
import { Logger } from "../../common/utils/logger/logger";
import { AppConfig } from "../configs";
import {
	IGroupsNamespaceServerToClientEvents,
	IPresentNamespaceServerToClientEvents,
	IRoomClientToServerCommonEvents,
	SocketEventLogInfo,
	SocketIdentityLogInfo
} from "./types";

export class ServerSocket {
	public static instance: ServerSocket;
	public io: Server;
	public groupNamespace: Namespace<
		IRoomClientToServerCommonEvents,
		IGroupsNamespaceServerToClientEvents
	>;
	public presentNamespace: Namespace<
		IRoomClientToServerCommonEvents,
		IPresentNamespaceServerToClientEvents
	>;

	private readonly LOG_CONTEXT = "Socket";

	constructor(server: HttpServer) {
		ServerSocket.instance = this;

		this.io = new Server(server, {
			serveClient: false,
			pingInterval: 10000,
			pingTimeout: 5000,
			cookie: false,
			cors: { origin: AppConfig.ORIGINS },
		});

		// groups connection
		this.groupNamespace = this.io.of("/groups")
		this.groupNamespace.on("connection", this.handleGroupNamespaceConnection.bind(this))

		// present connection
		this.presentNamespace = this.io.of("/present")
		this.presentNamespace.on("connection", this.handlePresentNamespaceConnection.bind(this))
	}

	handleGroupNamespaceConnection(socket: Socket<IRoomClientToServerCommonEvents>) {
		const eventInfo: SocketEventLogInfo = { name: "", type: "listen" };
		const identityInfo = { socketId: socket.id, namespace: "groups" };

		this._handleBaseListeningEvents(socket, identityInfo, eventInfo);
		this._handleRoomCommonEvents(socket, identityInfo, eventInfo);
	}

	handlePresentNamespaceConnection(socket: Socket<IRoomClientToServerCommonEvents>) {
		const eventInfo: SocketEventLogInfo = { name: "", type: "listen" };
		const identityInfo = { socketId: socket.id, namespace: "present" };

		this._handleBaseListeningEvents(socket, identityInfo, eventInfo);
		this._handleRoomCommonEvents(socket, identityInfo, eventInfo);
	}

	private _handleRoomCommonEvents(
		socket: Socket<IRoomClientToServerCommonEvents>,
		identityInfo: SocketIdentityLogInfo,
		eventInfo: SocketEventLogInfo,
	) {
		// join room
		socket.on("join-room", (room: string) => {
			socket.join(room);
			this.logInfo({ ...eventInfo, name: "join-room" }, { ...identityInfo, room });
		});

		// leave room
		socket.on("leave-room", (room: string) => {
			socket.join(room);
			this.logInfo({ ...eventInfo, name: "leave-room" }, { ...identityInfo, room });
		});
	}

	private _handleBaseListeningEvents(
		socket: Socket,
		identityInfo: SocketIdentityLogInfo,
		eventInfo: SocketEventLogInfo,
	) {
		this.logInfo({ ...eventInfo, name: "connection" }, identityInfo);

		socket.on("error", (error) => {
			this.logError({ ...eventInfo, name: "error" }, identityInfo, { error });
		})

		socket.on("disconnecting", (reason) => {
			const metadata = { reason, rooms: socket.rooms };
			this.logInfo({ ...eventInfo, name: "disconnecting" }, identityInfo, metadata);
		});

		socket.on("disconnect", (reason) => {
			this.logInfo({ ...eventInfo, name: "disconnect" }, identityInfo, { reason });
		});
	}

	logInfo(eventInfo: SocketEventLogInfo, identityInfo: SocketIdentityLogInfo, metadata?: object) {
		Logger.info(
			JSON.stringify({
				event: eventInfo,
				identity: identityInfo,
				metadata,
			}),
			this.LOG_CONTEXT
		);
	}

	logError(eventInfo: SocketEventLogInfo, identityInfo: SocketIdentityLogInfo, metadata?: object) {
		Logger.error(
			JSON.stringify({
				event: eventInfo,
				identity: identityInfo,
				metadata,
			}),
			this.LOG_CONTEXT
		);
	}
}

