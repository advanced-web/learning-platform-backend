import { PresentationPace } from "../../core";

export type SocketEventLogInfo = {
	name: string;
	type: "listen" | "emit";
};

export type SocketIdentityLogInfo = {
	socketId?: string;
	namespace?: string;
	room?: string;
};

export type SocketRoomIdentifiers = {
	voteKey: string;
	groupId: number | null;
};

export type SocketGroupsNamespaceStartPresentingMessage = {
	action: "present";
	voteKey: string;
};

export type SocketGroupsNamespaceQuitPresentationMessage = {
	action: "quit";
};

export interface IGroupsNamespaceServerToClientEvents {
	present: (
		data: SocketGroupsNamespaceStartPresentingMessage |
			SocketGroupsNamespaceQuitPresentationMessage
	) => void;
}

export interface IRoomClientToServerCommonEvents {
	"join-room": (roomId: string) => void;
	"leave-room": (roomId: string) => void;
}

export type SocketPresentNamespaceStartPresentingMessage = {
	action: "present";
	voteKey: string;
	pace: PresentationPace;
};

export type SocketPresentNamespaceQuitPresentationMessage = {
	action: "quit";
};

export type SocketPresentNamespaceChangeSlideMessage = {
	action: "change_slide";
	voteKey: string;
	seriesId: string;
	pace: PresentationPace;
};

export type SocketPresentNamespaceVotingResultMessage = {
	slideId: number;
	slideAdminKey: string;
	respondents: number;
	results: string;  // JSON.stringify([{ id: number; label: string; score: number[] }]);
};

export type SocketPresentNamespaceChatMessage = {
	id: number;
	presentationSeriesId: string;
	userId: number;
	userDisplayName: string;
	userAvatarLink: string | null;
	message: string;
	createdAt: string;
};

export type SocketCreateQuestionAction = "create";
export type SocketToggleVoteQuestionAction = "up_vote" | "down_vote";
export type SocketToggleQuestionAnswerState = "mark_as_answered" | "unmark_answered";

export type SocketPresentNamespaceCreateQuestionMessage = {
	action: SocketCreateQuestionAction;
	id: number;
	presentationSeriesId: string;
	totalLike: number;
	isArchive: boolean | null;
	userId: number;
	userDisplayName: string;
	question: string;
	createdAt: string;
	archivedAt: string | null;
};

export type SocketPresentNamespaceToggleVoteQuestionMessage = {
	action: SocketToggleVoteQuestionAction;
	id: number;
	totalLike: number;
	isArchive: boolean | null;
	archivedAt: string | null;
};

export type SocketPresentNamespaceToggleQuestionStateMessage = {
	action: SocketToggleQuestionAnswerState;
	id: number;
	presentationSeriesId: string;
	totalLike: number;
	isArchive: boolean | null;
	userId: number;
	userDisplayName: string;
	question: string;
	createdAt: string;
	archivedAt: string | null;
};

export interface IPresentNamespaceServerToClientEvents {
	present: (
		data: SocketPresentNamespaceStartPresentingMessage |
			SocketPresentNamespaceQuitPresentationMessage |
			SocketPresentNamespaceChangeSlideMessage
	) => void;
	vote: (data: SocketPresentNamespaceVotingResultMessage) => void;
	chat: (data: SocketPresentNamespaceChatMessage) => void;
	question: (
		data: SocketPresentNamespaceCreateQuestionMessage |
			SocketPresentNamespaceToggleVoteQuestionMessage |
			SocketPresentNamespaceToggleQuestionStateMessage
	) => void;
}
