import { PresentationPace } from "../../../core";
import { PRESENTATION_PACE_ACTIONS } from "../../../shared";
import { ServerSocket } from "../socket";
import {
	SocketEventLogInfo,
	SocketGroupsNamespaceQuitPresentationMessage,
	SocketGroupsNamespaceStartPresentingMessage,
	SocketPresentNamespaceChangeSlideMessage,
	SocketPresentNamespaceQuitPresentationMessage,
	SocketPresentNamespaceStartPresentingMessage,
	SocketRoomIdentifiers
} from "../types";

export class SlideControllerSocketEventHandler {
	static instance = new SlideControllerSocketEventHandler();
	private constructor() {}

	notifyStartPresentingToGroup(groupId: number, voteKey: string) {
		const eventInfo: SocketEventLogInfo = { name: "present", type: "emit" };
		const identityInfo = { namespace: "groups", room: groupId.toString() };
		const metadata = { action: "present" };

		const message: SocketGroupsNamespaceStartPresentingMessage = { action: "present", voteKey };
		ServerSocket.instance.groupNamespace.to(groupId.toString()).emit("present", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo, metadata);
	}

	notifyQuitPresentingToGroup(groupId: number) {
		const eventInfo: SocketEventLogInfo = { name: "present", type: "emit" };
		const identityInfo = { namespace: "groups", room: groupId.toString() };
		const metadata = { action: "quit" };

		const message: SocketGroupsNamespaceQuitPresentationMessage = { action: "quit" };
		ServerSocket.instance.groupNamespace.to(groupId.toString()).emit("present", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo, metadata);
	}

	present(voteKey: string, pace: PresentationPace) {
		const eventInfo: SocketEventLogInfo = { name: "present", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };
		const metadata = { action: "present" };

		const message: SocketPresentNamespaceStartPresentingMessage = { action: "present", voteKey, pace };
		ServerSocket.instance.presentNamespace.to(voteKey).emit("present", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo, metadata);
	}

	quit(voteKey: string) {
		const eventInfo: SocketEventLogInfo = { name: "present", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };
		const metadata = { action: "quit" };

		const message: SocketPresentNamespaceQuitPresentationMessage = { action: "quit" };
		ServerSocket.instance.presentNamespace.to(voteKey).emit("present", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo, metadata);
	}

	changeSlide(voteKey: string, seriesId: string, pace: PresentationPace) {
		const eventInfo: SocketEventLogInfo = { name: "present", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };
		const metadata = { action: "change_slide" };

		const message: SocketPresentNamespaceChangeSlideMessage = { action: "change_slide", voteKey, seriesId, pace };
		ServerSocket.instance.presentNamespace.to(voteKey).emit("present", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo, metadata);
	}

	handleSlideAction(
		roomIds: SocketRoomIdentifiers,
		action: string,
		seriesId: string,
		pace: PresentationPace,
	) {
		const { voteKey, groupId } = roomIds;

		switch (action) {
			case PRESENTATION_PACE_ACTIONS.PRESENT: {
				if (groupId) {
					SlideControllerSocketEventHandler.instance.notifyStartPresentingToGroup(groupId, voteKey);
				}

				SlideControllerSocketEventHandler.instance.present(voteKey, pace);
				break;
			}
			case PRESENTATION_PACE_ACTIONS.QUIT: {
				if (groupId) {
					SlideControllerSocketEventHandler.instance.notifyQuitPresentingToGroup(groupId);
				}

				SlideControllerSocketEventHandler.instance.quit(voteKey);
				break;
			}
			case PRESENTATION_PACE_ACTIONS.CHANGE_SLIDE: {
				SlideControllerSocketEventHandler.instance.changeSlide(voteKey, seriesId, pace);
				break;
			}
		}
	}
}
