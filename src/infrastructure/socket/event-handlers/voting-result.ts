import { IBaseSlideChoice, SlideVotingResult } from "../../../core";
import { VotingResultsService } from "../../../services";
import { ServerSocket } from "../socket";
import { SocketPresentNamespaceVotingResultMessage, SocketEventLogInfo } from "../types";

export class VotingResultSocketEventHandler {
	static instance = new VotingResultSocketEventHandler();

	protected _votingResultService: VotingResultsService;
	private constructor() {
		this._votingResultService = new VotingResultsService();
	}

	async sendResult(voteKey: string, slideId: number, slideAdminKey: string, choices: IBaseSlideChoice[]) {
		const votingResults = await this._votingResultService.getResultsByAdminKeyAndCountByAnswerId(slideAdminKey);
		const answerResults: SlideVotingResult<number>[] = [];
		let respondents = 0;

		// maps list of choices to list of voting results
		choices.map((choice) => {
			const votingResult = votingResults.find((element) => element.answerId === choice.id);
			const answerCount = votingResult ? Number(votingResult.answerCount) : 0;

			respondents += answerCount;
			answerResults.push({ id: choice.id, label: choice.label, score: [answerCount] });
		});

		// log info
		const eventInfo: SocketEventLogInfo = { name: "vote", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };

		// emit
		const message: SocketPresentNamespaceVotingResultMessage = {
			slideId,
			slideAdminKey,
			respondents,
			results: JSON.stringify(answerResults),
		};
		ServerSocket.instance.presentNamespace.to(voteKey).emit("vote", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo);
	}
}
