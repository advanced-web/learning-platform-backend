import { ServerSocket } from "../socket";
import {
	SocketEventLogInfo,
	SocketPresentNamespaceCreateQuestionMessage,
	SocketPresentNamespaceToggleQuestionStateMessage,
	SocketPresentNamespaceToggleVoteQuestionMessage,
	SocketToggleQuestionAnswerState,
	SocketToggleVoteQuestionAction
} from "../types";

export class QuestionAnswerSocketEventHandler {
	static instance = new QuestionAnswerSocketEventHandler();
	private constructor() {}

	createQuestion(voteKey: string, message: SocketPresentNamespaceCreateQuestionMessage) {
		const eventInfo: SocketEventLogInfo = { name: "question", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };

		ServerSocket.instance.presentNamespace.to(voteKey).emit("question", message);
		ServerSocket.instance.logInfo(eventInfo, identityInfo);
	}

	toggleVoteQuestion(
		action: SocketToggleVoteQuestionAction,
		voteKey: string,
		message: Omit<SocketPresentNamespaceToggleVoteQuestionMessage, "action">,
	) {
		const eventInfo: SocketEventLogInfo = { name: "question", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };

		ServerSocket.instance.presentNamespace.to(voteKey).emit("question", { ...message, action });
		ServerSocket.instance.logInfo(eventInfo, identityInfo);
	}

	toggleQuestionState(
		action: SocketToggleQuestionAnswerState,
		voteKey: string,
		message: Omit<SocketPresentNamespaceToggleQuestionStateMessage, "action">,
	) {
		const eventInfo: SocketEventLogInfo = { name: "question", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };

		ServerSocket.instance.presentNamespace.to(voteKey).emit("question", { ...message, action });
		ServerSocket.instance.logInfo(eventInfo, identityInfo);
	}
}
