import { PresentationChat } from "../../../core";
import { ServerSocket } from "../socket";
import { SocketPresentNamespaceChatMessage, SocketEventLogInfo } from "../types";

export class ChatSocketEventHandler {
	static instance = new ChatSocketEventHandler();
	private constructor() {}

	sendMessage(voteKey: string, data: PresentationChat) {
		const eventInfo: SocketEventLogInfo = { name: "chat", type: "emit" };
		const identityInfo = { namespace: "present", room: voteKey };

		const message: SocketPresentNamespaceChatMessage = {
			id: data.id,
			presentationSeriesId: data.presentationSeriesId,
			userId: data.userId,
			userDisplayName: data.userDisplayName,
			userAvatarLink: data.userAvatarLink,
			message: data.message,
			createdAt: data.createdAt.toISOString(),
		};
		ServerSocket.instance.presentNamespace.to(voteKey).emit("chat", message);

		ServerSocket.instance.logInfo(eventInfo, identityInfo);
	}
}
