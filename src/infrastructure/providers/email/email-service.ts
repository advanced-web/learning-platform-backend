import { IMailer } from ".";
import nodemailer from "nodemailer";
import { EmailConfig } from "../../configs";

export class EmailService implements IMailer {
	private _transporter: nodemailer.Transporter;

	constructor() {
		this._transporter = nodemailer.createTransport({
			host: EmailConfig.HOST,
			port: EmailConfig.PORT,
			secure: EmailConfig.SECURE,
			auth: {
				user: EmailConfig.USER,
				pass: EmailConfig.PASSWORD,
			},
			pool: true,
		});
	}

	send(to: string, subject: string, html: string) {
		return this._transporter.sendMail({
			from: EmailConfig.USER,
			to,
			subject,
			html,
		});
	}
}
