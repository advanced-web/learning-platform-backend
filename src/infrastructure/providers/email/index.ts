export interface IMailer {
	send(to: string, subject: string, content: string): Promise<any>;
}

export * from "./email-service";
