import fs from "fs";
import FormData from "form-data";
import axios from "axios";
import { ImgBBConfig } from "../../configs";
import { IImgBBResponse } from ".";
import { HttpErrorBuilder } from "../../../core";
import { Logger } from "../../../common/utils/logger/logger";
import { HTTP_CODE, RESPONSE_CODE } from "../../../shared";

export class ImgBBService {
	constructor() { }

	async uploadAsync(path: string, filename: string, removeFileAfterCompleting = true) {
		let isRemoved = false;

		try {
			const formData = new FormData();
			formData.append("image", fs.createReadStream(path));
			formData.append("name", filename);

			const uploadURL = `${ImgBBConfig.UPLOAD_URL}?key=${ImgBBConfig.API_KEY}`;
			const axiosConfig = { headers: formData.getHeaders() };
			const resp = await axios.post<IImgBBResponse>(uploadURL, formData, axiosConfig);

			if (removeFileAfterCompleting) {
				this._removeLocalFile(path);
				isRemoved = true;
			}

			return resp.data;
		} catch (error: any) {
			if (!isRemoved && removeFileAfterCompleting) {
				this._removeLocalFile(path);
			}

			const errorData = error?.response?.data;
			Logger.debug(JSON.stringify(errorData));

			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.UPLOAD_ERROR)
				.withMessage("Upload Error")
				.build();
			throw httpError;
		}
	}

	private _removeLocalFile(path: string) {
		fs.unlink(path, (error) => {
			if (error) Logger.error(error, "ImgBB");
			else Logger.info(`Removed ${path}`, "ImgBB");
		});
	}
}
