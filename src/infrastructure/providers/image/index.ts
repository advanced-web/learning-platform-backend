export interface IImgBBData {
	id: string;
	title: string;
	url_viewer: string;
	url: string;
	display_url: string;
	width: string;
	height: string;
	size: number;
	time: string;
	expiration: string;
	image: {
		filename: string;
		name: string;
		mime: string;
		extension: string;
		url: string;
	};
	thumb: {
		filename: string;
		name: string;
		mime: string;
		extension: string;
		url: string;
	};
	delete_url: string;
}

export interface IImgBBResponse {
	data: IImgBBData;
	success: boolean;
	status: number;
}

export * from "./imgbb.service";
