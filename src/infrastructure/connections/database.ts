import { DataSource } from "typeorm";
import { DbConfig } from "../configs";

export const defaultDataSource = new DataSource({
	type: "postgres",
	host: DbConfig.DB_HOST,
	port: DbConfig.DB_PORT,
	username: DbConfig.DB_USERNAME,
	password: DbConfig.DB_PASSWORD,
	database: DbConfig.DB_NAME,
	poolSize: 10,
	synchronize: false, // disable sync between entity class and db
	entities: [DbConfig.DB_ENTITY_PATH],
	migrations: [DbConfig.DB_MIGRATION_PATH],
});
