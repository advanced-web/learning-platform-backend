import { BaseEvent, IBaseEvent } from "../../core";

export type EmailEventType =
	| "send_account_activation"
	| "send_group_invitation"
	| "send_reset_password"
	| "send_reset_password_notify"
	| "send_collaborator_invitation";

export class SendEmailEvent implements IBaseEvent<EmailEventType> {
	private _core: IBaseEvent<EmailEventType>;
	constructor() {
		this._core = BaseEvent.instance;
	}

	static instance = new SendEmailEvent();

	public subscribe(eventType: EmailEventType, listener: (...args: any[]) => void): void {
		this._core.subscribe(eventType, listener);
	}

	public unSubscribe(eventType: EmailEventType, listener: (...args: any[]) => void): void {
		this._core.unSubscribe(eventType, listener);
	}

	public notify(eventType: EmailEventType, data: any): void {
		this._core.notify(eventType, data);
	}
}
