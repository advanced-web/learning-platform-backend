import fs from "fs";
import Handlebars from "handlebars";
import * as path from "path";
import { Logger } from "../../common/utils/logger/logger";
import { AppConfig } from "../configs";
import { EmailService } from "../providers";

export interface ISendAccountActivationData {
	to: string;
	token: string;
	userDisplayName?: string;
}

export interface ISendGroupInvitationData {
	to: string;
	groupName: string;
	invitationLink: string;
}

export interface ISendResetPasswordData {
	to: string;
	resetLink: string;
	siteUrl: string;
}

export interface ISendResetPasswordNotifyData {
	to: string;
	forgotLink: string;
}

export interface ISendCollaboratorInvitationData {
	to: string;
	presentationName: string;
	invitationLink: string;
}

export class SendEmailListener {
	private _emailService: EmailService;
	private _logger: Logger;

	constructor() {
		this._emailService = new EmailService();
		this._logger = new Logger("SendEmailListener");
	}

	static instance = new SendEmailListener();

	sendAccountActivatonEmailAsync = async (data: ISendAccountActivationData) => {
		try {
			const link = `${AppConfig.SERVER_URL}/account/activate?token=${data.token}`;
			const content = EmailContentBuilder.init()
				.withTemplateName("account-activation.hbs")
				.withData({
					userDisplayName: data.userDisplayName || "",
					activateLink: link,
					shortenActivateLink: link,
				})
				.getHtml();

			const sendResult = await this._emailService.send(data.to, "[H2A-Platform] - Kích hoạt tài khoản", content);

			// tracking email result
			this._logger.info(`Send activation email to ${data.to} - Success`);
			this._logger.debug(sendResult);
		} catch (error) {
			this._logger.info(`Send activation email to ${data.to} - Error`);
			this._logger.error((error as any).message);
		}
	};

	sendGroupInvitationEmailAsync = async (data: ISendGroupInvitationData) => {
		try {
			const content = EmailContentBuilder.init()
				.withTemplateName("group-invitation.hbs")
				.withData({
					groupName: data.groupName,
					invitationLink: data.invitationLink,
					shortenInvitationLink: data.invitationLink,
				})
				.getHtml();

			const sendResult = this._emailService.send(
				data.to,
				`[H2A-Platform] - Lời mời vào nhóm "${data.groupName}"`,
				content
			);

			// tracking email result
			this._logger.info(`Send invitation email to ${data.to} - Success`);
			this._logger.debug(sendResult);
		} catch (error) {
			this._logger.info(`Send invitation email to ${data.to} - Error`);
			this._logger.error((error as any).message);
		}
	};

	sendResetPasswordEmailAsync = async (data: ISendResetPasswordData) => {
		try {
			const content = EmailContentBuilder.init()
				.withTemplateName("reset-password.hbs")
				.withData({
					siteUrl: data.siteUrl,
					resetLink: data.resetLink,
				})
				.getHtml();
			const sendResult = this._emailService.send(data.to, `[H2A-Platform] - Khôi phục mật khẩu`, content);

			// tracking email result
			this._logger.info(`Send reset password email to ${data.to} - Success`);
			this._logger.debug(sendResult);
		} catch (error) {
			this._logger.info(`Send reset password email to ${data.to} - Error`);
			this._logger.error((error as any).message);
		}
	};

	sendResetPasswordNotifyEmailAsync = async (data: ISendResetPasswordNotifyData) => {
		try {
			const content = EmailContentBuilder.init()
				.withTemplateName("reset-password-notify.hbs")
				.withData({
					forgotLink: data.forgotLink,
				})
				.getHtml();
			const sendResult = this._emailService.send(data.to, `[H2A-Platform] - Thay đổi mật khẩu`, content);

			// tracking email result
			this._logger.info(`Send reset password notify email to ${data.to} - Success`);
			this._logger.debug(sendResult);
		} catch (error) {
			this._logger.info(`Send reset password notify email to ${data.to} - Error`);
			this._logger.error((error as any).message);
		}
	};

	sendCollaboratorInvitationEmailAsync = async (data: ISendCollaboratorInvitationData) => {
		try {
			const content = EmailContentBuilder.init()
				.withTemplateName("collaborator-invitation.hbs")
				.withData({
					presentationName: data.presentationName,
					invitationLink: data.invitationLink,
				})
				.getHtml();
			const sendResult = this._emailService.send(data.to, `[H2A-Platform] - Lời mời cộng tác viên`, content);

			// tracking email result
			this._logger.info(`Send collaborator invitation email to ${data.to} - Success`);
			this._logger.debug(sendResult);
		} catch (error) {
			this._logger.info(`Send collaborator invitation email to ${data.to} - Error`);
			this._logger.error((error as any).message);
		}
	};
}

class EmailContentBuilder {
	private _templateLocation: string;
	private _templateName: string;
	private _data: Record<string, any>;

	constructor() {
		this._templateLocation = path.join(__dirname, "../../views/templates/emails");
	}

	static init(): EmailContentBuilder {
		return new EmailContentBuilder();
	}

	withTemplateName(name: string): EmailContentBuilder {
		this._templateName = name;
		return this;
	}

	withData(data: Record<string, any>) {
		this._data = data;
		return this;
	}

	getHtml(): string {
		const content = fs.readFileSync(`${this._templateLocation}/${this._templateName}`, "utf8");
		const template = Handlebars.compile(content);

		return template(this._data);
	}
}
