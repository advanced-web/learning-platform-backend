import * as dotenv from "dotenv";
dotenv.config();

export const AppConfig = {
	APP_ENV: process.env.APP_ENV || "",
	APP_NAME: process.env.APP_NAME || "",
	CLIENT_MAIN_SITE: process.env.CLIENT_MAIN_SITE || "",
	CLIENT_ACTIVATION_REDIRECT: process.env.CLIENT_ACTIVATION_REDIRECT || "",

	PORT: process.env.PORT ? +process.env.PORT : 5000,
	POWER_BY: process.env.POWER_BY || "",
	SERVER_URL: process.env.SERVER_URL || "",

	ORIGINS: process.env.ORIGINS ? process.env.ORIGINS.split(", ") : "*",
	REQUIRED_CREDENTIALS: Boolean(process.env.REQUIRED_CREDENTIALS).valueOf() || false,

	LOG_LEVEL: process.env.LOG_LEVEL || "info",
	LOG_DRIVER: process.env.LOG_DRIVER || "",
};

/**
 * @deprecated
 */
export const OktaConfig = {
	AUTH_SERVER_URI: process.env.AUTH_SERVER_URI || "",

	CLIENT_ID: process.env.CLIENT_ID || "",
	CLIENT_SECRET: process.env.CLIENT_SECRET || "",

	RESPONSE_TYPE: process.env.RESPONSE_TYPE || "",
	GRANT_TYPE: process.env.GRANT_TYPE || "",
	SCOPE: process.env.SCOPE || "",

	REDIRECT_URI: process.env.REDIRECT_URI || "",

	endpoints: {
		AUTHORIZE: process.env.AUTHORIZE || "",
		TOKEN: process.env.TOKEN || "",
		INTROSPECT: process.env.INTROSPECT || "",
		USERINFO: process.env.USERINFO || "",
	},
};

export const DbConfig = {
	DB_HOST: process.env.DB_HOST || "",
	DB_PORT: process.env.DB_PORT ? +process.env.DB_PORT : 0,
	DB_USERNAME: process.env.DB_USERNAME || "",
	DB_PASSWORD: process.env.DB_PASSWORD || "",
	DB_NAME: process.env.DB_NAME || "",
	DB_ENTITY_PATH: process.env.DB_ENTITY_PATH || "",
	DB_MIGRATION_PATH: process.env.DB_MIGRATION_PATH || "",
};

export const TokenConfig = {
	ACCESS_TOKEN_SCHEME: process.env.ACCESS_TOKEN_SCHEME || "",
	ACCESS_TOKEN_SIGN: process.env.ACCESS_TOKEN_SIGN || "",
	ACCESS_TOKEN_LIFE_TIME: process.env.ACCESS_TOKEN_LIFE_TIME ? +process.env.ACCESS_TOKEN_LIFE_TIME : 0,

	ACTIVATE_TOKEN_SIGN: process.env.ACTIVATE_TOKEN_SIGN || "",
	ACTIVATE_TOKEN_LIFE_TIME: process.env.ACTIVATE_TOKEN_LIFE_TIME ? +process.env.ACTIVATE_TOKEN_LIFE_TIME : 0,
};

export const EmailConfig = {
	HOST: process.env.EMAIL_HOST || "",
	PORT: process.env.EMAIL_PORT ? +process.env.EMAIL_PORT : 0,
	SECURE: Boolean(process.env.EMAIL_SECURE).valueOf(),
	USER: process.env.EMAIL_USER || "",
	PASSWORD: process.env.EMAIL_PASSWORD || "",
};

export const GoogleOAuthConfig = {
	AUTHENTICATION_ENDPOINT:
		process.env.GOOGLE_AUTHENTICATION_ENDPOINT ?? "https://accounts.google.com/o/oauth2/v2/auth",
	VALIDATION_ENDPOINT: process.env.GOOGLE_VALIDATION_ENDPOINT ?? "https://oauth2.googleapis.com/token",
	DATA_ENDPOINT: process.env.GOOGLE_DATA_ENDPOINT ?? "https://www.googleapis.com/userinfo/v2/me",
	CLIENT_ID: process.env.GOOGLE_CLIENT_ID ?? "",
	CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET ?? "",
	RESPONSE_TYPE: process.env.GOOGLE_RESPONSE_TYPE ?? "code",
	SCOPE: process.env.GOOGLE_SCOPE ?? "",
	PROMPT: process.env.GOOGLE_PROMPT ?? "consent",
	GRANT_TYPE: process.env.GOOGLE_GRANT_TYPE ?? "authorization_code",
	ACCESS_TYPE: process.env.GOOGLE_ACCESS_TYPE ?? "offline",
	ACCESS_TOKEN_CONTENT_TYPE: process.env.GOOGLE_ACCESS_TOKEN_CONTENT_TYPE ?? "application/x-www-form-urlencoded",
	REDIRECT_URI: process.env.GOOGLE_REDIRECT_URI ?? "http://localhost:5173/login/callback",
};

export const ImgBBConfig = {
	API_KEY: process.env.IMGBB_API_KEY ?? "",
	UPLOAD_URL: process.env.IMGBB_UPLOAD_URL ?? "https://api.imgbb.com/1/upload",
};

export const InvitationLinkConfig = {
	INVITATION_LINK_SIGN: process.env.INVITATION_LINK_SIGN || "",
};

export const ResetPasswordConfig = {
	REST_PWD_TOKEN_LIFE_TIME: process.env.REST_PWD_TOKEN_LIFE_TIME ? +process.env.REST_PWD_TOKEN_LIFE_TIME : 0,
	REST_PWD_REDIRECT: process.env.REST_PWD_REDIRECT || "",
	REST_PWD_CLIENT: process.env.REST_PWD_CLIENT || "",
};

export const InviteCollaboratorConfig = {
	INVITE_COLLAB_TOKEN_SIGN: process.env.ACTIVATE_TOKEN_SIGN || "",
	INVITE_COLLAB_TOKEN_LIFE_TIME: process.env.ACTIVATE_TOKEN_LIFE_TIME ? +process.env.ACTIVATE_TOKEN_LIFE_TIME : 0,
	INVITE_COLLAB_REDIRECT: process.env.INVITE_COLLAB_REDIRECT || "",
};
