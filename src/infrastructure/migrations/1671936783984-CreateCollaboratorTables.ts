import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateCollaboratorTables1671936783984 implements MigrationInterface {
	name = "CreateCollaboratorTables1671936783984";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			create table if not exists public.presentation_collaborators (
				id serial not null primary key,
				pesentation_series_id uuid not null,
				user_id integer,
				email character varying (190) not null,
				status character varying (100) not null,
				invitation_token text not null,
				invitation_token_key text not null,
				invited_at timestamp with time zone default now(),
				joined_at timestamp with time zone
			)
			with( oids = false ) tablespace pg_default;
		`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			drop table if exists public.presentation_collaborators;
		`);
	}
}
