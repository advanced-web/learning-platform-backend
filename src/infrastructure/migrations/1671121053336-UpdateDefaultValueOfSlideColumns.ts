import { MigrationInterface, QueryRunner } from "typeorm"

export class UpdateDefaultValueOfSlideColumns1671121053336 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`UPDATE "slides" SET "question" = '' where "question" IS NULL`);
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question" set not null`);
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question" set default ''`);

		await queryRunner.query(`UPDATE "slides" SET "question_description" = '' where "question_description" IS NULL`);
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question_description" set not null`);
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question_description" set default ''`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question" drop not null`);
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question" drop default ''`);

		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question_description" drop not null`);
		await queryRunner.query(`ALTER TABLE "slides" ALTER COLUMN "question_description" drop default ''`);
	}

}
