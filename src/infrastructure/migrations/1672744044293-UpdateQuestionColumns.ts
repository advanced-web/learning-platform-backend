import { MigrationInterface, QueryRunner, TableColumn } from "typeorm";

export class UpdateQuestionColumns1672744044293 implements MigrationInterface {
	name = "UpdateQuestionColumns1672744044293";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumns("presentation_questions", [
			new TableColumn({ name: "user_avatar_link", type: "text", isNullable: true }),
			new TableColumn({ name: "is_archive", type: "boolean", default: false, isNullable: true }),
			new TableColumn({ name: "archived_at", type: "timestamp with time zone", isNullable: true }),
		]);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropColumns("presentation_questions", ["user_avatar_link", "is_archive", "archived_at"]);
	}
}
