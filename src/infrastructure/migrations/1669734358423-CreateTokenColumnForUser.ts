import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTokenColumnForUser1669734358423 implements MigrationInterface {
	name = "CreateTokenColumnForUser1669734358423";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "res_user" ADD COLUMN "activation_token" text`);
		await queryRunner.query(`ALTER TABLE "res_user" ADD COLUMN "activation_token_key" text`);
		await queryRunner.query(`ALTER TABLE "res_user" ADD COLUMN "activation_date" timestamp with time zone`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "res_user" DROP COLUMN "activation_token"`);
		await queryRunner.query(`ALTER TABLE "res_user" DROP COLUMN "activation_token_key"`);
		await queryRunner.query(`ALTER TABLE "res_user" DROP COLUMN "activation_date"`);
	}
}
