import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class ChangeVotingResultsTableStructure1671316103348 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("voting_results", true);
		await queryRunner.createTable(
			new Table({
				name: "voting_results",
				columns: [
					{ name: "id", type: "serial", isPrimary: true },
					{ name: "slide_id", type: "integer", isNullable: true },
					{ name: "slide_admin_key", type: "text", isNullable: false },
					{ name: "user_id", type: "integer", isNullable: false },
					{ name: "user_display_name", type: "character varying", isNullable: false, length: "120" },
					{ name: "answer_id", type: "integer", isNullable: false },
					{ name: "voted_at", type: "timestamp with time zone", isNullable: false, default: "now()" },
				],
			}),
			true,
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("voting_results", true);
		await queryRunner.createTable(
			new Table({
				name: "voting_results",
				columns: [
					{ name: "id", type: "serial", isPrimary: true },
					{ name: "slide_id", type: "integer", isNullable: false },
					{ name: "slide_admin_key", type: "text", isNullable: false, isUnique: true },
					{ name: "respondents", type: "integer", isNullable: false, default: 0 },
					{ name: "results", type: "json", isNullable: false },
					{ name: "created_at", type: "timestamp with time zone", isNullable: false, default: "now()" },
					{ name: "updated_at", type: "timestamp with time zone", isNullable: false, default: "now()" },
				],
			}),
			true
		);
	}
}
