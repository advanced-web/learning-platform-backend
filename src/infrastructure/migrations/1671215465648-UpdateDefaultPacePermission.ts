import { MigrationInterface, QueryRunner } from "typeorm"

export class UpdateDefaultPacePermission1671215465648 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`UPDATE "presentations" SET "pace" = "pace"::jsonb || '{"scope": "public", "groupId": null}'::jsonb`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`UPDATE "presentations" SET "pace" = "pace"::jsonb - '{scope,groupId}'::text[]`);
	}

}
