import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTokenColumnForLearningGroup1669895124801 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "learning_group" ADD COLUMN "invitation_token" text`);
		await queryRunner.query(`ALTER TABLE "learning_group" ADD COLUMN "invitation_token_key" text`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "learning_group" DROP COLUMN "invitation_token"`);
		await queryRunner.query(`ALTER TABLE "learning_group" DROP COLUMN "invitation_token_key"`);
	}
}
