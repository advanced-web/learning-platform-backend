import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreatePermissionTable1671227918653 implements MigrationInterface {
	name = "CreatePermissionTable1671227918653";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			create table if not exists public.user_permissions (
				id serial not null,
				subject_id integer not null,
				resource_id integer not null,
				resource_type character varying(100) not null,
				"permissions" text not null,
				created_at timestamp with time zone default now(),
				updated_at timestamp with time zone default now()
			)
			with (
				oids = false
			)
			tablespace pg_default;

			alter table public.user_permissions
			add constraint user_permissions_pkey primary key (id);

			create unique index if not exists uidx_user_permissions_user_resource
				on public.user_permissions using btree (subject_id, resource_id, resource_type)
				tablespace pg_default;
		`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			drop index if exists uidx_user_permissions_user_resource;

			drop table if exists public.user_permissions;
		`);
	}
}
