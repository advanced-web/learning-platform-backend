import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateSearchIndexOnGroupTable1671355027458 implements MigrationInterface {
	name = "CreateSearchIndexOnGroupTable1671355027458";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			ALTER TABLE public.learning_group
			ADD COLUMN search_index_col tsvector
			GENERATED ALWAYS AS (to_tsvector('simple', coalesce(vn_unaccent("name"), '') || ' ' || coalesce(vn_unaccent(description), ''))) STORED;
		`);

		await queryRunner.query(
			`CREATE INDEX IF NOT EXISTS idx_name_description_leanring_group ON public.learning_group USING GIN (search_index_col);`
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`drop index if exists idx_name_description_leanring_group;`);
		await queryRunner.query(`alter table public.learning_group drop column search_index_col`);
	}
}
