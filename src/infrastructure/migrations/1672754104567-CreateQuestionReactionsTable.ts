import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateQuestionReactionsTable1672754104567 implements MigrationInterface {
	name = "CreateQuestionReactionsTable1672754104567";

	public async up(queryRunner: QueryRunner): Promise<void> {
		const table = new Table({
			name: "question_reactions",
			columns: [
				{ name: "question_id", type: "integer", isNullable: false },
				{ name: "user_id", type: "integer", isNullable: false },
				{ name: "reaction_type", type: "character varying", length: "50", isNullable: false },
				{ name: "reacted_at", type: "timestamp with time zone", default: "now()", isNullable: true },
			],
		});
		await queryRunner.createTable(table, true, false);

		await queryRunner.createPrimaryKey(
			table,
			["question_id", "user_id", "reaction_type"],
			"question_reaction_user_pkey"
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("question_reactions", true);
	}
}
