import { MigrationInterface, QueryRunner } from "typeorm";

export class AddPresentationRefToGroup1671413085636 implements MigrationInterface {
	name = "AddPresentationRefToGroup1671413085636";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			alter table public.learning_group
			add column presentation_series_id uuid;
		`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`alter table public.learning_group drop column presentation_series_id;`);
	}
}
