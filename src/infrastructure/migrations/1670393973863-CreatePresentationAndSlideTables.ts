import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreatePresentationAndSlideTables1670393973863 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			create table if not exists presentations (
				id serial,
				name character varying(100) not null,
				series_id uuid default gen_random_uuid(),
				vote_key text not null,
				owner_id integer,
				owner_display_name character varying(120),
				pace json,
				closed_for_voting boolean default false,
				slide_count integer not null default 0,
				created_at timestamp with time zone default now(),
				updated_at timestamp with time zone default now()
			);

			alter table presentations
			add constraint presentation_pkey primary key (id);
		`);

		await queryRunner.query(`
			create table if not exists slides (
				id serial,
				admin_key text not null,
				presentation_id integer,
				presentation_series_id text,
				question text,
				question_description text,
				question_image_url text,
				question_video_url text,
				type character varying(40) not null,
				active boolean default true,
				hide_instruction_bar boolean default false,
				speaker_notes text,
				choices json,
				config json,
				position integer not null,
				created_at timestamp with time zone default now(),
				updated_at timestamp with time zone default now()
			);

			alter table slides
			add constraint slides_pkey primary key (id);

			alter table slides
			add constraint slides_admin_key_unique unique (admin_key);
		`);

		await queryRunner.query(`
			create table if not exists voting_results (
				id serial,
				slide_id integer,
				slide_admin_key text,
				respondents integer not null default 0,
				results json,
				created_at timestamp with time zone default now(),
				updated_at timestamp with time zone default now()
			);

			alter table voting_results
			add constraint voting_results_pkey primary key (id);

			alter table voting_results
			add constraint voting_results_admin_key_unique unique (slide_admin_key);
		`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> { }
}
