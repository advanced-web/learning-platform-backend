import { MigrationInterface, QueryRunner } from "typeorm"

export class CreatePresentationChatsTable1672458595276 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			create table if not exists presentation_chats (
				id serial not null primary key,
				presentation_series_id uuid not null,
				present_no integer not null,
				user_id integer not null,
				user_display_name character varying(120) not null,
				user_avatar_link text,
				message text not null,
				created_at timestamp with time zone default now(),
  				updated_at timestamp with time zone default now()
			);
		`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			drop table if exists presentation_chats;
		`);
	}
}
