import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateResetPasswordTable1671082358126 implements MigrationInterface {
	name = "CreateResetPasswordTable1671082358126";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: "reset_password_requests",
				columns: [
					{ name: "id", type: "serial", isPrimary: true },
					{ name: "email", type: "character varying", isNullable: false, length: "190", isUnique: true },
					{ name: "reset_token", type: "text", isNullable: false },
					{ name: "expire_at", type: "timestamp with time zone", isNullable: false },
					{ name: "created_at", type: "timestamp with time zone", default: "now()", isNullable: true },
					{ name: "updated_at", type: "timestamp with time zone", default: "now()", isNullable: true },
				],
			}),
			true,
			false
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable("reset_password_requests", true);
	}
}
