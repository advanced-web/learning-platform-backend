import { MigrationInterface, QueryRunner } from "typeorm"

export class AddSlideTextSizeColumn1670476718533 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "slides" ADD COLUMN "text_size" integer default 32`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "slides" DROP COLUMN "text_size"`);
	}
}
