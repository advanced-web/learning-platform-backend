import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateVoteKeyForGroup1672353936589 implements MigrationInterface {
	name = "UpdateVoteKeyForGroup1672353936589";

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			alter table public.learning_group add column vote_key text;
		`);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			alter table public.learning_group drop column vote_key;
		`);
	}
}
