import { MigrationInterface, QueryRunner } from "typeorm"

export class CreatePresentationQuestionsTable1672459737993 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			create table if not exists presentation_questions (
				id serial not null primary key,
				presentation_series_id uuid not null,
				question text not null,
				total_like integer not null default 0,
				user_id integer not null,
				user_display_name character varying(120) not null,
				created_at timestamp with time zone default now(),
  				updated_at timestamp with time zone default now()
			);
		`);
    }

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`
			drop table if exists presentation_questions;
		`);
    }
}
