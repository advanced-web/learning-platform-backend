import cors from "cors";
import express from "express";
import { create } from "express-handlebars";
import http from "http";
import passport from "passport";
import "reflect-metadata";
import { errorHandler } from "./common";
import { loggingMiddleware } from "./common/middlewares/logging.middleware";
import { Logger } from "./common/utils/logger/logger";
import { ControllerList } from "./controllers";
import { BaseController } from "./controllers/base.controller";
import { registerEvent } from "./event-registration";
import { AppConfig, defaultDataSource, ServerSocket } from "./infrastructure";
import { HTTP_CODE } from "./shared";

async function bootstrap() {
	const app = express();

	// create http and socket servers
	const httpServer = http.createServer(app);
	new ServerSocket(httpServer);

	app.use(express.json()); // parsing application/json
	app.use(express.urlencoded({ extended: true }));
	app.use(
		cors({
			origin: AppConfig.ORIGINS,
			credentials: AppConfig.REQUIRED_CREDENTIALS,
			methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
			allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"],
		})
	);
	app.use((req, res, next) => {
		res.setHeader("X-Powered-By", AppConfig.POWER_BY);
		next();
	});
	app.use(loggingMiddleware);

	// view engine
	const handlebars = create({
		layoutsDir: `${__dirname}/views/layouts`,
		partialsDir: `${__dirname}/views/partials`,
		defaultLayout: "main",
		extname: "hbs",
		helpers: {},
	});

	// HandlebarsSection(handlebars);
	app.engine("hbs", handlebars.engine);
	app.set("view engine", "hbs");
	app.set("views", `${__dirname}/views`);

	app.use(passport.initialize());

	// init routes
	ControllerList.forEach((controller: BaseController) => {
		app.use(controller.basePath(), controller.router());
	});

	// handle not found route
	app.use((req, res, next) => {
		res.status(HTTP_CODE.NOT_FOUND).send();
	});

	// handle error
	app.use(errorHandler);

	// init data source connection
	try {
		await defaultDataSource.initialize();
		Logger.info("Database connected", "Bootstrap");

		// start server
		const PORT = AppConfig.PORT;

		httpServer.listen(PORT, () => {
			Logger.info(`Server is ready on port ${PORT}`, "Bootstrap");
		});

		registerEvent();
	} catch (error) {
		Logger.error("Cannot connect to database", "Bootstrap");
		console.error(error);
	}
}
bootstrap();
