import { SendEmailEvent, SendEmailListener } from "./infrastructure";

export function registerEvent() {
	SendEmailEvent.instance.subscribe(
		"send_account_activation",
		SendEmailListener.instance.sendAccountActivatonEmailAsync
	);

	SendEmailEvent.instance.subscribe(
		"send_group_invitation",
		SendEmailListener.instance.sendGroupInvitationEmailAsync
	);

	SendEmailEvent.instance.subscribe("send_reset_password", SendEmailListener.instance.sendResetPasswordEmailAsync);

	SendEmailEvent.instance.subscribe(
		"send_reset_password_notify",
		SendEmailListener.instance.sendResetPasswordNotifyEmailAsync
	);

	SendEmailEvent.instance.subscribe(
		"send_collaborator_invitation",
		SendEmailListener.instance.sendCollaboratorInvitationEmailAsync
	);
}
