import jwt, { Algorithm, JwtHeader } from "jsonwebtoken";

export class JwtBuilder {
	private _header?: JwtHeader;

	private _payload: Record<string, any>;

	// More options
	private _algorithm?: Algorithm;
	private _expiresIn?: string | number;
	private _notBefore?: string | number;
	private _keyid?: string;
	private _audience?: string | string[];
	private _subject?: string;
	private _issuer?: string;
	private _jwtid?: string;
	private _mutatePayload?: boolean;
	private _noTimestamp?: boolean;
	private _encoding?: string;

	public static init(): JwtBuilder {
		return new JwtBuilder();
	}

	sign(secretOrPublicKey: string | Buffer): string {
		const options = {
			algorithm: this._algorithm,
			expiresIn: this._expiresIn,
			keyid: this._keyid,
			notBefore: this._notBefore,
			audience: this._audience,
			subject: this._subject,
			issuer: this._issuer,
			jwtid: this._jwtid,
			mutatePayload: this._mutatePayload,
			noTimestamp: this._noTimestamp,
			header: this._header,
			encoding: this._encoding,
		};
		Object.keys(options).forEach((key) => (options[key] === undefined ? delete options[key] : {}));

		return jwt.sign(this._payload, secretOrPublicKey, options);
	}

	withPayload(payload: Record<string, any>): JwtBuilder {
		this._payload = payload;
		return this;
	}

	withAlgorithm(algo: Algorithm): JwtBuilder {
		this._algorithm = algo;
		return this;
	}

	withExpiresIn(exp: string | number): JwtBuilder {
		this._expiresIn = exp;
		return this;
	}

	withNotBefore(notBefore: string | number): JwtBuilder {
		this._notBefore = notBefore;
		return this;
	}

	withKeyId(keyId: string): JwtBuilder {
		this._keyid = keyId;
		return this;
	}

	withAudience(aud: string | string[]): JwtBuilder {
		this._audience = aud;
		return this;
	}

	withSubject(sub: string): JwtBuilder {
		this._subject = sub;
		return this;
	}

	withIssuer(iss: string): JwtBuilder {
		this._issuer = iss;
		return this;
	}

	withJwtId(id: string): JwtBuilder {
		this._jwtid = id;
		return this;
	}

	withMutatePayload(canMutate: boolean): JwtBuilder {
		this._mutatePayload = canMutate;
		return this;
	}

	withNoTimestamp(noTimestamp: boolean): JwtBuilder {
		this._noTimestamp = noTimestamp;
		return this;
	}

	withEncoding(encoding: string): JwtBuilder {
		this._encoding = encoding;
		return this;
	}
}
