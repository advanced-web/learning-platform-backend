import jwt, { Jwt, JwtPayload } from "jsonwebtoken";

export class JwtDecoder {
	static init(): JwtDecoder {
		return new JwtDecoder();
	}

	decodePayload(token: string): JwtPayload | null {
		return jwt.decode(token, { json: true });
	}

	decode(token: string): Jwt | null {
		return jwt.decode(token, { complete: true });
	}
}
