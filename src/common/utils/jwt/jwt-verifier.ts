import jwt, { Jwt, VerifyErrors } from "jsonwebtoken";
import { TokenConfig } from "../../../infrastructure";

export class JwtVerifier {
	static init(): JwtVerifier {
		return new JwtVerifier();
	}

	verifiyBaseAsync(token: string): Promise<[Jwt | null, VerifyErrors | null]> {
		return new Promise(function (resolve, reject) {
			const key = TokenConfig.ACCESS_TOKEN_SIGN;

			jwt.verify(token, key, { complete: true }, function (err, payload) {
				if (err) {
					return resolve([null, err]);
				}

				resolve([payload || null, null]);
			});
		});
	}
}
