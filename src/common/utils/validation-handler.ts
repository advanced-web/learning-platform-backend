import { ValidationErrorItem } from 'joi';
import { IBaseSlideChoice, PresentationPaceScope } from '../../core';
import { SLIDE_TEXT_PROPERTY_LENGTHS, SLIDE_TYPE } from '../../shared';
import { ValueGenerator } from './value-generator';

export class ValidationHandler {
	static instance = new ValidationHandler();
	private constructor() {}

	validateQuestionLength(slideType: SLIDE_TYPE, question: string) {
		let maxLength = Number.MAX_SAFE_INTEGER;

		switch (slideType) {
			case SLIDE_TYPE.MULTIPLE_CHOICE:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.MULTIPLE_CHOICE_SLIDE_QUESTION;
				break;
			case SLIDE_TYPE.PARAGRAPH:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.PARAGRAPH_SLIDE_QUESTION;
				break;
			case SLIDE_TYPE.HEADING:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.HEADING_SLIDE_QUESTION;
				break;
		}

		return question.length <= maxLength;
	}

	validateQuestionDescriptionLength(slideType: SLIDE_TYPE, questionDescription: string) {
		let maxLength = Number.MAX_SAFE_INTEGER;

		switch (slideType) {
			case SLIDE_TYPE.MULTIPLE_CHOICE:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.MULTIPLE_CHOICE_SLIDE_QUESTION_DESCRIPTION;
				break;
			case SLIDE_TYPE.PARAGRAPH:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.PARAGRAPH_SLIDE_QUESTION_DESCRIPTION;
				break;
			case SLIDE_TYPE.HEADING:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.HEADING_SLIDE_QUESTION_DESCRIPTION;
				break;
		}

		return questionDescription.length <= maxLength;
	}

	validateAnswerLength(slideType: SLIDE_TYPE, answer: string) {
		let maxLength = Number.MAX_SAFE_INTEGER;

		switch (slideType) {
			case SLIDE_TYPE.MULTIPLE_CHOICE:
				maxLength = SLIDE_TEXT_PROPERTY_LENGTHS.MULTIPLE_CHOICE_SLIDE_ANSWER;
				break;
			case SLIDE_TYPE.PARAGRAPH, SLIDE_TYPE.HEADING:
				return true;
		}

		return typeof answer === "string" && answer.length <= maxLength;
	}

	validateAnswerListLength(slideType: SLIDE_TYPE, answers: string[]) {
		return Array.isArray(answers)
			&& (answers.length === 0 || answers.every(answer => this.validateAnswerLength(slideType, answer)));
	}

	validateSlideChoiceStructure(slideType: SLIDE_TYPE, choice: IBaseSlideChoice) {
		const schema = ValueGenerator.instance.generateSlideChoiceStructureSchema(slideType);
		const { error } = schema.validate(choice);

		return error ? error.details : undefined;
	}

	validateSlideChoiceListStructure(slideType: SLIDE_TYPE, choices: IBaseSlideChoice[]) {
		const schema = ValueGenerator.instance.generateSlideChoiceStructureSchema(slideType);
		const validationErrorItems: ValidationErrorItem[] = [];

		choices.forEach(choice => {
			const { error } = schema.validate(choice);

			if (error) {
				validationErrorItems.push(...error.details);
			}
		});

		// Check duplicate of choice id
		const choiceIds = choices.map(choice => choice.id);
		const setChoiceIds = new Set(choiceIds);
		if (setChoiceIds.size < choiceIds.length) {
			validationErrorItems.push({
				message: "Danh sách đáp án không hợp lệ",
				path: ["choice", "id"],
				type: "item.duplicate",
				context: {
					child: "choice.id",
					label: "choice.id",
					value: choiceIds,
					key: "choice.id",
				}
			});
		}

		// Check duplicate of position id
		const positionIds = choices.map(choice => choice.position);
		const setpositionIds = new Set(positionIds);
		if (setpositionIds.size < positionIds.length) {
			validationErrorItems.push({
				message: "Danh sách đáp án không hợp lệ",
				path: ["position", "id"],
				type: "item.duplicate",
				context: {
					child: "position.id",
					label: "position.id",
					value: positionIds,
					key: "position.id",
				}
			});
		}

		return validationErrorItems;
	}

	validatePresentationPacePermissionStructure(data: object) {
		const schema = ValueGenerator.instance.generatePresentationPacePermissionSchema();
		const { error } = schema.validate(data);

		if (error) {
			return error.details;
		}

		return [];
	}

	validatePresentationPacePermission(scope: PresentationPaceScope, groupId: number | null) {
		return (scope === "public" && groupId === null) || (scope === "group" && groupId !== null && groupId > 0);
	}
}
