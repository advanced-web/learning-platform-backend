import Joi from 'joi';
import { IBaseSlideChoice, IMultipleChoiceSlideChoice, PresentationPace } from '../../core';
import { DEFAULT_MULTIPLE_CHOICE_OPTIONS, PRESENTATION_PACE_SCOPES, SLIDE_TYPE } from '../../shared';

export class ValueGenerator {
	static instance = new ValueGenerator();
	private constructor() {}

	generateSlideChoices(slideType: SLIDE_TYPE) {
		const slideChoices: IBaseSlideChoice[] = [];

		if (slideType === SLIDE_TYPE.MULTIPLE_CHOICE) {
			for (let i = 0; i < DEFAULT_MULTIPLE_CHOICE_OPTIONS; i++) {
				const multipleChoiceItem: IMultipleChoiceSlideChoice = {
					id: i,
					label: `Lựa chọn ${i + 1}`,
					position: i,
					correctAnswer: false,
				};

				slideChoices.push(multipleChoiceItem);
			}
		}

		return slideChoices;
	}

	generateSlideChoiceStructureSchema(slideType: SLIDE_TYPE) {
		let schema = Joi.object({
			id: Joi
				.number()
				.integer()
				.greater(-1)
				.required()
				.messages({
					"any.required": "Đáp án không hợp lệ",
					"number.base": "Đáp án không hợp lệ",
					"number.greater": "Đáp án không hợp lệ",
				}),
			position: Joi
				.number()
				.integer()
				.greater(-1)
				.required()
				.messages({
					"any.required": "Thứ tự đáp án không hợp lệ",
					"number.base": "Thứ tự đáp án không hợp lệ",
					"number.greater": "Thứ tự đáp án không hợp lệ",
				}),
		});

		if (slideType === SLIDE_TYPE.MULTIPLE_CHOICE) {
			schema = schema.append({
				correctAnswer: Joi
					.boolean()
					.required()
					.messages({
						"any.required": "Trạng thái đúng/sai của đáp án là bắt buộc",
						"boolean.base": "Trạng thái đúng/sai của đáp án không hợp lệ",
					}),
				label: Joi
					.string()
					.max(50)
					.required()
					.messages({
						"any.required": "Mô tả đáp án là bắt buộc",
						"string.base": "Đáp án không hợp lệ",
						"string.empty": "Mô tả đáp án là bắt buộc",
						"string.max": "Đáp án không hợp lệ",
					}),
			});
		} else {
			schema = schema.append({
				label: Joi
					.string()
					.required()
					.messages({
						"any.required": "Mô tả đáp án là bắt buộc",
						"string.base": "Đáp án không hợp lệ",
						"string.empty": "Mô tả đáp án là bắt buộc",
						"string.max": "Đáp án không hợp lệ",
					}),
			});
		}

		return schema
			.options({ allowUnknown: false })
			.messages({ "object.unknown": "Danh sách đáp án không hợp lệ" });
	}

	generatePresentationPace(): PresentationPace {
		return {
			mode: "presenter",
			active: "",
			state: "idle",
			counter: 0,
			scope: "public",
			groupId: null,
		};
	}

	generatePresentationPacePermissionSchema() {
		const schema = Joi.object({
			groupId: Joi
				.number()
				.allow(null)
				.integer()
				.greater(0)
				.required()
				.messages({
					"any.required": "Nhóm không hợp lệ",
					"number.base": "Nhóm không hợp lệ",
					"number.greater": "Nhóm không hợp lệ",
				}),
			scope: Joi
				.string()
				.valid(...Object.values(PRESENTATION_PACE_SCOPES))
				.required()
				.messages({
					"any.required": "Nơi trình chiếu không được bỏ trống",
					"any.only": "Nơi trình chiếu không hợp lệ",
					"string.base": "Nơi trình chiếu không hợp lệ",
					"string.empty": "Nơi trình chiếu không hợp lệ",
				}),
		}).options({ allowUnknown: true });

		return schema;
	}
}
