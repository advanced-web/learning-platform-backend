import stringify from "fast-safe-stringify";
import "winston-daily-rotate-file";
import { WinstonLogger } from "./core-logger";

export class Logger {
	constructor(private context: string = "Application") {}

	debug(message: any, context?: string) {
		if (context) {
			this.context = context;
		}
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "debug", message: msg, context: this.context });
	}

	info(message: any, context?: string) {
		if (context) {
			this.context = context;
		}
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "info", message: msg, context: this.context });
	}

	warn(message: any, context?: string) {
		if (context) {
			this.context = context;
		}
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "warn", message: msg, context: this.context });
	}

	error(message: any, context?: string) {
		if (context) {
			this.context = context;
		}
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "error", message: msg, context: this.context });
	}

	static debug(message: any, context?: string) {
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "debug", message: msg, context: context || "Application" });
	}

	static info(message: any, context?: string) {
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "info", message: msg, context: context || "Application" });
	}

	static warn(message: any, context?: string) {
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "warn", message: msg, context: context || "Application" });
	}

	static error(message: any, context?: string) {
		const msg = typeof message === "object" ? stringify(message) : message;
		WinstonLogger.log({ level: "error", message: msg, context: context || "Application" });
	}
}
