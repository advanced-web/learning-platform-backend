import moment from "moment";
import * as winston from "winston";
import "winston-daily-rotate-file";
import { AppConfig } from "../../../infrastructure";

const format = winston.format.combine(
	winston.format.timestamp(),
	winston.format.ms(),
	winston.format.printf(({ context, level, timestamp, ms, message }) => {
		const timestampString = moment(timestamp).toISOString();
		return `[${timestampString}] : [${level}] : [${context}] : ${message} ${ms}`;
	})
);

const FileTransport = new winston.transports.DailyRotateFile({
	filename: "app-%DATE%",
	extension: ".log",
	dirname: "logs",
	datePattern: "YYYY-MM-DD",
	maxSize: "20m",
	maxFiles: "30d",
});

const ConsoleTransport = new winston.transports.Console();

const logTransports: winston.transport[] = [];
if (AppConfig.LOG_DRIVER === "file") logTransports.push(FileTransport);
if (AppConfig.LOG_DRIVER === "console") logTransports.push(ConsoleTransport);

export const WinstonLogger = winston.createLogger({
	transports: logTransports,
	format: format,
	level: AppConfig.LOG_LEVEL,
});
