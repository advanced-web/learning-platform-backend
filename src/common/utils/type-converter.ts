import { SLIDE_TYPE } from '../../shared';

export class TypeConverter {
	static instance = new TypeConverter();
	private constructor() {}

	stringToSlideTypeEnum(slideTypeString: string) {
		switch (slideTypeString) {
			case SLIDE_TYPE.MULTIPLE_CHOICE:
				return SLIDE_TYPE.MULTIPLE_CHOICE;
			case SLIDE_TYPE.PARAGRAPH:
				return SLIDE_TYPE.PARAGRAPH;
			case SLIDE_TYPE.HEADING:
				return SLIDE_TYPE.HEADING;
			default:
				return undefined;
		}
	}
}
