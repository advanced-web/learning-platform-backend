import { NextFunction, Request, Response } from "express";
import { HttpErrorBuilder } from "../../core";
import { UserPermissionService } from "../../services";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";

export function ResourcePermission(
	requiredPermissions: string[],
	detachResourceCb: (request: Request, response: Response) => { resourceId: number; resourceType: string }
): MethodDecorator {
	return function (target: Object, propertyKey: string | symbol, descriptor: PropertyDescriptor) {
		const origionalValue = descriptor.value;
		const userPermissionService = new UserPermissionService();

		descriptor.value = async function (...args: any[]) {
			const request = args[0] as Request;
			const response = args[1] as Response;
			const next = args[2] as NextFunction;

			// assume this is public routes
			if (requiredPermissions.length === 0) {
				return await origionalValue.apply(this, args);
			}

			// ensure this mid runs after auth mid
			const userinfo = request.userinfo;
			if (!userinfo || !userinfo.uid) {
				return next(
					HttpErrorBuilder.init()
						.withStatusCode(HTTP_CODE.UNAUTHORIZED)
						.withResponseCode(RESPONSE_CODE.INVALID_TOKEN)
						.build()
				);
			}

			// check the user permission with resource information
			try {
				const { resourceId, resourceType } = detachResourceCb(request, response);

				const result = await userPermissionService.verifyUserPermissionAsync(
					userinfo.uid,
					resourceId,
					resourceType,
					requiredPermissions
				);

				if (!result) {
					return next(
						HttpErrorBuilder.init()
							.withStatusCode(HTTP_CODE.FORBIDDEN)
							.withResponseCode(RESPONSE_CODE.INVALID_RESOURCE_PERMISSION)
							.build()
					);
				}

				return await origionalValue.apply(this, args);
			} catch (error) {
				console.log(error);
			}
		};
	};
}
