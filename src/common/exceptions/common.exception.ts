import statuses from "statuses";
import { BaseException } from ".";
import { IBaseResponseObject } from "../../core";
import { API_MESSAGES, HTTP_CODE } from "../../shared";

export class MiddlewareException extends BaseException {
	constructor(message: string, code?: number, exceptionDetail?: any, responseData?: any) {
		super("MiddlewareException", message, code, exceptionDetail);
	}
}

export class ControllerException extends BaseException {
	constructor(message: string, code?: number, exceptionDetail?: any, responseData?: any) {
		super("ControllerException", message, code, exceptionDetail);
	}
}

export class InfrastructureException extends BaseException {
	constructor(message: string, code?: number, exceptionDetail?: any, responseData?: any) {
		super("InfrastructureException", message, code, exceptionDetail);
	}
}

export class ServiceException extends BaseException {
	constructor(message: string, code?: number, exceptionDetail?: any, responseData?: any) {
		super("ServiceException", message, code, exceptionDetail);
	}
}

export class HttpError extends Error {
	statusCode: number;
	responseCode: number;
	detail?: any;

	constructor(statusCode: number, code?: number, message?: string, exceptionDetail?: any) {
		super(message);

		// determine what kind of error
		this.name = this.constructor.name;

		this.statusCode = statusCode;
		this.responseCode = code || HTTP_CODE.INTERNAL_SERVER_ERROR;
		this.message = message ?? statuses.message[this.responseCode] ?? API_MESSAGES.INTERNAL_ERROR;
		this.detail = exceptionDetail;

		// enable tracing error
		Error.captureStackTrace(this, this.constructor);
	}

	toJSON(): IBaseResponseObject {
		return {
			code: this.responseCode,
			message: this.message,
			errors: this.detail,
			metadata: {
				createdDate: new Date().toISOString(),
			},
		};
	}
}
