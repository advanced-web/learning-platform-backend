import { NextFunction, Request, Response } from "express";
import { HttpErrorBuilder } from "../../core";
import { AppConfig } from "../../infrastructure";
import { HTTP_CODE } from "../../shared";
import { BaseException, HttpError } from "../exceptions";
import { Logger } from "../utils/logger/logger";

export function errorHandler(error: any, request: Request, response: Response, next: NextFunction) {
	if (error instanceof HttpError) {
		const statusCode = error.statusCode
		const responseData = error.toJSON()
		Logger.error(JSON.stringify(responseData), "ErrorHandler");

		return response.status(statusCode).json(responseData)
	} else if (error instanceof BaseException) { // clear after refactoring the error in the entire project
		Logger.error(JSON.stringify(error.toErrorMessage()), "ErrorHandler");

		const responseObject = {
			code: error.code,
			message: error.message,
			errors: error.exceptionDetail,
			metadata: { createdDate: new Date().toISOString() },
		};

		switch (error.context) {
			case "MiddlewareException":
				response.status(error.code).json(responseObject);
				break;
			default:
				response.status(200).json(responseObject);
				break;
		}
		return;
	}

	// runtime error
	Logger.error({ stack: error.stack }, "ErrorHandler");

	const responseObject = HttpErrorBuilder.init()
		.withStatusCode(HTTP_CODE.INTERNAL_SERVER_ERROR)
		.build();

	if (AppConfig.APP_ENV !== "production") {
		responseObject["errors"] = error.stack;
	}

	response.status(responseObject.statusCode).json(responseObject);
}
