import { NextFunction, Request, Response } from "express";
import { ObjectSchema } from "joi";
import { HttpErrorBuilder } from "../../core";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";

export type RequestValidationType = "body" | "query" | "params" | "headers" | "fields";

// ! Need to be refactor with dynamic type
// https://joi.dev/api/?v=17.7.0#anyvalidatevalue-options
export const validateRequestMiddleware =
	(type: RequestValidationType, schema: ObjectSchema) => (req: Request, res: Response, next: NextFunction) => {
		const { error, value } = schema.validate(req[type]);

		if (error) {
			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.VALIDATION_ERROR)
				.withErrors(error.details)
				.build();
			next(httpError);
			return;
		}

		req[type] = value;
		next();
	};
