import { NextFunction, Request, Response } from "express";
import { HttpErrorBuilder } from "../../core";
import { LearningGroupMemberRepository } from "../../repositories";
import { GROUP_MEMBER_ACTIVITY_KEYS, HTTP_CODE, RESPONSE_CODE } from "../../shared";
import { Logger } from "../utils/logger/logger";

export async function verifyGroupMemberHasJoinedAsync(request: Request, response: Response, next: NextFunction) {
	const { uid } = request.userinfo;
	const groupId = parseInt(request.params.id);
	const statusList = [GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP, GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP];

	const queryBuilder = LearningGroupMemberRepository.createQueryBuilder("member")
		.select("id")
		.where("member.group_id = :groupId", { groupId })
		.andWhere("member.user_profile_id = :uid", { uid })
		.andWhere("member.current_status->>'key' IN (:...statusList)", { statusList });

	Logger.debug(`verifyGroupMemberHasJoinedAsync - ${queryBuilder.getSql()}`, "Query");

	try {
		const groupMemberId = await queryBuilder.getRawOne<{ id: number }>();
		if (groupMemberId) {
			next();
		} else {
			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.GROUP_NOT_FOUND)
				.build();
			next(httpError);
		}
	} catch (error) {
		next(error);
	}
}
