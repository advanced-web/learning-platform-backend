import { Request } from "express";
import multer from "multer";

type MulterStorageOptionsCallback = (error: Error | null, destinationOrFilename: string) => void;

const storage = multer.diskStorage({
	destination: function (request: Request, file: Express.Multer.File, callback: MulterStorageOptionsCallback) {
		callback(null, "uploads/");
	},
	filename: function (request: Request, file: Express.Multer.File, callback: MulterStorageOptionsCallback) {
		const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
		callback(null, file.fieldname + "-" + uniqueSuffix);
	},
});

export const multerMiddleware = multer({ storage });
