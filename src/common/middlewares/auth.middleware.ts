import { NextFunction, Request, Response } from "express";
import { HttpErrorBuilder } from "../../core";
import { TokenConfig } from "../../infrastructure";
import { ResUserRepository } from "../../repositories";
import { AuthService } from "../../services";
import { HTTP_CODE, RESPONSE_CODE } from "../../shared";
import { JwtVerifier } from "../utils";

// validate access token and attach token information in to request object
/**
 *
 * @deprecated
 */
export async function authMiddleware(request: Request, response: Response, next: NextFunction) {
	const authorization = request.headers.authorization || "";
	const token = authorization.split(" ")[1];

	if (!token) {
		const httpError = HttpErrorBuilder.init()
			.withStatusCode(HTTP_CODE.UNAUTHORIZED)
			.withResponseCode(RESPONSE_CODE.MISSING_TOKEN)
			.withErrors({ detail: authorization })
			.build();

		next(httpError);
		return;
	}

	// send validation request to IdP
	const authService = new AuthService();
	const introspectResult = await authService.introspectTokenAsync(token);
	if (!introspectResult.active) {
		const httpError = HttpErrorBuilder.init()
			.withStatusCode(HTTP_CODE.UNAUTHORIZED)
			.withResponseCode(RESPONSE_CODE.MISSING_TOKEN)
			.build();

		next(httpError);
		return;
	}

	// request.user = introspectResult;
	next();
}

export function authMiddlewareV2(request: Request, response: Response, next: NextFunction) {
	const authorization = request.headers.authorization || "";
	const [scheme, token] = authorization.split(" ");

	// Update: validate token base scheme
	if (!scheme || scheme !== TokenConfig.ACCESS_TOKEN_SCHEME || !token) {
		const httpError = HttpErrorBuilder.init()
			.withStatusCode(HTTP_CODE.UNAUTHORIZED)
			.withResponseCode(RESPONSE_CODE.MISSING_TOKEN)
			.build();

		next(httpError);
		return;
	}

	JwtVerifier.init()
		.verifiyBaseAsync(token)
		.then(([payload, error]) => {
			if (error || !payload) {
				const httpError = HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.UNAUTHORIZED)
					.withResponseCode(RESPONSE_CODE.INVALID_TOKEN)
					.build();

				next(httpError);
				return;
			}

			request.userinfo = {
				uid: payload.payload["uid"],
				username: payload.payload["username"],
				email: payload.payload["email"],
				email_verified: payload.payload["email_verified"],
				avatar: payload.payload["avatar"],
				display_name: payload.payload["display_name"],
			};
			next();
		});
}

export async function verifyUserActivationMiddlewareAsync(request: Request, response: Response, next: NextFunction) {
	const { uid } = request.userinfo;
	const user = await ResUserRepository.findOneBy({ id: uid });

	// inactive user => 4031
	// else => passed
	if (user) {
		request.fetchedUser = user;
		if (!user.isActive) {
			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.FORBIDDEN)
				.withResponseCode(RESPONSE_CODE.INACTIVE_USER)
				.build();

			next(httpError);
		} else {
			next();
		}

		return;
	}

	// the remaining cases
	const httpError = HttpErrorBuilder.init()
		.withStatusCode(HTTP_CODE.UNAUTHORIZED)
		.withResponseCode(RESPONSE_CODE.INVALID_TOKEN)
		.build();

	next(httpError);
}
