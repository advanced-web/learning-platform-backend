import { NextFunction, Request, Response } from "express";
import _ from "lodash";
import { In } from "typeorm";
import { HttpErrorBuilder } from "../../core";
import { LearningGroupMemberService, PresentationCollaboratorService, PresentationsService } from "../../services";
import {
	GROUP_MEMBER_ACTIVITY_KEYS,
	GROUP_MEMBER_ROLE,
	HTTP_CODE,
	PRESENTATION_COLLAB_STATUS,
	PRESENTATION_PACE_SCOPES,
	RESPONSE_CODE,
} from "../../shared";

/**
 *
 * @description Throws 40017, 4037
 */
export async function verifyPresentationVotingLinkPermission(request: Request, response: Response, next: NextFunction) {
	const { uid } = request.userinfo;
	const voteKey = request.params.vote_key;

	const presentationsService = new PresentationsService();
	const presentation = await presentationsService.getRecordByAsync({ voteKey });

	// Presentation not found
	if (!presentation) {
		const httpError = HttpErrorBuilder.init()
			.withStatusCode(HTTP_CODE.BAD_REQUEST)
			.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
			.build();
		next(httpError);
		return;
	}

	const { scope, groupId } = presentation.pace;

	if (scope === PRESENTATION_PACE_SCOPES.PUBLIC) {
		request.presentation = presentation;
		next();
	} else {
		const statusList = [GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP, GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP];
		const learningGroupMemberService = new LearningGroupMemberService();
		const isExisted = await learningGroupMemberService.existsByStatusListAsync(groupId ?? 0, uid, statusList);

		if (isExisted) {
			request.presentation = presentation;
			next();
		} else {
			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.FORBIDDEN)
				.withResponseCode(RESPONSE_CODE.JOIN_VOTING_PERMISSION)
				.build();
			next(httpError);
		}
	}
}

/**
 *
 * @description Throws 40017
 */
export function preGetPresentationAsync(seriesIdPath: string) {
	return async function (request: Request, response: Response, next: NextFunction) {
		try {
			const series_id = _.get(request, seriesIdPath, "") as string;

			if (!series_id) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.build();
			}

			const presentationsService = new PresentationsService();
			const presentation = await presentationsService.getRecordByAsync({ seriesId: series_id });
			// Presentation not found
			if (!presentation) {
				throw HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.BAD_REQUEST)
					.withResponseCode(RESPONSE_CODE.PRESENTATION_NOT_FOUND)
					.build();
			}

			request.presentation = presentation;
			next();
		} catch (error) {
			next(error);
		}
	};
}

/**
 * @description this middleware must be called after "preGetPresentationAsync"
 */
export async function mapPresentationRole(request: Request, response: Response, next: NextFunction) {
	try {
		const presentation = request.presentation!!;

		const result: { presentationRole: string | null; groupRole: string | null } = {
			presentationRole: null,
			groupRole: null,
		};

		if (request.userinfo.uid === presentation.ownerId) {
			result.presentationRole = "owner";
		}

		const collaboratorService = new PresentationCollaboratorService();
		const learningGroupMemberService = new LearningGroupMemberService();

		// Check collaborator
		const collaborator = await collaboratorService.getRecordByAsync({
			pesentationSeriesId: presentation.seriesId,
			userId: request.userinfo.uid,
		});

		if (collaborator && collaborator.status === PRESENTATION_COLLAB_STATUS.JOINED) {
			result.presentationRole = "collaborator";
		}

		// Check group information
		if (presentation.pace.scope === PRESENTATION_PACE_SCOPES.GROUP && presentation.pace.groupId) {
			// Check group member: owner or co-owner
			const groupMember = await learningGroupMemberService.getRecordByAsync({
				groupId: presentation.pace.groupId,
				userProfileId: request.userinfo.uid,
				role: In([GROUP_MEMBER_ROLE.OWNER, GROUP_MEMBER_ROLE.CO_OWNER]),
			});

			if (groupMember) {
				result.groupRole = groupMember.role;
			}
		}

		request.presentationRoleMap = result;
		next();
	} catch (error) {
		next(error);
	}
}
