import Joi from "joi";
import { REGEX } from "../../shared";

const nameValidation = (identifierName: string, maxLength: 60 | 120) => {
	return Joi.string()
		.trim()
		.max(maxLength)
		.regex(REGEX.NAME)
		.messages({
			"string.base": `${identifierName} phải là chuỗi`,
			"string.pattern.base": `${identifierName} không được chứa ký tự số và dấu gạch dưới`,
			"string.empty": `${identifierName} không được bỏ trống`,
		});
};

export const updateProfileBodySchema = Joi.object({
	firstName: nameValidation("Tên", 60),
	lastName: nameValidation("Họ", 60),
	displayName: nameValidation("Tên hiển thị", 120),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});
