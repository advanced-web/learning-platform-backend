import Joi from "joi";
import { GROUP_MEMBER_ROLE, PAGINATION } from "../../shared";
import { GROUP_PERMISSIONS } from "../../shared/permission.const";

export const createGroupBodySchema = Joi.object({
	name: Joi.string().required().trim().max(120).messages({
		"any.required": "Tên nhóm không được bỏ trống",
		"string.base": "Tên nhóm phải là chuỗi",
		"string.empty": "Tên nhóm không được bỏ trống",
		"string.max": "Tên nhóm không được quá 120 ký tự",
	}),
	description: Joi.string().required().trim().messages({
		"any.required": "Mô tả nhóm không được bỏ trống",
		"string.base": "Mô tả nhóm phải là chuỗi",
		"string.empty": "Mô tả nhóm không được bỏ trống",
	}),
	// groupAvatarUrl: Joi.string().optional().uri({}).messages({
	// 	"string.uri": "Đường dẫn tới hình ảnh không hợp lệ",
	// 	"string.empty": "Đường dẫn tới hình ảnh không hợp lệ",
	// }),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const getListGroupQuerySchema = Joi.object({
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
	searchKey: Joi.string().optional().trim().allow(""),
	groupType: Joi.string()
		.valid(...Object.values(GROUP_MEMBER_ROLE))
		.required()
		.messages({
			"any.required": "Loại nhóm không được bỏ trống",
			"any.only": "Loại nhóm không hợp lệ",
		}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const groupIdPathParamSchema = Joi.object({
	id: Joi.number().integer().greater(0).required().messages({
		"any.required": "Nhóm cần tìm không tồn tại",
		"number.base": "Nhóm cần tìm không tồn tại",
		"number.greater": "Nhóm cần tìm không tồn tại",
	}),
});

export const getGroupDetailQuerySchema = Joi.object({
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const acceptInvitedUserBodySchema = Joi.object({
	code: Joi.string().required().trim().messages({
		"any.required": "Mã không được bỏ trống",
		"string.base": "Mã phải là chuỗi",
		"string.empty": "Mã không được bỏ trống",
	}),
});

export const sendInvitationLinkByEmailBodySchema = Joi.object({
	emails: Joi.array()
		.items(Joi.string().email().message("Email không đúng định dạng"))
		.required()
		.min(1)
		.message("Danh sách Email không được bỏ trống"),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const setGroupMemberRoleParamsSchema = Joi.object({
	id: Joi.number().integer().greater(0).required().messages({
		"any.required": "Nhóm cần tìm không tồn tại",
		"number.base": "Nhóm cần tìm không tồn tại",
		"number.greater": "Nhóm cần tìm không tồn tại",
	}),
	groupMemberId: Joi.number().integer().greater(0).required().messages({
		"any.required": "Người dùng không tồn tại",
		"number.base": "Người dùng không tồn tại",
		"number.greater": "Người dùng không tồn tại",
	}),
});

export const setGroupMemberRoleBodySchema = Joi.object({
	role: Joi.string().valid(GROUP_MEMBER_ROLE.CO_OWNER, GROUP_MEMBER_ROLE.MEMBER).required().messages({
		"any.required": "Vai trò của thành viên nhóm không được bỏ trống",
		"any.only": "Vai trò của thành viên nhóm không hợp lệ",
	}),
	permissions: Joi.array()
		.items(Joi.string().valid(...Object.values(GROUP_PERMISSIONS)))
		.required()
		.messages({
			"any.required": "Danh sách quyền không được bỏ trống",
			"any.only": "Danh sách quyền không hợp lệ",
		}),
}).options({ allowUnknown: true, stripUnknown: true });

export const removeMemberParamsSchema = Joi.object({
	id: Joi.number().integer().greater(0).required().messages({
		"any.required": "Nhóm cần tìm không tồn tại",
		"number.base": "Nhóm cần tìm không tồn tại",
		"number.greater": "Nhóm cần tìm không tồn tại",
	}),
	memberId: Joi.number().integer().greater(0).required().messages({
		"any.required": "Thành viên không tồn tại",
		"number.base": "Thành viên không tồn tại",
		"number.greater": "Thành viên không tồn tại",
	}),
});
