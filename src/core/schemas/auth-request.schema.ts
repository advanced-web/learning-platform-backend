import joi from "joi";
import { REGEX } from "../../shared";

export const loginBodySchema = joi
	.object({
		username: joi.string().email().required().messages({
			"any.required": "Tên đăng nhập không được bỏ trống",
			"string.base": "Tên đăng nhập phải là chuỗi",
			"string.email": "Tên đăng nhập phải là email",
			"string.empty": "Tên đăng nhập không được bỏ trống",
		}),
		password: joi.string().required().pattern(REGEX.PASSWORD).messages({
			"any.required": "Mật khẩu không được bỏ trống",
			"string.base": "Mật khẩu phải là chuỗi",
			"string.empty": "Mật khẩu không được bỏ trống",
			"string.pattern.base": "Mật khẩu không đúng định dạng",
		}),
	})
	.options({ allowUnknown: true, stripUnknown: true });

export const registerBodySchema = joi
	.object({
		username: joi.string().email().required().messages({
			"any.required": "Tên đăng nhập không được bỏ trống",
			"string.base": "Tên đăng nhập phải là chuỗi",
			"string.empty": "Tên đăng nhập không được bỏ trống",
			"string.email": "Tên đăng nhập phải là email",
		}),
		password: joi.string().required().pattern(REGEX.PASSWORD).messages({
			"any.required": "Mật khẩu không được bỏ trống",
			"string.base": "Mật khẩu phải là chuỗi",
			"string.empty": "Mật khẩu không được bỏ trống",
			"string.pattern.base": "Mật khẩu không đúng định dạng",
		}),
		firstName: joi.string().regex(REGEX.NAME).required().messages({
			"any.required": "Tên không được bỏ trống",
			"string.base": "Tên phải là chuỗi",
			"string.pattern.base": "Tên không được chứa ký tự số và dấu gạch dưới",
			"string.empty": "Tên không được bỏ trống",
		}),
		lastName: joi.string().regex(REGEX.NAME).required().messages({
			"any.required": "Họ không được bỏ trống",
			"string.base": "Họ phải là chuỗi",
			"string.pattern.base": "Họ không được chứa ký tự số và dấu gạch dưới",
			"string.empty": "Họ không được bỏ trống",
		}),
	})
	.options({ allowUnknown: true, stripUnknown: true });

export const oauthProviderParamSchema = joi.object({
	provider: joi.valid("google").messages({
		"any.only": "Nhà cung cấp này không được hỗ trợ",
	}),
});

export const activateUserBodySchema = joi.object({
	email: joi.string().email().required().pattern(REGEX.PASSWORD).messages({
		"any.required": "Email không được bỏ trống",
		"string.base": "Email phải là chuỗi",
		"string.empty": "Email không được bỏ trống",
		"string.email": "Email không đúng định dạng",
	}),
});

export const changePasswordBodySchema = joi.object({
	oldPassword: joi.string().required().pattern(REGEX.PASSWORD).messages({
		"any.required": "Mật khẩu hiện tại không được bỏ trống",
		"string.base": "Mật khẩu hiện tại phải là chuỗi",
		"string.empty": "Mật khẩu hiện tại không được bỏ trống",
		"string.pattern.base": "Mật khẩu hiện tại không đúng định dạng",
	}),
	newPassword: joi.string().required().pattern(REGEX.PASSWORD).messages({
		"any.required": "Mật khẩu mới không được bỏ trống",
		"string.base": "Mật khẩu mới phải là chuỗi",
		"string.empty": "Mật khẩu mới không được bỏ trống",
		"string.pattern.base": "Mật khẩu hiện tại không đúng định dạng",
	}),
});

export const forgotPasswordBodySchema = joi.object({
	email: joi.string().email().required().messages({
		"any.required": "Email không được bỏ trống",
		"string.base": "Email phải là chuỗi",
		"string.empty": "Email không được bỏ trống",
		"string.email": "Email không đúng định dạng",
	}),
});

export const resetPasswordBodySchema = joi
	.object({
		password: joi.string().required().pattern(REGEX.PASSWORD).messages({
			"any.required": "Mật khẩu không được bỏ trống",
			"string.base": "Mật khẩu phải là chuỗi",
			"string.empty": "Mật khẩu không được bỏ trống",
			"string.pattern.base": "Mật khẩu không đúng định dạng",
		}),
		token: joi.string().required().messages({
			"any.required": "Mã khôi phục không hợp lệ",
			"string.base": "Mã khôi phục không hợp lệ",
			"string.empty": "Mã khôi phục không hợp lệ",
		}),
	})
	.options({ allowUnknown: true, stripUnknown: true });
