import Joi from "joi";
import { PAGINATION, QUESTION_REATION_TYPE } from "../../shared";

export const postNewQuestionBodySchema = Joi.object({
	question: Joi.string().trim().required().messages({
		"any.required": "Nội dung câu hỏi không được bỏ trống",
		"string.base": "Nội dung câu hỏi phải là chuỗi",
		"string.empty": "Nội dung câu hỏi không được bỏ trống",
	}),
}).options({ allowUnknown: true, stripUnknown: true });

export const getQuestionListQuerySchema = Joi.object({
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
	sortByTotal: Joi.string().valid("asc", "desc").optional().messages({
		"any.only": "Điều kiện sắp xếp không hợp lệ",
	}),
	sortByTime: Joi.string().valid("asc", "desc").optional().messages({
		"any.only": "Điều kiện sắp xếp không hợp lệ",
	}),
	isArchive: Joi.boolean().optional().messages({ "boolean.base": "Trạng thái câu hỏi không hợp lệ" }),
}).options({ allowUnknown: true, stripUnknown: true });

export const reactQuestionBodySchema = Joi.object({
	reaction: Joi.string()
		.valid(...Object.values(QUESTION_REATION_TYPE))
		.required()
		.messages({
			"string.base": "Loại tương tác phải là chuỗi",
			"any.required": "Loại tương tác không được bỏ trống",
			"any.only": "Loại tương tác không hợp lệ",
		}),
	actionType: Joi.string()
		.valid("up", "down")
		.required()
		.messages({ "any.only": "Tương tác không hợp lệ", "any.required": "Tương tác không được bỏ trống" }),
}).options({ allowUnknown: true, stripUnknown: true });

export const updateQuestionBodySchema = Joi.object({
	actionType: Joi.string().valid("archive", "restore").required().messages({
		"any.only": "Hành động cập nhật không hợp lệ",
		"any.required": "Hành động cập nhật không được bỏ trống",
	}),
}).options({ allowUnknown: true, stripUnknown: true });
