import Joi from "joi";
import { SLIDE_TYPE } from "../../shared";

export const createSlideSchema = Joi.object({
	type: Joi.string()
		.required()
		.trim()
		.valid(...Object.values(SLIDE_TYPE))
		.messages({
			"any.required": "Loại slide không được bỏ trống",
			"string.base": "Loại slide phải là chuỗi",
			"string.empty": "Loại slide không được bỏ trống",
			"any.only": "Loại slide không hợp lệ",
		}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const seriesIdAndAdminKeyParamsSchema = Joi.object({
	series_id: Joi.string()
		.guid({ version: ["uuidv4"] })
		.required()
		.messages({
			"any.required": "Bài trình bày không tồn tại",
			"string.base": "Bài trình bày không tồn tại",
			"string.empty": "Bài trình bày không tồn tại",
			"string.guid": "Bài trình bày không tồn ttại",
		}),
	admin_key: Joi.string().required().messages({
		"any.required": "Trang chiếu không tồn tại",
		"string.base": "Trang chiếu không tồn tại",
		"string.empty": "Trang chiếu không tồn tại",
	}),
});

export const voteKeyAndAdminKeyParamsSchema = Joi.object({
	vote_key: Joi.string().required().messages({
		"any.required": "Bài trình bày không tồn tại",
		"string.base": "Bài trình bày không tồn tại",
		"string.empty": "Bài trình bày không tồn tại",
		"string.guid": "Bài trình bày không tồn ttại",
	}),
	admin_key: Joi.string().required().messages({
		"any.required": "Trang chiếu không tồn tại",
		"string.base": "Trang chiếu không tồn tại",
		"string.empty": "Trang chiếu không tồn tại",
	}),
});

export const audienceVotingResultAsync = Joi.object({
	answer: Joi.number().integer().required().messages({
		"any.required": "Chưa có đáp án được chọn",
		"number.base": "Đáp án không hợp lệ",
	}),
});

// export const performSlideActionParamsSchema = Joi.object({
// 	series_id: Joi
// 		.string()
// 		.guid({ version: ['uuidv4'] })
// 		.required()
// 		.messages({
// 			"any.required": "Bài trình bày không tồn tại",
// 			"string.base": "Bài trình bày không tồn tại",
// 			"string.empty": "Bài trình bày không tồn tại",
// 			"string.guid": "Bài trình bày không tồn ttại",
// 		}),
// 	admin_key: Joi
// 		.string()
// 		.required()
// 		.messages({
// 			"any.required": "Trang trình chiếu không tồn tại",
// 			"string.base": "Trang trình chiếu không tồn tại",
// 			"string.empty": "Trang trình chiếu không tồn tại",
// 		}),
// 	action: Joi
// 		.string()
// 		.valid(...Object.values(PRESENTATION_PACE_ACTIONS))
// 		.required()
// 		.messages({
// 			"any.required": "Hành động không hợp lệ",
// 			"any.only": "Hành động không hợp lệ",
// 			"string.base": "Hành động không hợp lệ",
// 			"string.empty": "Hành động không hợp lệ",
// 		}),
// });

export const updateSlideSchema = Joi.object({
	question: Joi.string().allow("").required().messages({
		"any.required": "Tên câu hỏi/tiêu đề không được bỏ trống",
		"string.base": "Tên câu hỏi/tiêu đề phải là chuỗi",
	}),
	type: Joi.string()
		.valid(...Object.values(SLIDE_TYPE))
		.required()
		.messages({
			"any.required": "Loại trang trình chiếu không được bỏ trống",
			"any.only": "Loại trang trình chiếu không hợp lệ",
			"string.base": "Loại trang trình chiếu không hợp lệ",
			"string.empty": "Loại trang trình chiếu không hợp lệ",
		}),
	choices: Joi.array().required().messages({
		"any.required": "Danh sách đáp án không được bỏ trống",
		"array.base": "Danh sách đáp án không hợp lệ",
	}),
	position: Joi.number().integer().greater(-1).required().messages({
		"any.required": "Thứ tự trang trình chiếu không tồn tại",
		"number.base": "Thứ tự trang trình chiếu không hợp lệ",
		"number.greater": "Thứ tự trang trình chiếu không hợp lệ",
	}),
	questionDescription: Joi.string().allow("").required().messages({
		"any.required": "Mô tả không được bỏ trống",
		"string.base": "Mô tả phải là chuỗi",
	}),
	questionImageUrl: Joi.string().allow(null).default(null).messages({
		"string.base": "Đường dẫn hình ảnh phải là chuỗi",
		"string.empty": "Đường dẫn hình ảnh không hợp lệ",
	}),
	questionVideoUrl: Joi.string().allow(null).default(null).messages({
		"string.base": "Đường dẫn video phải là chuỗi",
		"string.empty": "Đường dẫn video không hợp lệ",
	}),
	speakerNotes: Joi.string().allow("", null).default(null).messages({
		"string.base": "Ghi chú phải là chuỗi",
	}),
	config: Joi.object().allow(null).default(null).messages({
		"object.base": "Cấu hình riêng cho loại trang trình chiếu không hợp lệ",
	}),
	active: Joi.boolean().required().messages({
		"any.required": "Trạng thái bình chọn là bắt buộc",
		"boolean.base": "Trạng thái bình chọn không hợp lệ",
	}),
	hideInstructionBar: Joi.boolean().required().messages({
		"any.required": "Trạng thái hướng dẫn vào trang bình chọn là bắt buộc",
		"boolean.base": "Trạng thái hướng dẫn vào trang bình chọn không hợp lệ",
	}),
	textSize: Joi.number().integer().greater(0).required().messages({
		"any.required": "Kích cỡ chữ của trang trình chiếu là bắt buộc",
		"number.base": "Kích cỡ chữ của trang trình chiếu phải là giá trị số",
		"number.greater": "Kích cỡ chữ của trang trình chiếu không hợp lệ",
	}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

// export const performSlideActionBodySchema = Joi.object({
// 	groupId: Joi
// 		.number()
// 		.allow(null)
// 		.integer()
// 		.greater(0)
// 		.required()
// 		.messages({
// 			"any.required": "Nhóm không hợp lệ",
// 			"number.base": "Nhóm không hợp lệ",
// 			"number.greater": "Nhóm không hợp lệ",
// 		}),
// 	scope: Joi
// 		.string()
// 		.valid(...Object.values(PRESENTATION_PACE_SCOPES))
// 		.required()
// 		.messages({
// 			"any.required": "Nơi trình chiếu không được bỏ trống",
// 			"any.only": "Nơi trình chiếu không hợp lệ",
// 			"string.base": "Nơi trình chiếu không hợp lệ",
// 			"string.empty": "Nơi trình chiếu không hợp lệ",
// 		}),
// }).options({
// 	allowUnknown: true,
// 	stripUnknown: true,
// });
