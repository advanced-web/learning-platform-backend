import Joi from "joi";
import { PAGINATION, PRESENTATION_PACE_ACTIONS, PRESENTATION_TYPE } from "../../shared";

export const getPresentationListSchema = Joi.object({
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
	type: Joi.string()
		.valid(...Object.values(PRESENTATION_TYPE))
		.required()
		.messages({
			"any.required": "Loại bài trình chiếu không hợp lệ",
			"any.only": "Loại bài trình chiếu không hợp lệ",
		}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const createPresentationSchema = Joi.object({
	name: Joi.string().required().trim().max(100).messages({
		"any.required": "Tên bài trình bày không được bỏ trống",
		"string.base": "Tên bài trình bày phải là chuỗi",
		"string.empty": "Tên bài trình bày không được bỏ trống",
		"string.max": "Tên bài trình bày không được quá 100 ký tự",
	}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const editPresentationSchema = Joi.object({
	name: Joi.string().required().trim().max(100).messages({
		"any.required": "Tên bài trình bày không được bỏ trống",
		"string.base": "Tên bài trình bày phải là chuỗi",
		"string.empty": "Tên bài trình bày không được bỏ trống",
		"string.max": "Tên bài trình bày không được quá 100 ký tự",
	}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const presentationIdPathParamSchema = Joi.object({
	id: Joi.number().integer().greater(0).required().messages({
		"any.required": "Bài trình bày không tồn tại",
		"number.base": "Bài trình bày không tồn tại",
		"number.greater": "Bài trình bày không tồn tại",
	}),
});

export const presentationSeriesIdPathParamSchema = Joi.object({
	series_id: Joi.string()
		.guid({ version: ["uuidv4"] })
		.required()
		.messages({
			"any.required": "Bài trình bày không tồn tại",
			"string.base": "Bài trình bày không tồn tại",
			"string.empty": "Bài trình bày không tồn tại",
			"string.guid": "Bài trình bày không tồn tại",
		}),
});

export const voteKeyPathParamSchema = Joi.object({
	vote_key: Joi.string().required().messages({
		"any.required": "Bài trình bày không tồn tại",
		"string.base": "Bài trình bày không tồn tại",
		"string.empty": "Bài trình bày không tồn tại",
		"string.guid": "Bài trình bày không tồn tại",
	}),
});

export const performSlideActionBodySchema = Joi.object({
	admin_key: Joi.string().messages({
		"string.base": "Trang trình chiếu không tồn tại",
		"string.empty": "Trang trình chiếu không tồn tại",
	}),
	action: Joi.string()
		.valid(...Object.values(PRESENTATION_PACE_ACTIONS))
		.required()
		.messages({
			"any.required": "Hành động không hợp lệ",
			"any.only": "Hành động không hợp lệ",
			"string.base": "Hành động không hợp lệ",
			"string.empty": "Hành động không hợp lệ",
		}),
}).options({ allowUnknown: true });
