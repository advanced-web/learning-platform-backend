import Joi from 'joi';
import { PAGINATION } from '../../shared';

export const presentationSeriesIdStatisticSchema = Joi.object({
	series_id: Joi
		.string()
		.guid({ version: ['uuidv4'] })
		.required()
		.messages({
			"any.required": "Bài trình bày không tồn tại",
			"string.base": "Bài trình bày không tồn tại",
			"string.empty": "Bài trình bày không tồn tại",
			"string.guid": "Bài trình bày không tồn tại",
		}),
});

export const getPresentationSummaryQuerySchema = Joi.object({
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const presentationSeriesIdAndAdminKeyStatisticSchema = Joi.object({
	series_id: Joi
		.string()
		.guid({ version: ['uuidv4'] })
		.required()
		.messages({
			"any.required": "Bài trình bày không tồn tại",
			"string.base": "Bài trình bày không tồn tại",
			"string.empty": "Bài trình bày không tồn tại",
			"string.guid": "Bài trình bày không tồn tại",
		}),
	admin_key: Joi
		.string()
		.required()
		.messages({
			"any.required": "Trang chiếu không tồn tại",
			"string.base": "Trang chiếu không tồn tại",
			"string.empty": "Trang chiếu không tồn tại",
		}),
});
