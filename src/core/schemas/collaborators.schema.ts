import Joi from "joi";
import { PAGINATION } from "../../shared";

const MAX_EMAIL = 10;

export const invitePresentationCollaboratorsSchema = Joi.object({
	presentationId: Joi.string().required().uuid().messages({
		"any.required": "Bài trình bày không tồn tại",
		"string.base": "Bài trình bày không tồn tại",
		"string.empty": "Bài trình bày không tồn tại",
		"string.guid": "Bài trình bày không tồn tại",
	}),
	emails: Joi.array()
		.items(Joi.string().email().message("Email không đúng định dạng"))
		.required()
		.min(1)
		.message("Danh sách Email không được bỏ trống")
		.max(MAX_EMAIL)
		.message(`Số lượng email gửi tối đa một lần là ${MAX_EMAIL}`),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const acceptInvitationSchema = Joi.object({
	presentationId: Joi.string().required().uuid().messages({
		"any.required": "Bài trình bày không tồn tại",
		"string.base": "Bài trình bày không tồn tại",
		"string.empty": "Bài trình bày không tồn tại",
		"string.guid": "Bài trình bày không tồn tại",
	}),
	invitationToken: Joi.string().required().trim().messages({
		"any.required": "Mã mời không hợp lệ",
		"string.base": "Mã mời không hợp lệ",
		"string.empty": "Mã mời không hợp lệ",
	}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});

export const getCollaboratorListSchema = Joi.object({
	presentationId: Joi.string().required().uuid().messages({
		"any.required": "Bài trình bày không tồn tại",
		"string.base": "Bài trình bày không tồn tại",
		"string.empty": "Bài trình bày không tồn tại",
		"string.guid": "Bài trình bày không tồn tại",
	}),
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
}).options({
	allowUnknown: true,
	stripUnknown: true,
});
