import Joi from "joi";
import { PAGINATION } from "../../shared";

export const createChatMessageSchema = Joi.object({
	message: Joi.string().trim().required().messages({
		"any.required": "Nội dung tin nhắn không được bỏ trống",
		"string.base": "Nội dung tin nhắn phải là chuỗi",
		"string.empty": "Nội dung tin nhắn không được bỏ trống",
	}),
}).options({
	allowUnknown: true,
	stripUnknown: true,
	convert: true,
});

export const getChatHistorySchema = Joi.object({
	presentNo: Joi.number().positive().required().messages({
		"number.base": "Thứ tự bài trình chiếu phải là số",
		"number.required": "Thứ tự bài trình chiếu không được bỏ trống",
		"number.positive": "Thứ tự bài trình chiếu không hợp lệ",
	}),
	page: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1) {
			return PAGINATION.DEFAULT_PAGE;
		}

		return numericValue;
	}, "Parse page query").default(PAGINATION.DEFAULT_PAGE),
	limit: Joi.custom((value, helper) => {
		const numericValue = parseInt(value);
		if (isNaN(numericValue) || numericValue < 1 || numericValue > PAGINATION.MAX_PAGE_SIZE) {
			return PAGINATION.DEFAULT_PAGE_SIZE;
		}

		return numericValue;
	}, "Parse limit query").default(PAGINATION.DEFAULT_PAGE_SIZE),
}).options({ allowUnknown: true, stripUnknown: true });
