import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "learning_group" })
export class LearningGroup extends GeneratedIdentifierWithTimeTracking {
	@Column({ type: "character varying", length: 120 })
	name: string;

	@Column({ type: "text", nullable: true })
	description: string;

	@Column({ name: "invitation_link", type: "character varying", length: 255, nullable: true })
	invitationLink: string | null;

	@Column({ name: "invitation_token", type: "text", nullable: true })
	invitationToken: string | null;

	@Column({ name: "invitation_token_key", type: "text", nullable: true })
	invitationTokenKey: string | null;

	@Column({ name: "group_avatar", type: "text", nullable: true })
	groupAvatar: string | null;

	@Column({ name: "owner_profile_id", type: "integer" })
	ownerProfileId: number;

	@Column({ name: "owner_display_name", type: "character varying", length: 120 })
	ownerDisplayName: string;

	@Column({ name: "presentation_series_id", type: "uuid", nullable: true })
	presentationSeriesId: string | null;

	@Column({ name: "vote_key", type: "text", nullable: true })
	voteKey: string | null;
}
