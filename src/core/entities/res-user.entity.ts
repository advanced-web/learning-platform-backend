import { Entity, Column } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "res_user" })
export class ResUser extends GeneratedIdentifierWithTimeTracking {
	@Column({ type: "character varying", length: 190, unique: true })
	username: string;

	@Column({ type: "character varying", nullable: true, length: 128 })
	password: string | null;

	@Column({ name: "is_active", default: false })
	isActive: boolean;

	@Column({ name: "oauth_provider", type: "character varying", nullable: true, length: 20 })
	oauthProvider: string | null;

	@Column({ name: "oauth_uid", type: "character varying", nullable: true, length: 128 })
	oauthUid: string | null;

	@Column({ name: "activation_date", type: "timestamp with time zone", nullable: true })
	activationDate: Date;

	@Column({ name: "activation_token", type: "text", nullable: true })
	activationToken: string | null;

	@Column({ name: "activation_token_key", type: "text", nullable: true })
	activationTokenKey: string | null;
}
