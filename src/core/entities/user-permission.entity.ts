import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "user_permissions" })
export class UserPermission extends GeneratedIdentifierWithTimeTracking {
	@Column({ name: "subject_id", type: "integer" })
	subjectId: number;

	@Column({ name: "resource_id", type: "integer" })
	resourceId: number;

	@Column({ name: "resource_type", type: "character varying", length: 100 })
	resourceType: string;

	@Column({ name: "permissions", type: "text" })
	permissions: string;
}
