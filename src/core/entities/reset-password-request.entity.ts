import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "reset_password_requests" })
export class ResetPasswordRequest extends GeneratedIdentifierWithTimeTracking {
	@Column({ type: "character varying", length: 190, unique: true })
	email: string;

	@Column({ name: "reset_token", type: "text" })
	resetToken: string;

	@Column({ name: "expire_at", type: "timestamp with time zone" })
	expireAt: Date;
}
