import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { QUESTION_REATION_TYPE } from "../../shared/entities.const";

export type ReactionType = typeof QUESTION_REATION_TYPE[keyof typeof QUESTION_REATION_TYPE];

@Entity({ name: "question_reactions" })
export class QuestionReaction extends BaseEntity {
	@PrimaryColumn({ name: "question_id", type: "integer" })
	questionId: number;

	@PrimaryColumn({ name: "user_id", type: "integer" })
	userId: number;

	@Column({ name: "reaction_type", type: "character varying", length: 50 })
	reactionType: string;

	@Column({ name: "reacted_at", type: "timestamp with time zone", default: "now()" })
	reactedAt: Date;
}
