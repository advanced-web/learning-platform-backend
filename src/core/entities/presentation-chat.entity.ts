import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "presentation_chats" })
export class PresentationChat extends GeneratedIdentifierWithTimeTracking {
	@Column({ name: "presentation_series_id", type: "uuid" })
	presentationSeriesId: string;

	@Column({ name: "present_no", type: "integer" })
	presentNo: number;

	@Column({ name: "user_id", type: "integer" })
	userId: number;

	@Column({ name: "user_display_name", type: "character varying", length: 120 })
	userDisplayName: string;

	@Column({ name: "user_avatar_link", type: "text", nullable: true })
	userAvatarLink: string | null;

	@Column({ name: "message", type: "text" })
	message: string;
}
