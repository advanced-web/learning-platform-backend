import { Column, Entity } from "typeorm";
import { GeneratedIdentifier } from "./base.entity";

export type FullySlideVotingScore = {
	userId: string;
	userDisplayName: string;
	votedAt: Date;
}

export type SlideVotingResult<Score> = {
	id: number;
	label: string;
	score: Score[];
};

@Entity({ name: "voting_results" })
export class VotingResults extends GeneratedIdentifier {
	@Column({ name: "slide_id", type: "integer" })
	slideId: number;

	@Column({ name: "slide_admin_key", type: "text" })
	slideAdminKey: string;

	@Column({ name: "user_id", type: "integer" })
	userId: number;

	@Column({ name: "user_display_name", type: "character varying", length: 120 })
	userDisplayName: string;

	@Column({ name: "answer_id", type: "integer" })
	answerId: number;

	@Column({ name: "voted_at", type: "timestamp with time zone", default: () => "now()" })
	votedAt: Date;
}

// export type SlideVotingResult = {
// 	id: number;
// 	label: string;
// 	score: number[];
// };

// @Entity({ name: "voting_results" })
// export class VotingResults extends GeneratedIdentifierWithTimeTracking {
// 	@Column({ name: "slide_id", type: "integer" })
// 	slideId: number;

// 	@Column({ name: "slide_admin_key", type: "text" })
// 	slideAdminKey: string;

// 	@Column({ name: "respondents", type: "integer", default: 0 })
// 	respondents: number;

// 	@Column({ name: "results", type: "json", array: true })
// 	results: SlideVotingResult[];
// }
