import { BaseEntity, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export abstract class GeneratedIdentifier extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;
}

export abstract class TimeTracking extends BaseEntity {
	@CreateDateColumn({ name: "created_at", type: "time with time zone" })
	createdAt: Date;

	@UpdateDateColumn({ name: "updated_at", type: "time with time zone" })
	updatedAt: Date;
}

export abstract class GeneratedIdentifierWithTimeTracking extends TimeTracking {
	@PrimaryGeneratedColumn()
	id: number;
}
