import { Column, Entity } from "typeorm";
import { GeneratedIdentifier } from "./base.entity";

export interface ActivityRecord {
	key: string;
	value: string;
	timestamp: string;
}

@Entity({ name: "learning_group_member" })
export class LearningGroupMember extends GeneratedIdentifier {
	@Column({ name: "group_id", type: "integer" })
	groupId: number;

	@Column({ name: "user_profile_id", type: "integer" })
	userProfileId: number;

	@Column({ name: "user_display_name", type: "character varying", length: 120 })
	userDisplayName: string;

	@Column({ name: "role_name", type: "character varying", length: 50 })
	roleName: string;

	@Column({ name: "role", type: "character varying", length: 20 })
	role: string;

	@Column({ name: "current_status", type: "json" })
	currentStatus: ActivityRecord;

	@Column({ type: "json" })
	activity: ActivityRecord[];
}
