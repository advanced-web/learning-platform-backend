import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

export type PresentationPaceScope = "public" | "group";
export type PresentationPace = {
	mode: string;
	active: string;
	state: string;
	counter: number;
	scope: PresentationPaceScope;
	groupId: number | null;
};

@Entity({ name: "presentations" })
export class Presentations extends GeneratedIdentifierWithTimeTracking {
	@Column({ name: "name", type: "character varying", length: 100 })
	name: string;

	@Column({ name: "series_id", type: "uuid", default: () => "gen_random_uuid()" })
	seriesId: string;

	@Column({ name: "vote_key", type: "text" })
	voteKey: string;

	@Column({ name: "owner_id", type: "integer" })
	ownerId: number;

	@Column({ name: "owner_display_name", type: "character varying", length: 120 })
	ownerDisplayName: string;

	@Column({ name: "pace", type: "text" })
	pace: PresentationPace;

	@Column({ name: "closed_for_voting", type: "boolean", default: false })
	closedForVoting: boolean;

	@Column({ name: "slide_count", type: "integer", default: 0 })
	slideCount: number;
}
