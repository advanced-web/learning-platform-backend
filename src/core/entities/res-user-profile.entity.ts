import { Entity, Column } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "res_user_profile" })
export class ResUserProfile extends GeneratedIdentifierWithTimeTracking {
	@Column({ name: "first_name", type: "character varying", length: 60 })
	firstName: string;

	@Column({ name: "last_name", type: "character varying", length: 60 })
	lastName: string;

	@Column({ name: "display_name", type: "character varying", length: 120 })
	displayName: string;

	@Column({ type: "character varying", length: 190 })
	email: string;

	@Column({ type: "text", nullable: true })
	avatar: string | null;

	@Column({ name: "user_id", type: "integer" })
	userId: number;
}
