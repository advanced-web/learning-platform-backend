import { Column, Entity } from "typeorm";
import { GeneratedIdentifier } from "./base.entity";

@Entity({ name: "presentation_collaborators" })
export class PresentationCollaborator extends GeneratedIdentifier {
	@Column({ name: "pesentation_series_id", type: "uuid" })
	pesentationSeriesId: string;

	@Column({ name: "user_id", type: "integer", nullable: true })
	userId: number;

	@Column({ type: "character varying", length: 190 })
	email: string;

	@Column({ type: "character varying", length: 100 })
	status: string;

	@Column({ name: "invitation_token", type: "text" })
	invitationToken: string;

	@Column({ name: "invitation_token_key", type: "text" })
	invitationTokenKey: string;

	@Column({ name: "invited_at", type: "time with time zone" })
	invitedAt: Date;

	@Column({ name: "joined_at", type: "time with time zone", nullable: true })
	joinedAt: Date;
}
