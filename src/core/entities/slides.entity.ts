import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

export interface IBaseSlideChoice {
	id: number;
	label: string;
	position: number;
};

export interface IMultipleChoiceSlideChoice extends IBaseSlideChoice {
	correctAnswer: boolean;
}

export type SlideConfig = {};

@Entity({ name: "slides" })
export class Slides<Choice extends IBaseSlideChoice = IBaseSlideChoice> extends GeneratedIdentifierWithTimeTracking {
	@Column({ name: "admin_key", type: "text" })
	adminKey: string;

	@Column({ name: "presentation_id", type: "integer" })
	presentationId: number;

	@Column({ name: "presentation_series_id", type: "text" })
	presentationSeriesId: string;

	@Column({ name: "question", type: "text", default: "" })
	question: string;

	@Column({ name: "question_description", type: "text", default: "" })
	questionDescription: string;

	@Column({ name: "question_image_url", type: "text", nullable: true })
	questionImageUrl: string | null;

	@Column({ name: "question_video_url", type: "text", nullable: true })
	questionVideoUrl: string | null;

	@Column({ name: "type", type: "character varying", length: 40 })
	type: string;

	@Column({ name: "active", type: "boolean", default: true })
	active: boolean;

	@Column({ name: "hide_instruction_bar", type: "boolean", default: false })
	hideInstructionBar: boolean;

	@Column({ name: "speaker_notes", type: "text", nullable: true })
	speakerNotes: string | null;

	@Column({ name: "choices", type: "json", array: true })
	choices: Choice[];

	@Column({ name: "config", type: "json", nullable: true })
	config: SlideConfig | null;

	@Column({ name: "position", type: "integer" })
	position: number;

	@Column({ name: "text_size", type: "integer", default: 32 })
	textSize: number;
}
