import { Column, Entity } from "typeorm";
import { GeneratedIdentifierWithTimeTracking } from "./base.entity";

@Entity({ name: "presentation_questions" })
export class PresentationQuestion extends GeneratedIdentifierWithTimeTracking {
	@Column({ name: "presentation_series_id", type: "uuid" })
	presentationSeriesId: string;

	@Column({ name: "question", type: "text" })
	question: string;

	@Column({ name: "total_like", type: "integer" })
	totalLike: number;

	@Column({ name: "user_id", type: "integer" })
	userId: number;

	@Column({ name: "user_display_name", type: "character varying", length: 120 })
	userDisplayName: string;

	@Column({ name: "user_avatar_link", type: "text", nullable: true })
	userAvatarLink: string | null;

	@Column({ name: "is_archive", type: "boolean", nullable: true, default: false })
	isArchive: boolean | null;

	@Column({ name: "archived_at", type: "timestamp without time zone", nullable: true })
	archivedAt: Date | null;

	// @Column({ name: "slide_admin_key", type: "text" })
	// slideAdminKey: string;
}
