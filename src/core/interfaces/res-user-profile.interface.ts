export type UpdateProfileBody = {
	firstName?: string;
	lastName?: string;
	displayName?: string;
};
