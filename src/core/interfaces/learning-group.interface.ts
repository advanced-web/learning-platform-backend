export type InvitationTokenPayload = {
	groupId: number;
	role: string;
	permissions: string;
};
