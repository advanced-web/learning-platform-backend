import { IBaseSlideChoice, PresentationPaceScope, SlideConfig, SlideVotingResult } from "../entities";

export type UpdateSlideBody = {
	question: string;
	questionDescription: string;
	questionImageUrl: string | null;
	questionVideoUrl: string | null;
	type: string;
	active: boolean;
	hideInstructionBar: boolean;
	speakerNotes: string | null;
	choices: IBaseSlideChoice[];
	config: SlideConfig | null;
	position: number;
	textSize: number;
};

export type PerformSlideActionBody = {
	scope: PresentationPaceScope;
	groupId: number | null;
};

export type SimpleVotingResultsResponse = {
	slideId: number;
	slideAdminKey: string;
	respondents: number;
	results: SlideVotingResult<number>[];
};
