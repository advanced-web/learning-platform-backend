import { BaseOAuthSocialLogin } from "../../services/oauth/strategies/base-oauth.strategy";
import { ResUser, ResUserProfile } from "../entities";

// Configurations
export type OAuthProvider = "zalo" | "google" | "facebook" | "apple";
export type OAuthConfig = {
	authenticationEndpoint: string;
	validationEndpoint: string;
	dataEndpoint: string;
	clientId: string;
	clientSecret: string;
	responseType: string;
	scope: string;
	prompt: string;
	grantType: string;
	accessType: string;
	accessTokenContentType: string;
	redirectUri: string;
};

// Login URL query parameters
export type OAuthStateParams = {
	provider: string;
};

// Callback query parameters
export type OAuthBaseCallbackResponse = {
	code: string;
	state: string;
};
export type OAuthBaseCallbackError = {};
export type OAuthBaseCallbackQuery = OAuthBaseCallbackResponse | OAuthBaseCallbackError;

export type OAuthGoogleCallbackResponse = OAuthBaseCallbackResponse & {
	scope: string;
	authuser: string;
	prompt: string;
};
export type OAuthGoogleCallbackError = OAuthBaseCallbackError;

// Access token
export type OAuthBaseAccessTokenResponse = {
	access_token: string;
};
export type OAuthGoogleAccessTokenResponse = OAuthBaseAccessTokenResponse & {
	id_token: string;
	expires_in: number;
	token_type: string;
	scope: string;
	refresh_token: string;
};

// Raw user profile (from provider)
export type OAuthGoogleProfile = {
	id: string;
	email: string;
	verified_email: boolean;
	name: string;
	given_name: string;
	family_name: string;
	picture: string;
	locale: string;
};
export type ParsedOAuthUserProfile = {
	oauthUid: string;
	email: string;
	firstName: string;
	lastName: string;
	picture: string;
};
export type OAuthUserCreationData = {
	user: ResUser;
	userProfile: ResUserProfile;
};

// From id_token (decoded)
// export type OAuthGoogleProfile = {
// 	sub: string; // oauth uid
// 	email: string;
// 	email_verified: boolean;
// 	name: string;
// 	given_name: string;
// 	family_name: string;
// 	picture: string;
// 	locale: string;
// };

// OAuth service types
export type BaseOauthStrategyType = BaseOAuthSocialLogin<
	object,
	OAuthBaseCallbackResponse,
	object,
	OAuthBaseAccessTokenResponse
>;

export type OAuthStrategyMap = {
	[key: string]: BaseOauthStrategyType;
};
