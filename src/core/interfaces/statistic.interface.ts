export type SlideSummaryResponseData = {
	userId: number;
	userDisplayName: string;
	answerId: number;
	answer: string;
	votedAt: Date;
};
