export interface IPagination {
	totalRecords: number;
	page: number;
	limit: number;
}

export interface IPaginationData {
	items: any[];
	pagination: IPagination;
}

export interface IResponseMetadata {
	createdDate: string;
}

export interface IBaseResponseObject {
	code: number;
	message: string;
	data?: any;
	errors?: any;
	metadata: IResponseMetadata & Record<string, any>;
}
