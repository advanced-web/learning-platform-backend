export * from "./core.interface";
export * from "./oauth.interface";
export * from "./auth-v2.interface";
export * from "./res-user-profile.interface";
export * from "./learning-group.interface";
export * from "./slides.interface";
export * from "./statistic.interface";
