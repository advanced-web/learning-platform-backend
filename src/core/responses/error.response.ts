import statuses from "statuses";
import { HttpError } from "../../common";
import { API_MESSAGES, HTTP_CODE, UNKNOWN_ERROR_RESPONSE_CODE } from "../../shared";
import { IBaseResponseObject } from "../interfaces";
import { BaseResponse } from "./base.response";
import _ from "lodash";
import { RESPONSE_CODE } from "../../shared";

export class ErrorResponse extends BaseResponse {
	constructor(code: number, message: string, errors?: any) {
		super(code, message, errors);
	}

	toJSON(): IBaseResponseObject {
		return {
			code: this._code,
			message: this._message,
			errors: this._data,
			metadata: {
				createdDate: this._createdDate,
			},
		};
	}
}

export class HttpErrorBuilder {
	private statusCode: number;
	private _message: string;
	private _responseCode: number;
	private _errors?: any;

	private _isAssignedCustomMessage = false;

	private constructor() {
		this.statusCode = HTTP_CODE.INTERNAL_SERVER_ERROR;
		this._message = statuses.message[this.statusCode] ?? API_MESSAGES.INTERNAL_ERROR;
		this._responseCode = HTTP_CODE.INTERNAL_SERVER_ERROR;
		this._errors = undefined;
	}

	static init() {
		return new HttpErrorBuilder();
	}

	withStatusCode(code: number) {
		this.statusCode = code;
		this._responseCode = code === HTTP_CODE.INTERNAL_SERVER_ERROR ? HTTP_CODE.INTERNAL_SERVER_ERROR : UNKNOWN_ERROR_RESPONSE_CODE;

		if (!this._isAssignedCustomMessage) {
			this._message = statuses.message[this.statusCode] ?? API_MESSAGES.INTERNAL_ERROR;
		}

		return this;
	}

	withMessage(message: string) {
		this._isAssignedCustomMessage = true;
		this._message = message;
		return this;
	}

	withResponseCode(code: number) {
		this._responseCode = code;
		if (!this._isAssignedCustomMessage) {
			this._message =
				_.findKey(RESPONSE_CODE, _.partial(_.isEqual, this._responseCode)) || API_MESSAGES.INTERNAL_ERROR;
		}
		return this;
	}

	withErrors(errors: any) {
		this._errors = errors;
		return this;
	}

	build(): HttpError {
		return new HttpError(this.statusCode, this._responseCode, this._message, this._errors);
	}

	throw() {
		throw this.build();
	}
}
