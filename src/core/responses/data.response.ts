import statuses from "statuses";
import { HTTP_CODE } from "../../shared";
import { IBaseResponseObject } from "../interfaces";
import { BaseResponse } from "./base.response";

export class DataResponse extends BaseResponse {
	constructor(code: number, message: string, data?: any) {
		super(code, message, data);
	}

	toJSON(): IBaseResponseObject {
		return {
			code: this._code,
			message: this._message,
			data: this._data,
			metadata: {
				createdDate: this._createdDate,
			},
		};
	}
}

export class DataResponseBuilder {
	private statusCode: number;
	private _message: string;
	private _responseCode: number;
	private _data?: any;

	private _isAssignedCustomMessage = false;

	private constructor() {
		this.statusCode = HTTP_CODE.OK;
		this._message = statuses.message[this.statusCode] ?? "OK";
		this._responseCode = HTTP_CODE.OK;
		this._data = undefined;
	}

	static init() {
		return new DataResponseBuilder();
	}

	withStatusCode(code: number) {
		this.statusCode = code;
		this._responseCode = code;

		if (!this._isAssignedCustomMessage) {
			this._message = statuses.message[this.statusCode] ?? "OK";
		}

		return this;
	}

	withMessage(message: string) {
		this._message = message;
		return this;
	}

	withResponseCode(code: number) {
		this._responseCode = code;
		return this;
	}

	withData(data: any) {
		this._data = data;
		return this;
	}

	build(): IBaseResponseObject {
		return {
			code: this._responseCode,
			message: this._message,
			data: this._data,
			metadata: {
				createdDate: new Date().toISOString(),
			},
		};
	}
}
