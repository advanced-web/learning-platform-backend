import { EventEmitter } from "events";

export interface IBaseEvent<T extends string | symbol> {
	subscribe(eventType: T, listener: (...args: any[]) => void): void;
	unSubscribe(eventType: T, listener: (...args: any[]) => void): void;
	notify(eventType: T, data: any): void;
}

export class BaseEvent<T extends string | symbol> implements IBaseEvent<T> {
	private _eventEmitter: NodeJS.EventEmitter;

	constructor() {
		this._eventEmitter = new EventEmitter();
	}

	static instance = new BaseEvent();

	public subscribe(eventType: T, listener: (...args: any[]) => void): void {
		this._eventEmitter.on(eventType, listener);
	}

	public unSubscribe(eventType: T, listener: (...args: any[]) => void): void {
		this._eventEmitter.removeListener(eventType, listener);
	}

	public notify(eventType: T, data: any): void {
		this._eventEmitter.emit(eventType, data);
	}
}
