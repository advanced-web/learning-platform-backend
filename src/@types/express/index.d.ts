import { Express } from "express-serve-static-core";
import { JsonObject } from "swagger-ui-express";
import { Presentations, ResUser } from "../../core/entities";

declare global {
	namespace Express {
		interface Request {
			// custom request object here
			userinfo: {
				uid: number;
				username: string;
				email: string;
				email_verified: boolean;
				avatar: string;
				display_name: string;
			} & Record<string, any>;
			fetchedUser: ResUser | null;
			presentation: Presentations | null;
			presentationRoleMap: { presentationRole: string | null; groupRole: string | null } | null;
		}
	}
}
