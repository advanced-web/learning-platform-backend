import { In } from "typeorm";
import { Presentations } from "../core";
import { PresentationsRepository, SlidesRepository, VotingResultsRepository } from "../repositories";
import { BaseService } from "./base.service";

export class PresentationsService extends BaseService<Presentations> {
	constructor() {
		super();
		this._repository = PresentationsRepository;
	}

	getRawPresentationByVoteKeyAsync(voteKey: string) {
		return this._repository
			.createQueryBuilder("p")
			.select(["name", "vote_key", "pace", "closed_for_voting", "slide_count", "series_id"])
			.where("p.vote_key = :voteKey", { voteKey })
			.getRawOne();
	}

	getPresentationListAndCountByOwnerIdAsync(ownerId: number, limit: number, page: number) {
		return this._repository
			.createQueryBuilder("p")
			.where("p.owner_id = :ownerId", { ownerId })
			.skip(limit * (page - 1))
			.take(limit)
			.getManyAndCount();
	}

	getPresentationListAndCountBySeriesIdAsync(ids: string[], limit: number, page: number) {
		return (
			this._repository
				.createQueryBuilder()
				.where(`series_id in (${ids.join(",")})`)
				.skip(limit * (page - 1))
				.take(limit)
				.getManyAndCount()
		);
	}

	async deleteByIdAndOwnerIdAsync(id: number, ownerId: number) {
		const slides = await SlidesRepository.createQueryBuilder("s")
			.select("id", "id")
			.where("s.presentation_id = :id", { id })
			.getRawMany<{ id: number }>();

		const deleteResult = await this._repository.delete({ id, ownerId });

		// Removes slides which belongs to this presentation
		// Removes voting results which are corresponding to the above slides.
		const slideIds = slides.map((item) => item.id);
		await SlidesRepository.delete({ id: In(slideIds) });
		await VotingResultsRepository.delete({ slideId: In(slideIds) });

		return deleteResult;
	}

	async hasSlideBySeriesIdAndAdminKey(seriesId: string, adminKey: string) {
		const slide = await SlidesRepository.findOneBy({
			adminKey,
			presentationSeriesId: seriesId,
		});

		return slide !== null;
	}

	async checkPaceStateByIdentifier(
		state: string,
		identifier: {
			key: "id" | "series_id" | "vote_key";
			value: string | number;
		}
	) {
		const presentation = await this._repository
			.createQueryBuilder("p")
			.where("p.pace->>'state' = :state", { state })
			.andWhere(`p.${identifier.key} = :value`, { value: identifier.value })
			.getOne();

		return presentation !== null;
	}
}
