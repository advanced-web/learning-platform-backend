import moment from "moment";
import { RandomCipherGenerator } from "../common";
import { ResetPasswordRequest } from "../core";
import { ResetPasswordConfig } from "../infrastructure";
import { ResetPasswordRequestRepository } from "../repositories/reset-password-request.repository";
import { BaseService } from "./base.service";

export class ResetPasswordRequestService extends BaseService<ResetPasswordRequest> {
	constructor() {
		super();
		this._repository = ResetPasswordRequestRepository;
	}

	async generateResetTokenAsync(email: string): Promise<string> {
		const token = RandomCipherGenerator.init().generateRandomBytes(32);

		// Save the token
		const oldToken = await this.getRecordByAsync({ email });
		if (!oldToken) {
			await this.saveAsync({
				email,
				resetToken: token,
				expireAt: moment().add(ResetPasswordConfig.REST_PWD_TOKEN_LIFE_TIME, "seconds").toDate(),
			});
		} else {
			await this.updateRecordByIdAsync(oldToken.id, {
				resetToken: token,
				expireAt: moment().add(ResetPasswordConfig.REST_PWD_TOKEN_LIFE_TIME, "seconds").toDate(),
			});
		}

		return token;
	}

	async getValidExpireRecordByToken(token: string) {
		const oldToken = await this.getRecordByAsync({ resetToken: token });
		if (!oldToken) {
			return null;
		}

		if (moment().isSameOrAfter(moment(oldToken.expireAt))) {
			return null;
		}

		return oldToken;
	}
}
