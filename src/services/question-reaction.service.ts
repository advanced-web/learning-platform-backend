import { QuestionReaction } from "../core";
import { QuestionReactionRepository } from "../repositories";
import { BaseService } from "./base.service";

export class QuestionReactionService extends BaseService<QuestionReaction> {
	constructor() {
		super();
		this._repository = QuestionReactionRepository;
	}
}
