import * as bcrypt from "bcrypt";
import moment from "moment";
import { JwtBuilder } from "../common";
import { ResUser, ResUserProfile } from "../core";
import { TokenConfig } from "../infrastructure";

export class LocalAuthService {
	constructor() {}

	signOut() {}

	compareUserPassword(rawPassword: string, encrypted: string): boolean {
		const isPasswordValid = bcrypt.compareSync(rawPassword, encrypted);
		return isPasswordValid;
	}

	generateAccessToken(user: ResUser, profile: ResUserProfile): { accessToken: string; expiresAt: string } {
		// get user information
		const accessToken = JwtBuilder.init()
			.withAlgorithm("HS256")
			.withExpiresIn(TokenConfig.ACCESS_TOKEN_LIFE_TIME)
			.withPayload({
				uid: user.id,
				username: user.username,
				email: profile.email,
				email_verified: user.isActive,
				avatar: profile.avatar,
				display_name: profile.displayName,
			})
			.sign(TokenConfig.ACCESS_TOKEN_SIGN);
		return {
			accessToken,
			expiresAt: moment().add(TokenConfig.ACCESS_TOKEN_LIFE_TIME, "seconds").toISOString(),
		};
	}
}
