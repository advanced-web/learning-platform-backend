import { CipherDecoder, CipherGenerator } from "../common";
import { PresentationCollaborator } from "../core/entities/presentation-collaborator.entity";
import { InviteCollaboratorConfig } from "../infrastructure";
import { PresentationCollaboratorRepository } from "../repositories/presentation-collaborator.repository";
import { BaseService } from "./base.service";
import stringify from "fast-safe-stringify";
import { PRESENTATION_COLLAB_STATUS } from "../shared";
import moment from "moment";

export class PresentationCollaboratorService extends BaseService<PresentationCollaborator> {
	constructor() {
		super();
		this._repository = PresentationCollaboratorRepository;
	}

	async prepareSendCollaboratorInvitationDataAsync(pesentationSeriesId: string, emails: string[]) {
		const data: Array<{ email: string; invitationLink: string }> = [];
		const baseUrl = InviteCollaboratorConfig.INVITE_COLLAB_REDIRECT.replace(":id", pesentationSeriesId);

		for (let email of emails) {
			const collaborator = await this.getRecordByAsync({ pesentationSeriesId, email });
			if (!collaborator) {
				// generate token and insert
				const tokenInfo = this.generateInvitationToken(email);
				await this.saveAsync({
					pesentationSeriesId,
					email,
					status: PRESENTATION_COLLAB_STATUS.PENDING,
					invitationToken: tokenInfo.token,
					invitationTokenKey: tokenInfo.iv,
				});

				data.push({
					email,
					invitationLink: `${baseUrl}?token=${tokenInfo.token}`,
				});
			} else if (collaborator.status === PRESENTATION_COLLAB_STATUS.PENDING) {
				data.push({
					email,
					invitationLink: `${baseUrl}?token=${collaborator.invitationToken}`,
				});
			}
		}

		return data;
	}

	generateInvitationToken(email: string) {
		const token = CipherGenerator.init()
			.withEncoding("hex")
			.withPayload(
				stringify({
					email,
					exp: moment().add(InviteCollaboratorConfig.INVITE_COLLAB_TOKEN_LIFE_TIME, "seconds").unix(),
				})
			)
			.sign(InviteCollaboratorConfig.INVITE_COLLAB_TOKEN_SIGN);

		return token;
	}

	decodeInvitationToken(token: string, iv: string) {
		// verify token
		const resultString = CipherDecoder.init().decode(token, iv, InviteCollaboratorConfig.INVITE_COLLAB_TOKEN_SIGN);
		const result = JSON.parse(resultString) as { email: string; exp: number };

		// check the expiry
		if (moment(result.exp * 1000).isBefore(moment())) {
			return null;
		}

		return result;
	}

	async getCollaboratorListWithUserAsync(
		filter: { presentationId: string },
		limit: number,
		page: number
	): Promise<[Array<PresentationCollaborator & { display_name: string; avatar: string }>, number]> {
		const query = this._repository
			.createQueryBuilder("collab")
			.leftJoin("res_user_profile", "u", "collab.user_id = u.id")
			.where("collab.pesentation_series_id = :presentationId", { presentationId: filter.presentationId });

		const count = await query.getCount();

		const fetchedFields = [
			"collab.id as id",
			"collab.pesentation_series_id",
			"collab.user_id",
			"collab.email as email",
			"collab.status as status",
			"collab.invited_at",
			"collab.joined_at",
			"u.display_name",
			"u.avatar as avatar",
		];

		const pagingQuery = query
			.select(fetchedFields)
			.offset(limit * (page - 1))
			.limit(limit);

		const result = await pagingQuery.getRawMany<
			PresentationCollaborator & { display_name: string; avatar: string }
		>();

		return [result, count];
	}
}
