import * as bcrypt from "bcrypt";
import moment from "moment";
import { CipherGenerator } from "../common";
import { ResUser } from "../core";
import { TokenConfig } from "../infrastructure";
import { ResUserRepository } from "../repositories";
import { BaseService } from "./base.service";

export class ResUserService extends BaseService<ResUser> {
	constructor() {
		super();
		this._repository = ResUserRepository;
	}

	getUserByUsernameAsync(username: string): Promise<ResUser[]> {
		return this._repository.findBy({ username });
	}

	createNewUserAsync(user: Partial<Omit<ResUser, "id">>): Promise<ResUser> {
		let hashed = "";
		if (user.password) {
			const salt = bcrypt.genSaltSync(12);
			hashed = bcrypt.hashSync(user.password, salt);
		}

		return this._repository.save({ ...user, password: hashed });
	}

	async generateUserActivationTokenAsync(user: Partial<ResUser>): Promise<string> {
		const tokenInfo = CipherGenerator.init()
			.withPayload(
				JSON.stringify({
					userId: user.id,
					exp: moment().add(TokenConfig.ACTIVATE_TOKEN_LIFE_TIME, "seconds").unix(),
				})
			)
			.sign(TokenConfig.ACTIVATE_TOKEN_SIGN);

		// Save the token
		await this._repository.update(
			{ id: user.id },
			{ activationToken: tokenInfo.token, activationTokenKey: tokenInfo.iv }
		);

		return tokenInfo.token;
	}

	getUserByTokenAsync(token: string): Promise<ResUser | null> {
		return this._repository.findOne({ where: { activationToken: token } });
	}

	updatePasswordByIdAsync(id: number, rawPassword: string) {
		const salt = bcrypt.genSaltSync(12);
		const hashedPassword = bcrypt.hashSync(rawPassword, salt);
		const savedData = { password: hashedPassword }

		return this._repository.update({ id }, savedData);
	}

	findOAuthUserAsync(provider: string, oauthUserId: string, email: string) {
		return this._repository
			.createQueryBuilder("user")
			.where("user.username = :email", { email })
			.orWhere("user.oauth_provider = :provider AND user.oauth_uid = :oauthUid", {
				provider,
				oauthUid: oauthUserId,
			})
			.getOne();
	}
}
