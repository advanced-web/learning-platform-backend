import { Logger } from '../common/utils/logger/logger';
import { VotingResults } from "../core";
import { VotingResultsRepository } from "../repositories";
import { BaseService } from "./base.service";

export class VotingResultsService extends BaseService<VotingResults> {
	constructor() {
		super();
		this._repository = VotingResultsRepository;
	}

	getResultsByAdminKeyAndCountByAnswerId(adminKey: string) {
		const queryBuilder = this._repository
			.createQueryBuilder("v")
			.select("answer_id", "answerId")
			.addSelect("COUNT(*)", "answerCount")
			.where("v.slide_admin_key = :adminKey", { adminKey })
			.groupBy("v.answer_id");

		Logger.debug(`countByAnswer - count - ${queryBuilder.getSql()}`, "Query");
		return queryBuilder.getRawMany<{ answerId: number, answerCount: number; }>();
	}

	getResultsBySlideIdAndCountByAnswerId(slideId: number, limit: number, page: number) {
		return this._repository
			.createQueryBuilder("vr")
			.where("vr.slide_id = :slideId", { slideId })
			.skip(limit * (page - 1))
			.take(limit)
			.orderBy("voted_at", "ASC")
			.getManyAndCount();
	}
}
