import { FindOptionsOrderValue } from "typeorm";
import { PresentationChat } from "../core";
import { PresentationChatRepository } from "../repositories";
import { BaseService } from "./base.service";

export class PresentationChatService extends BaseService<PresentationChat> {
	constructor() {
		super();
		this._repository = PresentationChatRepository;
	}

	/**
	 *
	 * @description Get recent chat messages by default - order by created_at desc
	 */
	async getChatHistoryAsync(
		filter: { presentationSeriesId: string; presentNo: number; orderBy: FindOptionsOrderValue },
		limit: number,
		page: number
	): Promise<[PresentationChat[], number]> {
		const result = await this._repository.findAndCount({
			where: { presentNo: filter.presentNo, presentationSeriesId: filter.presentationSeriesId },
			order: {
				createdAt: filter.orderBy,
			},
			take: limit,
			skip: limit * (page - 1),
		});

		return result;
	}
}
