import { UserPermission } from "../core";
import { UserPermissionRepository } from "../repositories";
import { BaseService } from "./base.service";

export class UserPermissionService extends BaseService<UserPermission> {
	constructor() {
		super();

		this._repository = UserPermissionRepository;
	}

	async verifyUserPermissionAsync(
		subjectId: number,
		resourceId: number,
		resourceType: string,
		requiredPermissions: string[]
	): Promise<boolean> {
		const resPermission = await this.getRecordByAsync({ subjectId, resourceId, resourceType });

		// if there is no permission => assume this user is not allowed
		if (!resPermission) {
			return false;
		}

		const userPermissionList = resPermission.permissions.split(",");

		for (let reqPermission of requiredPermissions) {
			if (!userPermissionList.includes(reqPermission)) {
				return false;
			}
		}

		return true;
	}
}
