import { CipherGenerator } from "../common";
import { LearningGroup } from "../core";
import { AppConfig, InvitationLinkConfig } from "../infrastructure";
import { LearningGroupRepository } from "../repositories";
import { GROUP_MEMBER_ROLE } from "../shared";
import { DEFAULT_GROUP_PERMISSION, GROUP_PERMISSIONS } from "../shared/permission.const";
import { BaseService } from "./base.service";

export class LearningGroupService extends BaseService<LearningGroup> {
	constructor() {
		super();
		this._repository = LearningGroupRepository;
	}

	// !Refactor query and function parameters
	async getGroupListAsync(
		filter: { uid?: string | number; searchKey?: string; groupIdList?: number[] },
		limit: number,
		page: number
	) {
		let query = this._repository.createQueryBuilder("group");

		if (filter.groupIdList) {
			query = query.whereInIds(filter.groupIdList);
		}

		if (filter.uid) {
			query = query.andWhere("group.ownerProfileId = :uid", { uid: filter.uid });
		}

		// implement full text search here
		if (filter.searchKey) {
			const keyList = filter.searchKey.split(" ").join("&");
			query = query.andWhere(`search_index_col @@ to_tsquery(vn_unaccent('${keyList}'))`);
		}

		return query
			.skip(limit * (page - 1))
			.take(limit)
			.getManyAndCount();
	}

	getRawGroupByIdAsync(id: number, fields: string[]): Promise<Partial<LearningGroup> | undefined> {
		return this._repository.createQueryBuilder("group").select(fields).where("group.id = :id", { id }).getRawOne();
	}

	async generateAndSaveInvitationLinkAsync(id: number): Promise<string> {
		const invitationInfo = CipherGenerator.init()
			.withPayload(
				JSON.stringify({
					groupId: id,
					role: GROUP_MEMBER_ROLE.MEMBER,
					permissions: DEFAULT_GROUP_PERMISSION.MEMBER.join(","),
				})
			)
			.sign(InvitationLinkConfig.INVITATION_LINK_SIGN);

		// client_main_site + groups/invite/:code
		const invitationLink = `${AppConfig.CLIENT_MAIN_SITE}/groups/invite/${invitationInfo.token}`;

		// Save the invitation link
		await this._repository.update(
			{ id },
			{
				invitationLink,
				invitationToken: invitationInfo.token,
				invitationTokenKey: invitationInfo.iv,
			}
		);

		return invitationLink;
	}
}
