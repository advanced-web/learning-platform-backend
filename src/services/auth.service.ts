import axios, { AxiosError } from "axios";
import moment from "moment";
import * as queryString from "query-string";
import { v4 as uuidV4 } from "uuid";
import { ServiceException } from "../common";
import { OktaConfig } from "../infrastructure";
import { API_MESSAGES } from "../shared";

export class AuthService {
	constructor() {}

	getLoginURI() {
		const state = uuidV4();
		const query = {
			client_id: OktaConfig.CLIENT_ID,
			response_type: OktaConfig.RESPONSE_TYPE,
			scope: OktaConfig.SCOPE,
			redirect_uri: OktaConfig.REDIRECT_URI,
			state,
		};

		return `${OktaConfig.AUTH_SERVER_URI}${OktaConfig.endpoints.AUTHORIZE}?${queryString.stringify(query)}`;
	}

	async obtainTokenDataAsync(code: string) {
		const body = {
			grant_type: OktaConfig.GRANT_TYPE,
			client_id: OktaConfig.CLIENT_ID,
			client_secret: OktaConfig.CLIENT_SECRET,
			code,
			redirect_uri: OktaConfig.REDIRECT_URI,
		};

		try {
			const res = await axios.post(
				`${OktaConfig.AUTH_SERVER_URI}${OktaConfig.endpoints.TOKEN}`,
				queryString.stringify(body),
				{
					headers: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
				}
			);
			const data = res.data;
			const tokenData = {
				tokenType: data.token_type,
				accessToken: data.access_token,
				refreshToken: data.refresh_token,
				idToken: data.id_token,
				expiresIn: data.expires_in,
				expiresTime: moment().add(data.expires_in, "seconds").toISOString(),
			};
			return tokenData;
		} catch (error) {
			if (error instanceof AxiosError && error.response) {
				throw new ServiceException("Đăng nhập không thành công, vui lòng thử lại", 400, {
					status: error.response.status,
					data: error.response.data,
				});
			}

			throw new ServiceException(API_MESSAGES.INTERNAL_ERROR, 500, error);
		}
	}

	async introspectTokenAsync(token: string) {
		const body = {
			client_id: OktaConfig.CLIENT_ID,
			client_secret: OktaConfig.CLIENT_SECRET,
			token,
			token_type_hint: "access_token",
		};

		try {
			const res = await axios.post(
				`${OktaConfig.AUTH_SERVER_URI}${OktaConfig.endpoints.INTROSPECT}`,
				queryString.stringify(body),
				{
					headers: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
				}
			);
			const data = res.data;
			return data;
		} catch (error) {
			if (error instanceof AxiosError && error.response) {
				throw new ServiceException(API_MESSAGES.INTERNAL_ERROR, 500, {
					status: error.response.status,
					data: error.response.data,
				});
			}

			throw new ServiceException(API_MESSAGES.INTERNAL_ERROR, 500, error);
		}
	}

	async getUserInformationAsync(token: string) {
		try {
			const res = await axios.get(`${OktaConfig.AUTH_SERVER_URI}${OktaConfig.endpoints.USERINFO}`, {
				headers: {
					Authorization: `Bearer ${token}`,
				},
			});
			const data = res.data;
			return {
				name: data.name,
				profile: data.profile,
				email: data.email,
				picture: data.picture,
				gender: data.gender,
				preferredUsername: data.preferred_username,
				givenName: data.given_name,
				familyName: data.family_name,
				emailVerified: data.email_verified,
			};
		} catch (error) {
			if (error instanceof AxiosError && error.response) {
				throw new ServiceException(error.response.data.error, error.response.status, {
					status: error.response.status,
					data: error.response.data,
				});
			}

			throw new ServiceException(API_MESSAGES.INTERNAL_ERROR, 500, error);
		}
	}
}
