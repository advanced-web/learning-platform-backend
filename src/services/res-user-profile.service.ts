import _ from "lodash";
import { ResUser, ResUserProfile } from "../core";
import { ResUserProfileRepository } from "../repositories";
import { BaseService } from "./base.service";

export class ResUserProfileService extends BaseService<ResUserProfile> {
	constructor() {
		super();
		this._repository = ResUserProfileRepository;
	}

	getProfileByUserIdAsync(userId: number) {
		return this._repository.findOne({ where: { userId } });
	}

	getRawProfileByUserIdAsync(userId: number, fields: string[]): Promise<Partial<ResUser> | undefined> {
		return this._repository
			.createQueryBuilder("profile")
			.select(fields)
			.where("profile.user_id = :userId", { userId })
			.getRawOne();
	}

	updatePartiallyByUserIdAsync(userId: number, data: Partial<ResUserProfile>) {
		return this._repository.update({ userId }, data);
	}
}
