import { Logger } from "../common/utils/logger/logger";
import { LearningGroupMember } from "../core";
import { LearningGroupMemberRepository } from "../repositories";
import { GROUP_MEMBER_ACTIVITY_KEYS, GROUP_MEMBER_ROLE } from "../shared";
import { BaseService } from "./base.service";

type GroupMemberList = {
	id: number;
	user_profile_id: number;
	user_display_name: string;
	role_name: string;
	avatar: string | null;
	email: string;
};

export class LearningGroupMemberService extends BaseService<LearningGroupMember> {
	constructor() {
		super();
		this._repository = LearningGroupMemberRepository;
	}

	getGroupListByMemberIdRoleAsync(userId: number, role: string): Promise<[LearningGroupMember[], number]> {
		const statusList = [GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP, GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP];

		return this._repository
			.createQueryBuilder("group_member")
			.where("group_member.userProfileId = :userId", { userId })
			.andWhere("group_member.role = :role", { role })
			.andWhere("group_member.current_status->>'key' IN (:...statusList)", { statusList })
			.getManyAndCount();
	}

	async getRawMemberListJoinUserByGroupIdAsync(
		groupId: number,
		limit: number,
		page: number
	): Promise<[GroupMemberList[], number]> {
		const fetchedFields = [
			"member.id",
			"member.user_profile_id",
			"member.user_display_name",
			"member.role_name",
			"member.role",
			"user_profile.avatar",
			"user_profile.email",
			"member.current_status",
			"member.activity",
		];
		const statusList = [GROUP_MEMBER_ACTIVITY_KEYS.CREATE_GROUP, GROUP_MEMBER_ACTIVITY_KEYS.JOINED_GROUP];

		const queryBuilder = this._repository
			.createQueryBuilder("member")
			.innerJoin("res_user_profile", "user_profile", "member.user_profile_id = user_profile.user_id")
			.where("member.group_id = :groupId", { groupId })
			.andWhere("member.current_status->>'key' IN (:...statusList)", { statusList });

		Logger.debug(`getRawMemberListJoinUserByGroupIdAsync - count - ${queryBuilder.getSql()}`, "Query");
		const memberCount = await queryBuilder.getCount();

		const paginationQueryBuilder = queryBuilder
			.select(fetchedFields)
			.skip(limit * (page - 1))
			.take(limit);

		Logger.debug(
			`getRawMemberListJoinUserByGroupIdAsync - pagination - ${paginationQueryBuilder.getSql()}`,
			"Query"
		);
		const memberList = await paginationQueryBuilder.getRawMany<GroupMemberList>();

		return [memberList, memberCount];
	}

	async existsByStatusListAsync(groupId: number, userId: number, statusList: string[]) {
		const numberOfRecords = await this._repository
			.createQueryBuilder("member")
			.where("member.group_id = :groupId AND member.user_profile_id = :userId", { groupId, userId })
			.andWhere("member.current_status->>'key' IN (:...statusList)", { statusList })
			.getCount();

		return numberOfRecords === 1;
	}

	async existsByStatusListAndRoleListAsync(
		groupId: number,
		userId: number,
		statusList: string[],
		roleList: GROUP_MEMBER_ROLE[]
	) {
		const numberOfRecords = await this._repository
			.createQueryBuilder("member")
			.where("member.group_id = :groupId AND member.user_profile_id = :userId", { groupId, userId })
			.andWhere("member.current_status->>'key' IN (:...statusList)", { statusList })
			.andWhere("member.role IN (:...roleList)", { roleList })
			.getCount();

		return numberOfRecords === 1;
	}
}
