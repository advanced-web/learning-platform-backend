import { nanoid } from "nanoid";
import { ValueGenerator } from '../common';
import { Logger } from "../common/utils/logger/logger";
import { HttpErrorBuilder, PresentationPace, Presentations, Slides } from "../core";
import { PresentationsRepository, SlidesRepository, VotingResultsRepository } from "../repositories";
import { HTTP_CODE, PRESENTATION_PACE_STATES, RESPONSE_CODE, SLIDE_TYPE } from "../shared";
import { BaseService } from "./base.service";

export class SlidesService extends BaseService<Slides> {
	constructor() {
		super();
		this._repository = SlidesRepository;
	}

	getRawSlidesBySeriesId(seriesId: string, fields: string[]) {
		return this._repository.createQueryBuilder("slide")
			.select(fields)
			.where("slide.presentation_series_id = :seriesId", { seriesId })
			.orderBy("slide.position", "ASC")
			.getRawMany<Partial<Slides>>();
	}

	async createSlideAndUpdatePresentation(slideType: SLIDE_TYPE, presentation: Presentations) {
		// Generates default slide choices
		const slideChoices = ValueGenerator.instance.generateSlideChoices(slideType);

		// Creates slide
		Logger.debug("Creating slide");
		const createdSlide = await this.saveAsync({
			adminKey: nanoid(),
			presentationId: presentation.id,
			presentationSeriesId: presentation.seriesId,
			question: "",
			type: slideType,
			choices: slideChoices,
			position: presentation.slideCount,
		});
		Logger.debug(`Created slide, id admin_key ${createdSlide.id} ${createdSlide.adminKey}`);

		// Updates presentation's slide count and pace
		if (createdSlide.id) {
			const updateData: Partial<Presentations> = { slideCount: presentation.slideCount + 1 };

			// If this is the first slide, it will be set to presented slide
			if (presentation.slideCount === 0) {
				updateData.pace = {
					...presentation.pace,
					active: createdSlide.adminKey,
				};
			}

			Logger.debug(`Updating slide count and pace`);
			await PresentationsRepository.update(presentation.id, updateData);
			Logger.debug(`Updated slide count and pace`);
		}

		// Generates default slide voting results
		// const slideVotingResults: SlideVotingResult[] = slideChoices.map(choice => {
		// 	return {
		// 		id: choice.id,
		// 		label: choice.label,
		// 		score: [0],
		// 	};
		// });

		// Creates voting results
		// Logger.debug("Creating voting results");
		// await VotingResultsRepository.save({
		// 	slideId: createdSlide.id,
		// 	slideAdminKey: createdSlide.adminKey,
		// 	respondents: 0,
		// 	results: slideVotingResults,
		// });
		// Logger.debug("Created voting results");

		return createdSlide;
	}

	async deleteByAdminKeyAndPresentationInstance(adminKey: string, presentation: Presentations) {
		const { seriesId, slideCount, id: presentationId, pace: presentationPace } = presentation;

		// Fetchs one slide by admin_key and series_id
		const slideToDelete = await this.getRecordByAsync({ adminKey, presentationSeriesId: seriesId });

		// Slide is not found/not belongs to the presentation
		if (!slideToDelete) {
			Logger.error("Slide is not found/not belongs to the presentation");

			HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.SLIDE_NOT_FOUND)
				.throw();
			return;
		}

		// Deletes voting result and slide
		await VotingResultsRepository.delete({ slideAdminKey: adminKey });
		const deleteResult = await this._repository.delete({ adminKey });

		// Updates the remaining slides' position
		// If the removed slide is the last slide, you can
		if (slideToDelete.position !== slideCount - 1) {
			await this._repository.query(
				"update slides set position = position - 1 where position > $1 and presentation_series_id = $2;",
				[slideToDelete.position, seriesId],
			);
		}

		// If it's pace slide, then updating new presentation's pace
		if (presentationPace.active === slideToDelete.adminKey) {
			const replacedSlidePosition = slideToDelete.position - (slideToDelete.position === slideCount - 1 ? 1 : 0);

			// Fetchs the replacement of the slide
			// It always isn't null, because the presentation always has at least one slide
			const replacedSlide = await this.getRecordByAsync({
				position: replacedSlidePosition,
				presentationSeriesId: seriesId,
			});

			// Infers new presentation's pace
			let newPace: PresentationPace | undefined = undefined;
			if (replacedSlide) {
				newPace = {
					...presentationPace,
					active: replacedSlide.adminKey,
					state: PRESENTATION_PACE_STATES.IDLE,
				};
			}

			// Updates the presentation's pace and slide count
			await PresentationsRepository.update(presentationId, { pace: newPace, slideCount: slideCount - 1 });
		} else {
			// Updates the presentation's slide count
			await PresentationsRepository.update(presentationId, { slideCount: slideCount - 1 });
		}

		return deleteResult;
	}
}
