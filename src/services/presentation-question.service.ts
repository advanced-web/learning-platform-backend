import { FindOptionsOrderValue } from "typeorm";
import { PresentationQuestion } from "../core";
import { PresentationQuestionRepository } from "../repositories";
import { BaseService } from "./base.service";

export class PresentationQuestionService extends BaseService<PresentationQuestion> {
	constructor() {
		super();
		this._repository = PresentationQuestionRepository;
	}

	/**
	 *
	 * @description Get recent chat messages by default - order by created_at desc
	 */
	async getQuestionListAsync(
		filter: {
			presentationSeriesId: string;
			isArchive: boolean;
			orderByTotal?: FindOptionsOrderValue;
			orderByTime?: FindOptionsOrderValue;
		},
		limit: number,
		page: number
	): Promise<[PresentationQuestion[], number]> {
		const result = await this._repository.findAndCount({
			where: { presentationSeriesId: filter.presentationSeriesId, isArchive: filter.isArchive },
			order: {
				totalLike: filter.orderByTotal,
				createdAt: filter.orderByTime,
			},
			take: limit,
			skip: limit * (page - 1),
		});

		return result;
	}
}
