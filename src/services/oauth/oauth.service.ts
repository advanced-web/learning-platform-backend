import { BaseOauthStrategyType, HttpErrorBuilder, OAuthBaseCallbackQuery, OAuthStrategyMap } from "../../core";
import { HTTP_CODE, OAUTH_PROVIDERS, RESPONSE_CODE } from "../../shared";
import { OAuthGoogleLogin } from "./strategies";

export class OAuthService {
	private strategies: OAuthStrategyMap = {};

	private _getOAuthStrategy(provider: string): BaseOauthStrategyType {
		let strategy = this.strategies[provider];
		if (strategy) return strategy;

		if (provider === OAUTH_PROVIDERS.GOOGLE) {
			return new OAuthGoogleLogin();
		}

		const httpError = HttpErrorBuilder.init()
			.withStatusCode(HTTP_CODE.BAD_REQUEST)
			.withResponseCode(RESPONSE_CODE.OAUTH_PROVIDER_NOT_SUPPORT)
			.build();
		throw httpError;
	}

	getLoginURL(provider: string) {
		const strategy = this._getOAuthStrategy(provider);
		return strategy.createLoginURL();
	}

	processOAuthCallback(provider: string, query: OAuthBaseCallbackQuery) {
		const strategy = this._getOAuthStrategy(provider);
		return strategy.performLoginFlow(query);
	}
}
