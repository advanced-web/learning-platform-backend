import { AxiosRequestConfig } from "axios";
import qs from "qs";
import { Logger } from "../../../common/utils/logger/logger";
import {
	HttpErrorBuilder,
	OAuthGoogleAccessTokenResponse,
	OAuthGoogleCallbackError,
	OAuthGoogleCallbackResponse,
	OAuthGoogleProfile,
	OAuthStateParams,
	ParsedOAuthUserProfile
} from "../../../core";
import { GoogleOAuthConfig } from "../../../infrastructure";
import { HTTP_CODE, OAUTH_PROVIDERS, RESPONSE_CODE } from "../../../shared";
import { BaseOAuthSocialLogin } from "./base-oauth.strategy";

const logger = new Logger("google-oauth.strategy");

export class OAuthGoogleLogin extends BaseOAuthSocialLogin<
	OAuthGoogleProfile,
	OAuthGoogleCallbackResponse,
	OAuthGoogleCallbackError,
	OAuthGoogleAccessTokenResponse
> {
	provider = OAUTH_PROVIDERS.GOOGLE;
	config = {
		authenticationEndpoint: GoogleOAuthConfig.AUTHENTICATION_ENDPOINT,
		validationEndpoint: GoogleOAuthConfig.VALIDATION_ENDPOINT,
		dataEndpoint: GoogleOAuthConfig.DATA_ENDPOINT,
		clientId: GoogleOAuthConfig.CLIENT_ID,
		clientSecret: GoogleOAuthConfig.CLIENT_SECRET,
		responseType: GoogleOAuthConfig.RESPONSE_TYPE,
		scope: GoogleOAuthConfig.SCOPE,
		prompt: GoogleOAuthConfig.PROMPT,
		grantType: GoogleOAuthConfig.GRANT_TYPE,
		accessType: GoogleOAuthConfig.ACCESS_TYPE,
		accessTokenContentType: GoogleOAuthConfig.ACCESS_TOKEN_CONTENT_TYPE,
		redirectUri: GoogleOAuthConfig.REDIRECT_URI,
	};

	createLoginURL() {
		const state: OAuthStateParams = { provider: this.provider };
		const params = [
			`client_id=${this.config.clientId}`,
			`state=${JSON.stringify(state)}`,
			`scope=${this.config.scope}`,
			`response_type=${this.config.responseType}`,
			`prompt=${this.config.prompt}`,
			`access_type=${this.config.accessType}`,
			`redirect_uri=${this.config.redirectUri}`,
		];

		return `${this.config.authenticationEndpoint}?${params.join("&")}`;
	}

	createRequestToGetAccessToken(url: string, authorizationCode: string): AxiosRequestConfig {
		const method = "POST";
		const headers = { "Content-Type": this.config.accessTokenContentType };
		const data = qs.stringify({
			code: authorizationCode,
			client_id: this.config.clientId,
			client_secret: this.config.clientSecret,
			redirect_uri: this.config.redirectUri,
			grant_type: this.config.grantType,
		});

		return { url, method, headers, data };
	}

	createRequestToGetProfile(url: string, accessToken: string): AxiosRequestConfig {
		const method = "GET";
		const headers = { Authorization: `Bearer ${accessToken}` };

		return { url, method, headers };
	}

	parseOAuthUserProfile(profile: OAuthGoogleProfile): ParsedOAuthUserProfile {
		if (typeof profile.id !== "string" || profile.id === "" || typeof profile.name !== "string") {
			logger.error("Invalid oauth user id and/or name");

			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.OAUTH_INVALID_REQUESTED_USER_PROFILE)
				.build();
			throw httpError;
		}

		return {
			email: profile.email,
			oauthUid: profile.id,
			firstName: profile.given_name,
			lastName: profile.family_name,
			picture: profile.picture,
		};
	}
}
