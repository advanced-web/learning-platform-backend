import axios, { AxiosRequestConfig } from "axios";
import _ from "lodash";
import fs from "fs";
import Handlebars from "handlebars";
import {
	HttpErrorBuilder,
	OAuthBaseAccessTokenResponse,
	OAuthBaseCallbackError,
	OAuthBaseCallbackResponse,
	OAuthConfig,
	OAuthProvider,
	OAuthUserCreationData,
	ParsedOAuthUserProfile
} from "../../../core";
import { HTTP_CODE, RESPONSE_CODE } from "../../../shared";
import { AppConfig, EmailService } from "../../../infrastructure";
import { Logger } from "../../../common/utils/logger/logger";
import { ResUserProfileService, ResUserService } from "../..";

const logger = new Logger("base-oauth.strategy");

export abstract class BaseOAuthSocialLogin<
	OAuthUserProfile extends object,
	AuthCallbackResponse extends OAuthBaseCallbackResponse,
	AuthCallbackError extends OAuthBaseCallbackError,
	AccessTokenResponse extends OAuthBaseAccessTokenResponse
> {
	protected _userProfileService = new ResUserProfileService();
	protected _userService = new ResUserService();

	protected abstract provider: OAuthProvider;
	protected abstract config: OAuthConfig;

	abstract createLoginURL(): string;
	protected abstract createRequestToGetAccessToken(url: string, authorizationCode: string): AxiosRequestConfig;
	protected abstract createRequestToGetProfile(url: string, accessToken: string): AxiosRequestConfig;
	protected abstract parseOAuthUserProfile(profile: OAuthUserProfile): ParsedOAuthUserProfile;

	async performLoginFlow(authCallbackQuery: AuthCallbackResponse | AuthCallbackError) {
		// authorization code
		const code = _.get(authCallbackQuery, "code");
		if (typeof code !== "string") {
			// TODO: handle oauth error
			logger.error("Failed to authenticate");

			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.UNAUTHORIZED)
				.withResponseCode(RESPONSE_CODE.OAUTH_FAILED_TO_AUTHENTICATE)
				.build();
			throw httpError;
		}

		// get access token from code
		const accessToken = await this.getAccessToken(code);
		if (!accessToken) {
			logger.error("Cannot get access token by authorization code");

			const httpError = HttpErrorBuilder.init()
				.withStatusCode(HTTP_CODE.BAD_REQUEST)
				.withResponseCode(RESPONSE_CODE.OAUTH_FAILED_TO_OBTAIN_ACCESS_TOKEN)
				.build();
			throw httpError;
		}

		const oauthUserProfile = await this.getOAuthUserProfile(accessToken);
		const parsedProfile = this.parseOAuthUserProfile(oauthUserProfile);

		// create oauth user
		return this.createUserIfNeeded(parsedProfile);
	}

	private async getAccessToken(authorizationCode: string) {
		const axiosConfig = this.createRequestToGetAccessToken(this.config.validationEndpoint, authorizationCode);
		const accessTokenResp = await this.performOAuthRequest<AccessTokenResponse>(axiosConfig, {
			beforeCalling: `Perform OAuth request ${this.provider} to get access token`,
			callingCompleted: `Completed OAuth request ${this.provider}`,
		});

		return accessTokenResp.access_token;
	}

	private async getOAuthUserProfile(accessToken: string): Promise<OAuthUserProfile> {
		const axiosConfig = this.createRequestToGetProfile(this.config.dataEndpoint, accessToken);
		const userProfile = await this.performOAuthRequest<OAuthUserProfile>(axiosConfig, {
			beforeCalling: `Perform OAuth request ${this.provider} to get user profile`,
			callingCompleted: `Completed OAuth request ${this.provider}`,
		});

		return userProfile;
	}

	private async createUserIfNeeded(parsedProfile: ParsedOAuthUserProfile): Promise<OAuthUserCreationData> {
		const { email, oauthUid, firstName, lastName, picture } = parsedProfile;

		// find credentials
		const foundUser = await this._userService.findOAuthUserAsync(this.provider, oauthUid, email);

		// user has already logged in
		if (foundUser) {
			const foundUserProfile = await this._userProfileService.getRecordByAsync({ userId: foundUser.id });
			if (!foundUserProfile) {
				const httpError = HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.INTERNAL_SERVER_ERROR)
					.build();
				throw httpError;
			}

			if (!foundUser.isActive) {
				// generate activation code
				const activationToken = await this._userService.generateUserActivationTokenAsync(foundUser);

				// !MUST REFACTOR: push to event emiiter (observer pattern is recommened)
				// send email with link
				const link = `${AppConfig.SERVER_URL}/account/activate?token=${activationToken}`;
				const content = fs.readFileSync(`src/views/templates/emails/account-activation.hbs`, "utf8");
				const template = Handlebars.compile(content);
				new EmailService()
					.send(
						foundUser.username,
						"[H2A-Platform] - Kích hoạt tài khoản",
						template({ userDisplayName: foundUserProfile?.displayName, activateLink: link, shortenActivateLink: link })
					)
					.then(console.log)
					.catch(console.log);

				const httpError = HttpErrorBuilder.init()
					.withStatusCode(HTTP_CODE.FORBIDDEN)
					.withResponseCode(RESPONSE_CODE.INACTIVE_USER)
					.withErrors({ email })
					.build();
				throw httpError;
			}

			return { user: foundUser, userProfile: foundUserProfile };
		}

		// create new user
		const userData = {
			username: email,
			isActive: true,
			oauthProvider: this.provider,
			oauthUid,
		};
		const createdUser = await this._userService.saveAsync(userData);

		// create new user profile
		const userProfileData = {
			firstName,
			lastName,
			displayName: `${firstName} ${lastName}`,
			email,
			avatar: picture,
			userId: createdUser.id,
		};
		const createdUserProfile = await this._userProfileService.saveAsync(userProfileData);

		return { user: createdUser, userProfile: createdUserProfile };
	}

	async performOAuthRequest<ResponseType extends object>(
		axiosConfig: AxiosRequestConfig,
		messageLog: { beforeCalling: string; callingCompleted: string }
	): Promise<ResponseType> {
		let isLoggedError = false;
		let isLoggedAfterCallingAPI = false;

		const httpError = HttpErrorBuilder.init()
			.withStatusCode(HTTP_CODE.BAD_REQUEST)
			.withResponseCode(RESPONSE_CODE.OAUTH_COMMON)
			.build();

		try {
			logger.debug(messageLog.beforeCalling);
			const resp = await axios(axiosConfig);

			const status = resp.status;
			const data = resp.data;
			isLoggedError = true;

			if (status >= 400 || !data) {
				logger.error("status >= 400 or missing data, error when calling to OAuth");
				throw httpError;
			}

			logger.debug(messageLog.callingCompleted);
			isLoggedAfterCallingAPI = true;

			return resp.data;
		} catch (error) {
			if (!isLoggedError) {
				logger.error(JSON.stringify(error));
			}
			if (!isLoggedAfterCallingAPI) {
				logger.debug(messageLog.callingCompleted);
			}

			throw httpError;
		}
	}
}
