import _ from "lodash";
import { BaseEntity, DeleteResult, FindManyOptions, FindOptionsWhere, Repository } from "typeorm";
import { EntityId } from "typeorm/repository/EntityId";

export abstract class BaseService<T extends BaseEntity> {
	protected _repository: Repository<T>;
	constructor() {}

	async existsByIdAsync(id: EntityId) {
		const record = await this.getRecordByIdAsync(id);
		return record !== null;
	}

	async existsByAsync(where: FindOptionsWhere<T>) {
		const record = await this.getRecordByAsync(where);
		return record !== null;
	}

	getRecordByIdAsync(id: EntityId): Promise<T | null> {
		return this._repository.createQueryBuilder().where({ id }).getOne();
	}

	getManyRecordByIdsAsync(ids: EntityId[]): Promise<T[]> {
		return this._repository.createQueryBuilder().whereInIds(ids).getMany();
	}

	updateRecordByIdAsync(id: EntityId, data: Partial<T>) {
		return this._repository.update(id, data as any);
	}

	deleteRecordByIdAsync(id: EntityId): Promise<DeleteResult> {
		return this._repository.delete(id);
	}

	getManyRecordsAsync(options?: FindManyOptions<T>) {
		return this._repository.find(options);
	}

	getRecordByAsync(where: FindOptionsWhere<T>) {
		return this._repository.findOneBy(where);
	}

	updateByAsync(where: FindOptionsWhere<T>, data: Partial<T>) {
		return this._repository.update(where, data as any);
	}

	countByAsync(where: FindOptionsWhere<T>) {
		return this._repository.countBy(where);
	}

	saveAsync(entity: Partial<T>): Promise<T> {
		return this._repository.save(entity as T);
	}

	deleteByAsync(where: FindOptionsWhere<T>) {
		return this._repository.delete(where);
	}
}
