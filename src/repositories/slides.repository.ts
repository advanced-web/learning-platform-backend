import { Slides } from "../core";
import { defaultDataSource } from "../infrastructure/connections/database";

export const SlidesRepository = defaultDataSource.getRepository(Slides).extend({});
