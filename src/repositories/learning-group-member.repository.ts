import { LearningGroupMember } from "../core/entities";
import { defaultDataSource } from "../infrastructure/connections/database";

export const LearningGroupMemberRepository = defaultDataSource.getRepository(LearningGroupMember).extend({});
