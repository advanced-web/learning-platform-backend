import { PresentationChat } from "../core";
import { defaultDataSource } from "../infrastructure";

export const PresentationChatRepository = defaultDataSource.getRepository(PresentationChat).extend({});
