import { PresentationQuestion } from "../core";
import { defaultDataSource } from "../infrastructure";

export const PresentationQuestionRepository = defaultDataSource.getRepository(PresentationQuestion).extend({});
