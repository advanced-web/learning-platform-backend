export * from "./learning-group-member.repository";
export * from "./learning-group.repository";
export * from "./presentation-chat.repository";
export * from "./presentation-collaborator.repository";
export * from "./presentation-question.repository";
export * from "./presentations.repository";
export * from "./question-reaction.repository";
export * from "./res-user-profile.repository";
export * from "./res-user.repository";
export * from "./slides.repository";
export * from "./user-permission.repository";
export * from "./voting-results.repository";
