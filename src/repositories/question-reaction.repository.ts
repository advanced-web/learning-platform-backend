import { QuestionReaction } from "../core";
import { defaultDataSource } from "../infrastructure";

export const QuestionReactionRepository = defaultDataSource.getRepository(QuestionReaction).extend({});
