import { LearningGroup } from "../core/entities/learning-group.entity";
import { defaultDataSource } from "../infrastructure/connections/database";

export const LearningGroupRepository = defaultDataSource.getRepository(LearningGroup).extend({});
