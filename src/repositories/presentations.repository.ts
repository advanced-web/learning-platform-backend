import { Presentations } from "../core";
import { defaultDataSource } from "../infrastructure/connections/database";

export const PresentationsRepository = defaultDataSource.getRepository(Presentations).extend({});
