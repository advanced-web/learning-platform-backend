import { UserPermission } from "../core";
import { defaultDataSource } from "../infrastructure";

export const UserPermissionRepository = defaultDataSource.getRepository(UserPermission).extend({});
