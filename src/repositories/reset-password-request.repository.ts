import { ResetPasswordRequest } from "../core/entities";
import { defaultDataSource } from "../infrastructure/connections/database";

export const ResetPasswordRequestRepository = defaultDataSource.getRepository(ResetPasswordRequest).extend({});
