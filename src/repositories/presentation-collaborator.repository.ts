import { PresentationCollaborator } from "../core/entities/presentation-collaborator.entity";
import { defaultDataSource } from "../infrastructure";

export const PresentationCollaboratorRepository = defaultDataSource.getRepository(PresentationCollaborator).extend({});
