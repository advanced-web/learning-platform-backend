import { VotingResults } from "../core";
import { defaultDataSource } from "../infrastructure/connections/database";

export const VotingResultsRepository = defaultDataSource.getRepository(VotingResults).extend({});
