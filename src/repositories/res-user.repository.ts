import { ResUser } from "../core/entities";
import { defaultDataSource } from "../infrastructure/connections/database";

export const ResUserRepository = defaultDataSource.getRepository(ResUser).extend({});
