import { ResUserProfile } from "../core/entities";
import { defaultDataSource } from "../infrastructure/connections/database";

export const ResUserProfileRepository = defaultDataSource.getRepository(ResUserProfile).extend({});
