----------------------------------------
-- Table: res_user
----------------------------------------
create table if not exists res_user (
  id serial,
  username character varying(190) not null,
  password character varying(128),
  is_active boolean default false,
  oauth_provider character varying(20),
  oauth_uid character varying(128),
  -- oauth_access_token text,
  created_at timestamp with time zone default now(),
  updated_at timestamp with time zone default now()
);

alter table res_user
add constraint res_user_pkey primary key (id);

alter table res_user
add constraint res_user_username_unique unique (username);

alter table res_user
add constraint res_user_oauth_unique unique (oauth_provider, oauth_uid);


----------------------------------------
-- Table: res_user_profile
----------------------------------------
create table if not exists res_user_profile (
  id serial,
  first_name character varying(60) not null,
  last_name character varying(60) not null,
  display_name character varying(120) not null,
  email character varying(190) not null,
  avatar text,
  user_id integer,
  created_at timestamp with time zone default now(),
  updated_at timestamp with time zone default now()
);

alter table res_user_profile
add constraint res_user_profile_pkey primary key (id);


----------------------------------------
-- Table: learning_group
----------------------------------------
create table if not exists learning_group (
  id serial,
  name character varying(120) not null,
  description text,
  invitation_link character varying(255),
  group_avatar text,
  owner_profile_id integer not null,
  owner_display_name character varying(120) not null,
  created_at timestamp with time zone default now(),
  updated_at timestamp with time zone default now()
);

alter table learning_group
add constraint learning_group_pkey primary key (id);


----------------------------------------
-- Table: learning_group_member
-- Column: current_status = activity.lastElement
----------------------------------------
create table if not exists learning_group_member (
  id serial,
  group_id integer not null,
  user_profile_id integer not null,
  user_display_name character varying(120) not null,
  role_name character varying(50) not null,
  role character varying(20) not null,
  current_status json not null,
  activity json not null
);

alter table learning_group_member
add constraint learning_group_member_pkey primary key (id);

alter table learning_group_member
add constraint learning_group_member_identifier_unique unique (group_id, user_profile_id);
